var path = require('path');

module.exports = {
    mode: 'production',
    entry: './front_end/src/main.js',
    output: {
        path: path.resolve(__dirname, 'public/assets/js'),
        filename: 'scbmk.js'
    }
};
