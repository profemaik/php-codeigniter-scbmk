<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="No encontrado">
    <meta name="author" content="Maikel Carballo">

    <title>SCBmk - Problemas</title>
    <link href="<?= base_url('assets/imgs/icon/favicon.ico') ?>" rel="icon" type="image/png">

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/thirdparty/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/thirdparty/google-fonts/Nunito.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/css/sb-admin-2.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/scbmk.css') ?>" rel="stylesheet">

    <!-- sweetalert2 css -->
    <link rel="stylesheet" href="<?= base_url('assets/thirdparty/sweetalert2/themes/borderless.min.css') ?>">

    <!-- Estilo de CodeIgniter -->
    <style type="text/css">
		<?= preg_replace('#[\r\n\t ]+#', ' ', file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'debug.css')) ?>
	</style>

</head>

<body id="page-top">
    <!--[if lt IE 8]>
          <p class="browserupgrade">Usted está usando un <strong>NAVEGADOR WEB OBSOLETO</strong>. Por favor, <a href="http://browsehappy.com/">ACTUALICE SU NAVEGADOR O USE OTRO</a> para que pueda usar este sistema correctamente.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- 404 Error Text -->
                    <div class="text-center">
                        <div class="error mx-auto" data-text="500">500</div>
                        <p class="lead text-gray-800 mb-5">Error</p>
                        <p class="text-gray-500 mb-0">
                            ¡Vaya!, actualmente hay problemas para procesar lo
                            que Usted intentó.
						</p>
                        <a href="<?= base_url() ?>">&larr; Regresar al Inicio del SCBmk</a>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Maikel Carballo 2019 - 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/thirdparty/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/thirdparty/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/thirdparty/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/js/sb-admin-2.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/scbmk.js') ?>"></script>

    <!-- sweetalert2 js -->
    <script src="<?= base_url('assets/thirdparty/sweetalert2/sweetalert2.min.js') ?>"></script>

</body>

</html>