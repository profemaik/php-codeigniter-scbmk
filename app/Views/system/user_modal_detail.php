
<div id="modal_alert_parent"></div>
<nav>
  <div class="nav nav-pills flex-column flex-sm-row" id="nav-tab" role="tablist">
    <a class="flex-sm-fill text-sm-center nav-link active" id="nav-user-tab" data-toggle="tab" href="#nav-user" role="tab" aria-controls="nav-user" aria-selected="true">Usuario</a>
    <a class="flex-sm-fill text-sm-center nav-link" id="nav-person-tab" data-toggle="tab" href="#nav-person" role="tab" aria-controls="nav-person" aria-selected="false">Persona</a>
    <a class="flex-sm-fill text-sm-center nav-link" id="nav-auth-tab" data-toggle="tab" href="#nav-auth" role="tab" aria-controls="nav-auth" aria-selected="false">Permisos</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-user" role="tabpanel" aria-labelledby="nav-user-tab">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Nombre de usuario: <strong><?= esc($user->username, 'attr') ?></strong></li>
        <li class="list-group-item">Último inicio de sesión: <?php if (! $user->last_login) : ?>Nunca<?php else : ?><?= esc($user->last_login->humanize(), 'attr') ?> [<?= esc($user->last_login->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]<?php endif ?></li>
        <li class="list-group-item">Fecha de Ingreso: <?= esc($user->created_at->humanize(), 'attr') ?> [<?= esc($user->created_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]</li>
        <li class="list-group-item">Fecha de Egreso: <?php if ($user->deleted_at) : ?><?= esc($user->deleted_at->humanize(), 'attr') ?> [<?= esc($user->deleted_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]<?php endif ?></li>
        <li class="list-group-item">Imagen:
            <?php if ($upd->photo !== '') : ?>
                <a href="<?= base_url($upd->photo) ?>"
                    data-lightbox="image-<?= esc($user->username, 'attr') ?>"
                    data-title="<?= esc($user->username, 'attr') ?>">
                    <img class="img-profile rounded-circle"
                        src="<?= base_url($upd->photo) ?>"
                        alt="<?= esc($user->username, 'attr') ?>" style="height: 5rem;">
                </a>
            <?php endif ?>
        </li>
    </ul>
  </div>
  <div class="tab-pane fade" id="nav-person" role="tabpanel" aria-labelledby="nav-person-tab">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Nacionalidad: <?= NATIONALITIES[$upd->nationality] ?></li>
        <li class="list-group-item">Número de CI: <?= esc($upd->id_card_num, 'attr') ?></li>
        <li class="list-group-item">Nombres: <?= esc($upd->names, 'attr') ?></li>
        <li class="list-group-item">Apellidos: <?= esc($upd->lastnames, 'attr') ?></li>
        <li class="list-group-item">Género: <?= SOCIAL_GENDER[$upd->gender] ?></li>
        <li class="list-group-item">Correo Electrónico Principal: <?php if (isset($upd->emails['main'])) : ?><?= esc($upd->emails['main'], 'attr') ?><?php endif ?></li>
        <li class="list-group-item">Correo Electrónico Auxiliar: <?php if (isset($upd->emails['aux'])) : ?><?= esc($upd->emails['aux'], 'attr') ?><?php endif ?></li>
        <li class="list-group-item">Número Telefónico Principal: <?php if (isset($upd->phone_nums['main'])) : ?><?= esc($upd->phone_nums['main'], 'attr') ?><?php endif ?></li>
        <li class="list-group-item">Número Telefónico Auxiliar: <?php if (isset($upd->phone_nums['aux'])) : ?><?= esc($upd->phone_nums['aux'], 'attr') ?><?php endif ?></li>
    </ul>
  </div>
  <div class="tab-pane fade" id="nav-auth" role="tabpanel" aria-labelledby="nav-auth-tab">
    <ul class="list-group list-group-flush">
        <li class="list-group-item list-group-item-dark">¿Es Superusuario?</li>
        <?php if ($user->is_super === 't') : ?>
            <li class="list-group-item">Sí</li>
        <?php else : ?>
            <li class="list-group-item">No</li>
        <?php endif ?>
    </ul>
    <ul class="list-group list-group-flush">
        <li class="list-group-item list-group-item-dark">¿Es usuario Admin?</li>
        <?php if ($user->is_admin === 't') : ?>
            <li class="list-group-item">Sí</li>
        <?php else : ?>
            <li class="list-group-item">No</li>
        <?php endif ?>
    </ul>
    <ul class="list-group list-group-flush">
        <li class="list-group-item list-group-item-dark">Roles</li>
        <?php foreach ($roles as $role) : ?>
            <?php if ($role->selected) : ?>
                <li class="list-group-item"><?= esc($role->name) ?></li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
    <ul class="list-group list-group-flush">
        <li class="list-group-item list-group-item-dark">Permisos específicos del Usuario</li>
        <?php foreach ($permission as $rule) : ?>
            <?php if ($rule->selected) : ?>
                <li class="list-group-item"><?= esc($rule->name) ?></li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
  </div>
</div>