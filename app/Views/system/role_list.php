<?= $this->extend('system/table_layout') ?>

<?= $this->section('nav_item_roles') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Roles<?= $this->endSection() ?>
<?= $this->section('buttons_page_heading') ?>
    <a href="#" class="d-sm-inline btn btn-sm btn-light text-danger mr-2"><i class="fas fa-file-pdf fa-sm text-white-80"></i> Reporte en PDF</a>
    <?php if (session('is_super') === 't' || in_array('add_role', session('user_nav_item_codenames')) ) : ?>
    <a href="<?= route_to('display_add_role_form') ?>" class="d-sm-inline btn btn-sm btn-light text-primary mr-2"><i class="fas fa-plus fa-sm text-white-80"></i> Nuevo Rol</a>
    <?php endif ?>
<?= $this->endSection() ?>

<?= $this->section('card_header_title') ?>Roles registrados<?= $this->endSection() ?>

<?= $this->section('table_search_filters') ?>
    <div class="form-inline">
        <div class="mr-sm-2 mb-2" data-column="0">
            <label class="sr-only" for="col0_filter">Rol</label>
            <input type="text" class="form-control column_filter" id="col0_filter" placeholder="Buscar por rol">
        </div>
        <?php if (session('is_super') === 't' || in_array('enable_role', session('user_nav_item_codenames')) || in_array('disable_role', session('user_nav_item_codenames'))) : ?>
            <div class="custom-control custom-switch mr-sm-2 mb-2 ">
                <input type="checkbox" class="custom-control-input" id="swDisReg" data-deleted="<?= route_to('all_roles') ?>"
                    data-nodeleted="<?= route_to('roles') ?>" <?php if ($withDeleted) : ?>checked="checked"<?php endif ?>>
                <label class="custom-control-label" for="swDisReg">Roles Deshabilitados</label>
            </div>
    <?php endif ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('table_content') ?>
    <table class="table table-hover" id="dtb_role" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th>Rol</th>
                <th class="text-center" data-orderable="false">Opciones</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($roles as $role) : ?>
            <tr class="<?php if ($role->deleted_at) : ?>table-warning<?php else : ?><?php endif ?> text-dark">
                <td><?= esc($role->name) ?></td>
                <td class="text-center">
                    <?php if (session('is_super') === 't' || in_array('view_role', session('user_nav_item_codenames'))) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-info"
                            data-toggle="modal" data-target="#inDetailModal"
                            data-id="<?= esc($role->id, 'attr') ?>" data-name="<?= esc($role->name, 'attr') ?>"
                            data-method="get" title="Detalles" data-deleted-at="<?= esc($role->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_display_role_details', ACTION_FLAG_VIEW) ?>" data-type="html"
                            data-button="dtb_btn_see">
                            <i class="fas fa-eye" style="font-size: 20px"></i>
                        </button>
                    <?php endif ?>
                    <?php if (session('is_super') === 't' || in_array('change_role', session('user_nav_item_codenames'))) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-primary dtb-btn-change"
                            data-id="<?= esc($role->id, 'attr') ?>" data-name="<?= esc($role->name, 'attr') ?>"
                            data-method="get" title="Modificar" data-deleted-at="<?= esc($role->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_display_change_role_form', ACTION_FLAG_CHANGE) ?>" data-type="json"
                            data-button="dtb_btn_change">
                            <i class="fas fa-pencil-alt" style="font-size: 20px"></i>
                        </button>
                    <?php endif ?>
                    <?php if ((session('is_super') === 't' || in_array('enable_role', session('user_nav_item_codenames'))) && $role->deleted_at) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-success dtb-btn-enable"
                            data-toggle="modal" data-target="#confirmActionModal"
                            data-id="<?= esc($role->id, 'attr') ?>" data-name="<?= esc($role->name, 'attr') ?>"
                            data-method="post" title="Habilitar" data-deleted-at="<?= esc($role->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_enable_role', ACTION_FLAG_ENABLE) ?>" data-type="json"
                            data-button="dtb_btn_enable" data-<?= csrf_token() ?>="<?= csrf_hash() ?>">
                            <i class="fas fa-user-check" style="font-size: 20px;"></i>
                        </button>
                    <?php endif ?>
                    <?php if ((session('is_super') === 't' || in_array('delete_role', session('user_nav_item_codenames'))) && $role->deleted_at) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-danger dtb-btn-delete"
                            data-toggle="modal" data-target="#confirmActionModal"
                            data-id="<?= esc($role->id, 'attr') ?>" data-name="<?= esc($role->name, 'attr') ?>"
                            data-method="post" title="Eliminar permanentemente" data-deleted-at="<?= esc($role->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_delete_role', ACTION_FLAG_DELETE) ?>" data-type="json"
                            data-button="dtb_btn_suppress" data-<?= csrf_token() ?>="<?= csrf_hash() ?>">
                        <i class="fas fa-trash-alt" style="font-size: 20px"></i>
                    </button>
                    <?php elseif (session('is_super') === 't' || in_array('disable_role', session('user_nav_item_codenames'))) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-secondary dtb-btn-disable"
                            data-toggle="modal" data-target="#confirmActionModal"
                            data-id="<?= esc($role->id, 'attr') ?>" data-name="<?= esc($role->name, 'attr') ?>"
                            data-method="post" title="Deshabilitar" data-deleted-at="<?= esc($role->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_disable_role', ACTION_FLAG_DISABLE) ?>" data-type="json"
                            data-button="dtb_btn_disable" data-<?= csrf_token() ?>="<?= csrf_hash() ?>">
                        <i class="fas fa-user-slash" style="font-size: 20px"></i>
                    </button>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?= $this->endSection() ?>

<?= $this->section('my_modals') ?>
    <!-- User Detail Modal-->
    <div class="modal fade modal-action" id="inDetailModal" tabindex="-1" role="dialog" aria-labelledby="inDetailModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="inDetailModalLabel">Rol en detalle</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('extra_js_code') ?>
    <script>
        function filterColumn(i) {
            $('#dtb_role').DataTable().column(i).search(
                $('#col' + i + '_filter').val()
            ).draw();
        }
        $(document).ready(function() {
            $('#dtb_role').DataTable({
                "language": {
                    "url": "<?= base_url('assets/thirdparty/datatables/lang/Spanish.json'); ?>",
                },
                "responsive": true,
                "ordering": true,
            });

            $('input.column_filter').on( 'keyup click', function () {
                filterColumn( $(this).parents('div').attr('data-column') );
            });

            $('.dtb-btn-enable').click(function (e) {
                e.preventDefault();

                const dataObject = $(this).data();
                const msg = '¿Habilitar al rol: ' + dataObject.name + '?'
                $('#msgConfirmModal').text(msg);
            });

            $('.dtb-btn-disable').click(function (e) {
                e.preventDefault();

                const dataObject = $(this).data();
                const msg = '¿Deshabilitar al rol: ' + dataObject.name + '?'
                $('#msgConfirmModal').text(msg);
            });

            $('.dtb-btn-delete').click(function (e) {
                e.preventDefault();

                const dataObject = $(this).data();
                const msg = '¿ELiminar PERMANENTEMENTE al rol: ' + dataObject.name + '?'
                $('#msgConfirmModal').text(msg);
            });
        });
    </script>
<?= $this->endSection() ?>
