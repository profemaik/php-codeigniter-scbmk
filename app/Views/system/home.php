<?= $this->extend('system/base_layout') ?>

<?= $this->section('extra_meta_tags') ?>
    <meta name="description" content="Sistema de Control Base mk">
<?= $this->endSection() ?>

<?= $this->section('head_title') ?>
    <title>SCBmk</title>
<?= $this->endSection() ?>

<?= $this->section('body_attrs') ?>id="page-top"<?= $this->endSection() ?>

<?= $this->section('body_all_content') ?>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">SCBmk <sup>0.2</sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?= $this->renderSection('nav_item_dashboard') ?>">
                <a class="nav-link" href="<?= base_url() ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Tablero</span></a>
            </li>

            <!-- Divider -->
            <?php if (session('is_super') === 't' || in_array(['view_role', 'view_user'], session('user_nav_items')) ) : ?>
            <hr class="sidebar-divider">
                <!-- Heading -->
                <div class="sidebar-heading">
                    Sistema
                </div>
            <?php endif ?>

            <?php if (session('is_super') === 't' || in_array('view_role', session('user_nav_item_codenames'))) : ?>
                <!-- Nav Item - Roles -->
                <li class="nav-item <?= $this->renderSection('nav_item_roles') ?>">
                    <a class="nav-link" href="<?= route_to('roles') ?>">
                        <i class="fas fa-fw fa-user-tag"></i>
                        <span>Roles</span></a>
                </li>
            <?php endif ?>

            <?php if (session('is_super') === 't' || in_array('view_user', session('user_nav_item_codenames'))) : ?>
                <!-- Nav Item - Users -->
                <li class="nav-item <?= $this->renderSection('nav_item_users') ?>">
                    <a class="nav-link" href="<?= route_to('users') ?>">
                        <i class="fas fa-fw fa-users"></i>
                        <span>Usuarios</span></a>
                </li>
            <?php endif ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar..."
                                aria-label="Buscar" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Buscar..." aria-label="Buscar"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <!-- Nav Item - Alerts -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell fa-fw"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger badge-counter"><?= $this->renderSection('badge_counter_alerts') ?></span>
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="alertsDropdown">
                                <h6 class="dropdown-header">
                                    Centro de Alertas
                                </h6>
                                <?= $this->renderSection('dropdown_items_alerts') ?>
                                <a class="dropdown-item text-center small text-gray-500" href="#">No se han generado alertas</a>
                            </div>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= user_name() ?></span>
                                <img class="img-profile rounded-circle"
                                    src="<?= user_picture() ?>">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <!--<a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Perfil
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Configuraciones
                                </a>-->
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#userToolModal"
                                    data-method="get" data-url="<?= route_to('display_full_user_log') ?>"
                                    data-type="html">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Historial de Actividades
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= route_to('logout') ?>">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Cerrar Sesión
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $this->renderSection('title_page_heading') ?></h1>
                        <div class="col-12 col-xl-auto mb-3 d-flex flex-row">
                            <?= $this->renderSection('buttons_page_heading') ?>
                        </div>
                    </div>
                    <?php if (isset(session()->getFlashdata('page_notification')['msg'])) : ?>
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="alert alert-<?= session()->getFlashdata('page_notification')['type'] ?> fade show" role="alert">
                                <?= session()->getFlashdata('page_notification')['msg'] ?>
                              </div>
                            </div>
                        </div>
                    <?php endif ?>

                    <!-- Content -->
                    <?= $this->renderSection('page_content') ?>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <?= $this->include('includes/footer_container') ?>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
    </a>

    <!-- Confirm action Modal-->
    <div class="modal fade" id="confirmActionModal" tabindex="-1" role="dialog" aria-labelledby="confirmActionModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmActionModalLabel"><i class="fas fa-exclamation-triangle" style="font-size: 48px;"></i> CONFIRME</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cancelar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body"><p class="font-weight-bolder" id="msgConfirmModal"></p></div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="button" id="mdl_btn_accept">Continuar</button>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('my_modals') ?>
    <!-- User Tool Modal-->
    <div class="modal fade modal-action" id="userToolModal" tabindex="-1" role="dialog" aria-labelledby="userToolModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userToolModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>
