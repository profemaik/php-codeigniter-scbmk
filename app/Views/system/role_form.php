<?= $this->extend('system/form_layout') ?>

<?= $this->section('nav_item_roles') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Nuevo Rol<?= $this->endSection() ?>
<?= $this->section('buttons_page_heading') ?>
    <a href="<?= route_to('roles') ?>" class="d-sm-inline btn btn-sm btn-light text-primary mr-2"><i class="fas fa-arrow-left fa-sm text-white-80"></i> Atrás</a>
<?= $this->endSection() ?>

<?= $this->section('card_header_title') ?>Añadir nuevo rol<?= $this->endSection() ?>

<?= $this->section('form_id') ?><?= esc(str_replace('#', '', $formId), 'attr') ?><?= $this->endSection() ?>
<?= $this->section('form_content') ?>
    <div class="form-group row">
        <label for="Name" class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="name" id="Name">
            <?= $this->include('includes/form_feedbacks') ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="Summary" class="col-sm-2 col-form-label">Descripción breve</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="summary" id="Summary">
            <?= $this->include('includes/form_feedbacks') ?>
        </div>
    </div>
    <?= $this->include('includes/form_actions_save') ?>
<?= $this->endSection() ?>
