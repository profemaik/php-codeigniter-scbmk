<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
    <script>document.documentElement.className
        = document.documentElement.className
        .replace(/\bno-js\b/g, '') + ' js ';
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Maikel Carballo">
    <?= csrf_meta() ?>
    <?= $this->renderSection('extra_meta_tags') ?>

    <?= $this->renderSection('head_title') ?>

    <link href="<?= base_url('assets/imgs/icon/favicon.ico') ?>" rel="icon" type="image/png">
    <?= $this->renderSection('extra_icons') ?>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/thirdparty/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/thirdparty/google-fonts/Nunito.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/css/sb-admin-2.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/scbmk.css') ?>" rel="stylesheet">

    <!-- bibliotecas de terceros -->
    <link href="<?= base_url('assets/thirdparty/datatables/datatables-bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/thirdparty/bootstrap-duallistbox/bootstrap-duallistbox.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/thirdparty/lightbox/css/lightbox.min.css') ?>" rel="stylesheet" type="text/css">
    <?= $this->renderSection('extra_css_libs') ?>
    <?= $this->renderSection('extra_css_code') ?>

</head>

<body <?= $this->renderSection('body_attrs') ?>>
    <!--[if lt IE 8]>
            <p class="browserupgrade">
                <strong>NAVEGADOR WEB OBSOLETO</strong>. Por favor, <a href="http://browsehappy.com/">ACTUALICE SU
                NAVEGADOR O USE OTRO</a> para una mejor experiencia.
            </p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->

    <?= $this->renderSection('body_all_content') ?>

    <!-- Request fail Modal-->
    <div class="modal fade" id="errqtModal" tabindex="-1" role="dialog" aria-labelledby="errqtModalLabel"
        aria-hidden="true">
        <div class="modal-dialog<?php if (ENVIRONMENT !== 'production') : ?> modal-xl<?php endif ?>" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="errqtModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="#" class="img-fluid" id="errqim" alt="Responsive image">
                    <p class="lead text-gray-800 mt-5" id="errqms"></p>
                    <?php if (ENVIRONMENT !== 'production') : ?>
                        <div>
                            <h5>Debug Data</h5>
                            <div>
                                <pre class="text-warning bg-dark"><code id="errqtx"></code></pre>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <?= $this->renderSection('my_modals') ?>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/thirdparty/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/thirdparty/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/thirdparty/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/js/sb-admin-2.min.js') ?>"></script>

    <!-- bibliotecas de terceros -->
    <script src="<?= base_url('assets/thirdparty/datatables/datatables-bootstrap4.min.js') ?>"></script>
    <script src="<?= base_url('assets/thirdparty/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') ?>"></script>
    <script src="<?= base_url('assets/thirdparty/lightbox/js/lightbox.min.js') ?>"></script>
    <script src="<?= base_url('assets/thirdparty/bs-custom-file-input/bs-custom-file-input.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/scbmk.js') ?>"></script>
    <?= $this->renderSection('extra_js_libs') ?>
    <?= $this->renderSection('extra_js_code') ?>

</body>

</html>