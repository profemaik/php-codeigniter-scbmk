<fieldset class="mb-2">
    <legend class="col-form-label-sm">Aplicar Filtros de Búsqueda</legend>
    <div class="form-inline">
        <div class="mr-sm-2 mb-2" data-column="0">
            <label class="sr-only" for="col0_filter">Dirección IP</label>
            <input type="text" class="form-control column_filter" id="col0_filter" placeholder="por dirección IP">
        </div>
        <div class="mr-sm-2 mb-2" data-column="1">
            <label class="sr-only" for="col1_filter">Objeto Afectado</label>
            <input type="text" class="form-control column_filter" id="col1_filter" placeholder="por objeto afectado">
        </div>
        <div class="mr-sm-2 mb-2" data-column="2">
            <label class="sr-only" for="col2_filter">Tipo de Objeto</label>
            <input type="text" class="form-control column_filter" id="col2_filter" placeholder="por tipo de objeto">
        </div>
        <div class="mr-sm-2 mb-2" data-column="3">
            <label class="sr-only" for="col3_filter">Acción Ejecutada</label>
            <input type="text" class="form-control column_filter" id="col3_filter" placeholder="por acción ejecutada">
        </div>
        <div class="mr-sm-2 mb-2" data-column="5">
            <label class="sr-only" for="col5_filter">Marca de Tiempo</label>
            <input type="date" class="form-control column_filter" id="col5_filter" placeholder="por marca de tiempo">
        </div>
    </div>
</fieldset>

<div class="table-container">
    <table class="table table-hover" id="dtb_user_log" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th>Dirección IP</th>
                <th>Objeto Afectado</th>
                <th>Tipo de Objeto</th>
                <th>Acción Ejecutada</th>
                <th>Detalle</th>
                <th>Marca de Tiempo</th>
            </tr>
        </thead>
    </table>
</div>

<script>
    function filterColumn(i) {
        $('#dtb_user_log').DataTable().column(i).search(
            $('#col' + i + '_filter').val()
        ).draw();
    }

    $(document).ready(function() {
        $('#dtb_user_log').DataTable({
            "dom": 'lrtip',
            "columnDefs": [
                { "name": "ip_dir",    "targets": 0 },
                { "name": "obj_name",  "targets": 1 },
                { "name": "obj_type",  "targets": 2 },
                { "name": "action",    "targets": 3 },
                { "name": "detail",    "targets": 4 },
                { "name": "timestamp", "targets": 5 }
            ],
            "columns": [
                { "data": "ip_dir"    },
                { "data": "obj_name"  },
                { "data": "obj_type"  },
                { "data": "action"    },
                { "data": "detail"    },
                { "data": "timestamp" }
            ],
            "processing": true,
            "serverSide": true,
            "ajax": "<?= route_to('display_dtb_user_log') ?>",
            "language": {
                "url": "<?= base_url('assets/thirdparty/datatables/lang/Spanish.json'); ?>",
            },
            "responsive": true,
            "ordering": true,
        });

        $('input.column_filter').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').attr('data-column') );
        });
    });
</script>
