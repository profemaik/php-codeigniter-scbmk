<?= $this->extend('system/home') ?>

<?= $this->section('nav_item_dashboard') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Tablero<?= $this->endSection() ?>

<?= $this->section('buttons_page_heading') ?><?= $this->endSection() ?>

<?= $this->section('page_content') ?>
    <div class="row">

        <!-- Mini tarjeta Usuarios -->
        <div class="col-xl-6 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Usuarios Registrados</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $usersCount ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mini tarjeta Roles -->
        <div class="col-xl-6 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Roles Registrados</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $rolesCount ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-tag fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Content Column -->
        <div class="col-lg-12 mb-4">

            <!-- User Log -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Actividad Reciente</h6>
                    <!-- Para una futura implementación
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Filtrar Actividad</div>
                            <a class="dropdown-item" href="#">
                                <span class="badge badge-primary">Ingresos</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="badge badge-warning">Modificaciones</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="badge badge-danger">Eliminaciones</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <span class="badge badge-success">Habilitados</span>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="badge badge-secondary">Deshabilitados</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <span class="badge badge-info">Inicios de Sesión</span>
                            </a>
                        </div>
                    </div>
                    -->
                </div>
                <div class="card-body">
                    <div class="timeline timeline-sm">
                    <?php foreach ($userActivities as $activity) : ?>
                        <div class="timeline-item">
                            <div class="timeline-item-marker">
                                <div class="timeline-item-marker-text"><?= esc($activity->created_at->humanize()) ?></div>
                                <div class="timeline-item-marker-indicator bg-<?= BG_COLOR_CSS[esc($activity->action_flag)] ?>"></div>
                            </div>
                            <div class="timeline-item-content">
                                <?= ucfirst(esc($activity->object_type)) ?>
                                <span class="font-weight-bold text-dark"><?= esc($activity->object_repr) ?></span>
                                <?= esc($activity->message) ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                    </div>
                </div>
            </div>

        </div>

        <!-- Content Column -->
        <div class="col-lg-12 mb-4">

            <!-- Approach -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Sistema de Control Base mk (SCBmk)</h6>
                </div>
                <div class="card-body">
                    <p>SCBmk pretende ser una plantilla para crear Sistemas de Información ofreciendo una interfaz gráfica de usuario de tipo panel de control (dashboard).</p>
                    <p class="mb-0">El front-end está basado en herramientas estándares: HTML 5, CSS 3, JavaScript. Intentando minimizar, en lo posible el uso de librerías de terceros.</p>
                    <p>El diseño de la interfaz gráfica está basado en la plantilla <a href="https://github.com/StartBootstrap/startbootstrap-sb-admin-2" target="_blank">SB Admin 2</a></p>
                    <p>El back-end está basado en CodeIgniter 4, Apache HTTP Server 2, PHP 7. Utiliza Composer para la instalación de bibliotecas.</p>
                </div>
            </div>

        </div>
    </div>
<?= $this->endSection() ?>
