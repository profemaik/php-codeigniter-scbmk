<?= $this->extend('system/form_layout') ?>

<?= $this->section('nav_item_roles') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Rol Existente<?= $this->endSection() ?>
<?= $this->section('buttons_page_heading') ?>
    <a href="<?= route_to('roles') ?>" class="d-sm-inline btn btn-sm btn-light text-primary mr-2"><i class="fas fa-arrow-left fa-sm text-white-80"></i> Atrás</a>
<?= $this->endSection() ?>

<?= $this->section('card_header_title') ?>Modificar rol existente<?= $this->endSection() ?>

<?= $this->section('form_id') ?><?= esc(str_replace('#', '', $formId), 'attr') ?><?= $this->endSection() ?>
<?= $this->section('form_content') ?>
    <!-- Fieldset Rol -->

    <fieldset class="form-group">
        <legend>Rol</legend>
        <div class="form-group row">
            <label for="Name" class="col-sm-2 col-form-label">Nombre de Rol</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="Name"
                    value="<?= esc($role->name, 'attr') ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Summary" class="col-sm-2 col-form-label">Descripción breve</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="summary" id="Summary"
                    value="<?= esc($role->summary, 'attr') ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
          <label for="DateCreated" class="col-sm-2 col-form-label">Fecha de Creado</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" disabled name="dateCreated" id="DateCreated"
                value="<?= esc($role->created_at->humanize(), 'attr') ?> [<?= esc($role->created_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]">
          </div>
        </div>
        <div class="form-group row">
          <label for="DateCreated" class="col-sm-2 col-form-label">Fecha de Desactivado</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" disabled name="dateCreated" id="DateCreated"
                value="<?php if ($role->deleted_at) : ?><?= esc($role->deleted_at->humanize(), 'attr') ?> [<?= esc($role->deleted_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]<?php endif ?>">
          </div>
        </div>
    </fieldset>

    <!-- Fieldset Permisos -->
    <fieldset class="form-group">
        <legend class="bg-gradient-primary text-white">Permisos</legend>
        <div class="form-group row">
          <label for="UserStatus" class="col-sm-2 col-form-label">Estatus</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext <?php if ($role->deleted_at) : ?> text-danger<?php else : ?> text-success<?php endif ?>" id="UserStatus"
                value="<?php if ($role->deleted_at) : ?>Rol Deshabilitado<?php else : ?>Rol Habilitado<?php endif ?>">
          </div>
        </div>
        <div class="form-group row">
            <label for="Permission_id" class="col-sm-2 col-form-label">Permisos del rol</label>
            <div class="col-sm-10">
                <select class="form-control" size="10" name="permission_id[]" id="Permission_id" multiple>
                    <?php foreach ($permission as $rule) : ?>
                        <option value="<?= esc($rule->id, 'attr') ?>" <?= esc($rule->selected, 'attr') ?>><?= esc($rule->name) ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </fieldset>
    <?= $this->include('includes/form_actions_savedelete') ?>
<?= $this->endSection() ?>
