<?= $this->extend('system/home') ?>

<?= $this->section('page_content') ?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $this->renderSection('card_header_title') ?></h6>
        </div>
        <div class="card-body">
            <div class="form-inline">
                <?= $this->renderSection('table_search_filters') ?>
            </div>
            <br>
            <div class="table-container">
                <?= $this->renderSection('table_content') ?>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>
