<?= $this->extend('system/form_layout') ?>

<?= $this->section('nav_item_users') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Usuario Existente<?= $this->endSection() ?>
<?= $this->section('buttons_page_heading') ?>
    <a href="<?= route_to('users') ?>" class="d-sm-inline btn btn-sm btn-light text-primary mr-2"><i class="fas fa-arrow-left fa-sm text-white-80"></i> Atrás</a>
<?= $this->endSection() ?>

<?= $this->section('card_header_title') ?>Modificar usuario existente<?= $this->endSection() ?>

<?= $this->section('form_id') ?>Fm_ch_user<?= $this->endSection() ?>
<?= $this->section('form_attr') ?>enctype="multipart/form-data"<?= $this->endSection() ?>
<?= $this->section('form_content') ?>
    <!-- Fieldset Usuario -->

    <fieldset class="form-group">
        <legend>Usuario</legend>
        <div class="form-group row">
            <label for="Username" class="col-sm-2 col-form-label">Nombre de Usuario</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="username" id="Username"
                    value="<?= esc($user->username, 'attr') ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
          <label for="Last_login" class="col-sm-2 col-form-label">Último inicio de sesión</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" name="last_login" id="Last_login"
                value="<?= esc($user->last_login, 'attr') ?>">
          </div>
        </div>
        <div class="form-group row">
          <label for="DateCreated" class="col-sm-2 col-form-label">Fecha de Ingreso</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" disabled name="dateCreated" id="DateCreated"
                value="<?= esc($user->created_at->humanize(), 'attr') ?> [<?= esc($user->created_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]">
          </div>
        </div>
        <div class="form-group row">
          <label for="DateCreated" class="col-sm-2 col-form-label">Fecha de Egreso</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" disabled name="dateCreated" id="DateCreated"
                value="<?php if ($user->deleted_at) : ?><?= esc($user->deleted_at->humanize(), 'attr') ?> [<?= esc($user->deleted_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]<?php endif ?>">
          </div>
        </div>
        <div class="form-group row">
          <label for="UserPhoto" class="col-sm-2 col-form-label">Imagen del Usuario</label>
            <?php if ($upd->photo !== '') : ?>
            <div class="col-md-2">
                <a href="<?= base_url($upd->photo) ?>" id="UserPhoto"
                    data-lightbox="image-<?= esc($user->username, 'attr') ?>"
                    data-title="Imagen del Usuario">
                    <img class="img-profile rounded-circle"
                        src="<?= base_url($upd->photo) ?>"
                        alt="<?= esc($user->username, 'attr') ?>" style="height: 5rem;">
                </a>
            </div>
            <?php endif ?>
            <div class="col d-flex align-items-center">
                <a href="#" class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#userPicFormModal">
                    <span class="icon text-white-50">
                        <i class="fas fa-file-upload"></i>
                    </span>
                    <span class="text">Reemplazar</span>
                </a>
            </div>
        </div>
    </fieldset>

    <!-- Fieldset Datos Personales -->
    <fieldset class="form-group">
        <legend class="bg-gradient-primary text-white">Datos Personales</legend>
        <div class="form-group row">
            <label for="Nationality" class="col-sm-2 col-form-label">Nacionalidad</label>
            <div class="col-sm-10">
                <select class="custom-select" name="nationality" id="Nationality">
                    <option value="">Seleccione</option>
                    <option value="V"<?php if ($upd->nationality === 'V') : ?> selected<?php endif ?>>Venezolano</option>
                    <option value="E"<?php if ($upd->nationality === 'E') : ?> selected<?php endif ?>>Extranjero</option>
                </select>
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Id_card_num" class="col-sm-2 col-form-label">Número de CI</label>
            <div class="col-sm-10">
                <input type="text" class="form-control ci" name="id_card_num" id="Id_card_num"
                    value="<?= esc($upd->id_card_num, 'attr') ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Names" class="col-sm-2 col-form-label">Nombres</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="names" id="Names"
                    value="<?= esc($upd->names, 'attr') ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Lastnames" class="col-sm-2 col-form-label">Apellidos</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" name="lastnames" id="Lastnames"
                    value="<?= esc($upd->lastnames, 'attr') ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Gender" class="col-sm-2 col-form-label">Género</label>
            <div class="col-sm-10">
                <select class="custom-select" name="gender" id="Gender">
                    <option value="">Seleccione</option>
                    <option value="M"<?php if ($upd->gender === 'M') : ?> selected<?php endif ?>>Mujer</option>
                    <option value="H"<?php if ($upd->gender === 'H') : ?> selected<?php endif ?>>Hombre</option>
                    <option value="X"<?php if ($upd->gender === 'X') : ?> selected<?php endif ?>>Ser Humano</option>
                </select>
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Emails.main" class="col-sm-2 col-form-label">Correo Electrónico Principal</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="emails[main]" id="Emails.main"
                    value="<?php if (isset($upd->emails['main'])) : ?><?= esc($upd->emails['main'], 'attr') ?><?php endif ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Emails.aux" class="col-sm-2 col-form-label">Correo Electrónico Auxiliar</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="emails[aux]" id="Emails.aux"
                    value="<?php if (isset($upd->emails['aux'])) : ?><?= esc($upd->emails['aux'], 'attr') ?><?php endif ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Phone_nums" class="col-sm-2 col-form-label">Número Telefónico Principal</label>
            <div class="col-sm-10">
                <input type="tel" class="form-control" name="phone_nums[main]" id="Phone_nums"
                    value="<?php if (isset($upd->phone_nums['main'])) : ?><?= esc($upd->phone_nums['main'], 'attr') ?><?php endif ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="Phone_nums" class="col-sm-2 col-form-label">Número Telefónico Auxiliar</label>
            <div class="col-sm-10">
                <input type="tel" class="form-control" name="phone_nums[aux]" id="Phone_nums"
                    value="<?php if (isset($upd->phone_nums['aux'])) : ?><?= esc($upd->phone_nums['aux'], 'attr') ?><?php endif ?>">
                <?= $this->include('includes/form_feedbacks') ?>
            </div>
        </div>
    </fieldset>

    <!-- Fieldset Permisos -->
    <fieldset class="form-group">
        <legend class="bg-gradient-primary text-white">Permisos</legend>
        <div class="form-group row">
          <label for="UserStatus" class="col-sm-2 col-form-label">Estatus</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext <?php if ($user->deleted_at) : ?> text-danger<?php else : ?> text-success<?php endif ?>" id="UserStatus"
                value="<?php if ($user->deleted_at) : ?>Usuario Deshabilitado<?php else : ?>Usuario Habilitado<?php endif ?>">
          </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="is_admin" id="Is_admin"
                        aria-describedby="isAdminHelpBlock" value="t"
                        <?php if ($user->is_admin === 't') : ?> checked<?php endif ?>>
                    <label class="form-check-label" for="Is_admin">
                        Es administrador
                    </label>
                    <small id="isAdminHelpBlock" class="form-text text-muted">
                      Indica si este usuario puede entrar en la configuración del sistema.
                    </small>
                    <?= $this->include('includes/form_feedbacks') ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="is_super" id="Is_super"
                        aria-describedby="isSuperHelpBlock" value="t"
                        <?php if ($user->is_super === 't') : ?> checked<?php endif ?>>
                    <label class="form-check-label" for="Is_super">
                      Es superusuario
                    </label>
                    <small id="isSuperHelpBlock" class="form-text text-muted">
                      Indica si este usuario tiene todos los permisos sin asignárselos explícitamente.
                    </small>
                    <?= $this->include('includes/form_feedbacks') ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="Role_id" class="col-sm-2 col-form-label">Roles</label>
            <div class="col-sm-10">
                <select class="form-control" size="10" name="role_id[]" id="Role_id" multiple>
                    <?php foreach ($roles as $role) : ?>
                        <option value="<?= esc($role->id, 'attr') ?>" <?= esc($role->selected, 'attr') ?>><?= esc($role->name) ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="Permission_id" class="col-sm-2 col-form-label">Permisos de usuario</label>
            <div class="col-sm-10">
                <select class="form-control" size="10" name="permission_id[]" id="Permission_id" multiple>
                    <?php foreach ($permission as $rule) : ?>
                        <option value="<?= esc($rule->id, 'attr') ?>" <?= esc($rule->selected, 'attr') ?>><?= esc($rule->name) ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </fieldset>
    <?= $this->include('includes/form_actions_savedelete') ?>
<?= $this->endSection() ?>

<?= $this->section('my_modals') ?>
    <!-- User Picture Form Modal-->
    <div class="modal fade modal-action" id="userPicFormModal" tabindex="-1" role="dialog" aria-labelledby="userPicFormModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userPicFormModalLabel">Seleccionar nueva imagen</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cancelar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modal_alert_parent"></div>
                    <form id="Fm_user_pic" enctype="multipart/form-data" novalidate>
                        <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">
                        <input type="hidden" name="MAX_FILE_SIZE" value="120000">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="photo" id="Photo" accept="image/*">
                            <label class="custom-file-label" for="Photo">Buscar archivo</label>
                            <?= $this->include('includes/form_feedbacks') ?>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="button" id="mdl_user_pic_btn_accept"
                            data-form="#Fm_user_pic" data-url="<?= route_to('save_image_user') ?>"
                            data-type="json" data-method="post" data-button="mdl_user_pic_btn_accept">
                        Continuar
                    </button>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>
