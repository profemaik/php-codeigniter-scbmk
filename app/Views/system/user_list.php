<?= $this->extend('system/table_layout') ?>

<?= $this->section('nav_item_users') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Usuarios<?= $this->endSection() ?>
<?= $this->section('buttons_page_heading') ?>
    <a href="#" class="d-sm-inline btn btn-sm btn-light text-danger mr-2"><i class="fas fa-file-pdf fa-sm text-white-80"></i> Reporte en PDF</a>
    <?php if (session('is_super') === 't' || in_array('add_user', session('user_nav_item_codenames')) ) : ?>
    <a href="<?= route_to('display_add_user_form') ?>" class="d-sm-inline btn btn-sm btn-light text-primary mr-2"><i class="fas fa-plus fa-sm text-white-80"></i> Nuevo Usuario</a>
    <?php endif ?>
<?= $this->endSection() ?>

<?= $this->section('card_header_title') ?>Usuarios registrados<?= $this->endSection() ?>

<?= $this->section('table_search_filters') ?>
    <div class="form-inline">
        <div class="mr-sm-2 mb-2" data-column="0">
            <label class="sr-only" for="col0_filter">Usuario</label>
            <input type="text" class="form-control column_filter" id="col0_filter" placeholder="Buscar por usuario">
        </div>
        <div class="mr-sm-2 mb-2" data-column="1">
            <label class="sr-only" for="col1_filter">Número de CI</label>
            <input type="number" class="form-control column_filter" id="col1_filter" placeholder="Buscar por nro. de Cédula" step="any">
        </div>
        <div class="mr-sm-2 mb-2" data-column="2">
            <label class="sr-only" for="col2_filter">Nombre(s)</label>
            <input type="text" class="form-control column_filter" id="col2_filter" placeholder="Buscar por nombres">
        </div>
        <div class="mr-sm-2 mb-2" data-column="3">
            <label class="sr-only" for="col3_filter">Apellido(s)</label>
            <input type="text" class="form-control column_filter" id="col3_filter" placeholder="Buscar por apellidos">
        </div>
        <?php if (session('is_super') === 't' || in_array('enable_user', session('user_nav_item_codenames')) || in_array('disable_user', session('user_nav_item_codenames'))) : ?>
            <div class="custom-control custom-switch mr-sm-2 mb-2 ">
                <input type="checkbox" class="custom-control-input" id="swDisReg" data-deleted="<?= route_to('all_users') ?>"
                    data-nodeleted="<?= route_to('users') ?>" <?php if ($withDeleted) : ?>checked="checked"<?php endif ?>>
                <label class="custom-control-label" for="swDisReg">Usuarios Deshabilitados</label>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('table_content') ?>
    <table class="table table-hover" id="dtb_user" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th>Usuario</th>
                <th>Nro. de Cédula</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th class="text-center" data-orderable="false">Opciones</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user) : ?>
            <tr class="<?php if ($user->deleted_at) : ?>table-warning<?php else : ?><?php endif ?> text-dark">
                <td><?= esc($user->username) ?></td>
                <td><?= esc($user->id_card_num) ?></td>
                <td><?= esc($user->names) ?></td>
                <td><?= esc($user->lastnames) ?></td>
                <td class="text-center">
                    <?php if (session('is_super') === 't' || in_array('view_user', session('user_nav_item_codenames'))) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-info"
                            data-toggle="modal" data-target="#inDetailModal"
                            data-id="<?= esc($user->id, 'attr') ?>" data-username="<?= esc($user->username, 'attr') ?>"
                            data-id-card-num="<?= esc($user->id_card_num, 'attr') ?>" data-names="<?= esc($user->names, 'attr') ?>"
                            data-lastnames="<?= esc($user->lastnames, 'attr') ?>" data-method="get" title="Detalles"
                            data-deleted-at="<?= esc($user->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_display_user_details', ACTION_FLAG_VIEW) ?>" data-type="html"
                            data-button="dtb_btn_see">
                            <i class="fas fa-eye" style="font-size: 20px"></i>
                        </button>
                    <?php endif ?>
                    <?php if (session('is_super') === 't' || in_array('change_user', session('user_nav_item_codenames'))) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-primary dtb-btn-change"
                            data-id="<?= esc($user->id, 'attr') ?>" data-username="<?= esc($user->username, 'attr') ?>"
                            data-id-card-num="<?= esc($user->id_card_num, 'attr') ?>" data-names="<?= esc($user->names, 'attr') ?>"
                            data-lastnames="<?= esc($user->lastnames, 'attr') ?>" data-method="get" title="Modificar"
                            data-deleted-at="<?= esc($user->deleted_at, 'attr') ?>"
                            data-url="<?= route_to('dtb_display_change_user_form', ACTION_FLAG_CHANGE) ?>" data-type="json"
                            data-button="dtb_btn_change">
                            <i class="fas fa-pencil-alt" style="font-size: 20px"></i>
                        </button>
                    <?php endif ?>
                    <?php if (session('is_super') === 't' && $user->deleted_at) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-success dtb-btn-enable"
                            data-toggle="modal" data-target="#confirmActionModal"
                            data-id="<?= esc($user->id, 'attr') ?>" data-username="<?= esc($user->username, 'attr') ?>"
                            data-id-card-num="<?= esc($user->id_card_num, 'attr') ?>" data-names="<?= esc($user->names, 'attr') ?>"
                            data-lastnames="<?= esc($user->lastnames, 'attr') ?>" data-method="post" title="Habilitar"
                            data-url="<?= route_to('dtb_enable_user', ACTION_FLAG_ENABLE) ?>" data-type="json"
                            data-button="dtb_btn_enable" data-<?= csrf_token() ?>="<?= csrf_hash() ?>">
                            <i class="fas fa-user-check" style="font-size: 20px;"></i>
                        </button>
                    <?php endif ?>
                    <?php if (session('is_super') === 't' && $user->deleted_at) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-danger dtb-btn-delete"
                            data-toggle="modal" data-target="#confirmActionModal"
                            data-id="<?= esc($user->id, 'attr') ?>" data-username="<?= esc($user->username, 'attr') ?>"
                            data-id-card-num="<?= esc($user->id_card_num, 'attr') ?>" data-names="<?= esc($user->names, 'attr') ?>"
                            data-lastnames="<?= esc($user->lastnames, 'attr') ?>" data-method="post" title="Suprimir permanentemente"
                            data-url="<?= route_to('dtb_delete_user', ACTION_FLAG_DELETE) ?>" data-type="json"
                            data-button="dtb_btn_suppress" data-<?= csrf_token() ?>="<?= csrf_hash() ?>">
                        <i class="fas fa-trash-alt" style="font-size: 20px"></i>
                    </button>
                    <?php elseif (session('is_super') === 't' || in_array('delete_user', session('user_nav_item_codenames'))) : ?>
                        <button class="btn btn-light btn-circle btn-sm text-secondary dtb-btn-disable"
                            data-toggle="modal" data-target="#confirmActionModal"
                            data-id="<?= esc($user->id, 'attr') ?>" data-username="<?= esc($user->username, 'attr') ?>"
                            data-id-card-num="<?= esc($user->id_card_num, 'attr') ?>" data-names="<?= esc($user->names, 'attr') ?>"
                            data-lastnames="<?= esc($user->lastnames, 'attr') ?>" data-method="post" title="Deshabilitar"
                            data-url="<?= route_to('dtb_disable_user', ACTION_FLAG_DISABLE) ?>" data-type="json"
                            data-button="dtb_btn_disable" data-<?= csrf_token() ?>="<?= csrf_hash() ?>">
                        <i class="fas fa-user-slash" style="font-size: 20px"></i>
                    </button>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?= $this->endSection() ?>

<?= $this->section('my_modals') ?>
    <!-- User Detail Modal-->
    <div class="modal fade modal-action" id="inDetailModal" tabindex="-1" role="dialog" aria-labelledby="inDetailModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="inDetailModalLabel">Usuario en detalle</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('extra_js_code') ?>
    <script>
        function filterColumn(i) {
            $('#dtb_user').DataTable().column(i).search(
                $('#col' + i + '_filter').val()
            ).draw();
        }
        $(document).ready(function() {
            $('#dtb_user').DataTable({
                "language": {
                    "url": "<?= base_url('assets/thirdparty/datatables/lang/Spanish.json'); ?>",
                },
                "responsive": true,
                "ordering": true,
            });

            $('input.column_filter').on( 'keyup click', function () {
                filterColumn( $(this).parents('div').attr('data-column') );
            });

            $('.dtb-btn-enable').click(function (e) {
                e.preventDefault();

                const dataObject = $(this).data();
                const msg = '¿Habilitar al usuario: ' + dataObject.username + '?'
                $('#msgConfirmModal').text(msg);
            });

            $('.dtb-btn-disable').click(function (e) {
                e.preventDefault();

                const dataObject = $(this).data();
                const msg = '¿Deshabilitar al usuario: ' + dataObject.username + '?'
                $('#msgConfirmModal').text(msg);
            });

            $('.dtb-btn-delete').click(function (e) {
                e.preventDefault();

                const dataObject = $(this).data();
                const msg = '¿ELiminar PERMANENTEMENTE al usuario: ' + dataObject.username + '?'
                $('#msgConfirmModal').text(msg);
            });
        });
    </script>
<?= $this->endSection() ?>
