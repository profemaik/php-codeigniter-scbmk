<?= $this->extend('system/form_layout') ?>

<?= $this->section('nav_item_users') ?>active<?= $this->endSection() ?>

<?= $this->section('title_page_heading') ?>Nuevo Usuario<?= $this->endSection() ?>
<?= $this->section('buttons_page_heading') ?>
    <a href="<?= route_to('users') ?>" class="d-sm-inline btn btn-sm btn-light text-primary mr-2"><i class="fas fa-arrow-left fa-sm text-white-80"></i> Atrás</a>
<?= $this->endSection() ?>

<?= $this->section('card_header_title') ?>Añadir nuevo usuario<?= $this->endSection() ?>

<?= $this->section('form_id') ?><?= esc(str_replace('#', '', $formId), 'attr') ?><?= $this->endSection() ?>
<?= $this->section('form_content') ?>
    <div class="form-group row">
        <label for="Id_card_num" class="col-sm-2 col-form-label">Número de CI</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="id_card_num" id="Id_card_num">
            <?= $this->include('includes/form_feedbacks') ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="Username" class="col-sm-2 col-form-label">Nombre de Usuario</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="username" id="Username">
            <?= $this->include('includes/form_feedbacks') ?>
        </div>
    </div>
    <?= $this->include('includes/form_actions_save') ?>
<?= $this->endSection() ?>
