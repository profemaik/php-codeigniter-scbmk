<?= $this->extend('system/auth/base_auth_form_layout') ?>

<?= $this->section('extra_meta_tags') ?>
    <meta name="description"
        content="Recuperación de contraseña en el Sistema de Control Base mk">
<?= $this->endSection() ?>

<?= $this->section('head_title') ?>
    <title>SCBmk - Recuperar Contraseña</title>
<?= $this->endSection() ?>

<?= $this->section('bg_class_image') ?>bg-forgot-password-image<?= $this->endSection() ?>

<?= $this->section('p5_text') ?>
    <p>Recuperar Contraseña</p>
    <p>
        Ingrese su nombre de usuario en el siguiente campo y presione el botón
        "Reiniciar Contraseña". Se le enviará a su correo electrónico las
        instrucciones para continuar.
    </p>
<?= $this->endSection() ?>

<?= $this->section('auth_form_id') ?>Fm_rcv_psw<?= $this->endSection() ?>

<?= $this->section('auth_form_groups') ?>
    <div class="form-group">
        <input type="text" class="form-control form-control-user"
            id="Username" name="username" aria-describedby="usernameHelp"
            placeholder="Usuario" autofocus>
        <?= $this->include('includes/form_feedbacks') ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('auth_form_action_buttons') ?>
    <button type="submit" class="btn btn-primary btn-user btn-block btn_action"
        data-form="#Fm_rcv_psw" data-url="<?= route_to('recover_password') ?>"
        data-type="json" data-method="post" data-button="btn_rcvpsw">
      Reiniciar Contraseña
    </button>
<?= $this->endSection() ?>

<?= $this->section('auth_bottom_extra_content') ?>
    <hr>
    <div class="text-center">
        <a class="small" href="<?= base_url() ?>">¿Ya tiene cuenta? Acceda aquí</a>
    </div>
<?= $this->endSection() ?>
