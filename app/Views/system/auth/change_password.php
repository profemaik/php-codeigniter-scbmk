<?= $this->extend('system/auth/base_auth_form_layout') ?>

<?= $this->section('extra_meta_tags') ?>
    <meta name="description" content="Cambio de contraseña en el Sistema de Control Base mk">
<?= $this->endSection() ?>

<?= $this->section('head_title') ?>
    <title>SCBmk - Cambiar Contraseña</title>
<?= $this->endSection() ?>

<?= $this->section('bg_class_image') ?>bg-password-image<?= $this->endSection() ?>

<?= $this->section('p5_text') ?>
    <p>Cambiar Contraseña</p>
<?= $this->endSection() ?>

<?= $this->section('auth_form_id') ?>Fm_ch_psw<?= $this->endSection() ?>

<?= $this->section('auth_form_groups') ?>
    <div class="form-group">
        <input type="password" class="form-control form-control-user" id="Password"
            name="password" placeholder="Contraseña" autocomplete="new-password" autofocus>
        <?= $this->include('includes/form_feedbacks') ?>
    </div>
    <div class="form-group">
        <input type="password" class="form-control form-control-user" id="Password2"
            name="password2" placeholder="Repita Contraseña" autocomplete="new-password">
        <?= $this->include('includes/form_feedbacks') ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('auth_form_action_buttons') ?>
    <button type="submit" class="btn btn-primary btn-user btn-block btn_action"
        data-form="#Fm_ch_psw" data-url="<?= route_to('change_password') ?>"
        data-type="json" data-method="post" data-button="btn_change_psw">
      Cambiar Contraseña
    </button>
    <a href="<?= route_to('logout') ?>"
        class="btn btn-secondary btn-user btn-block" id="btn_cancel_change_psw">
      Cancelar
    </a>
<?= $this->endSection() ?>
