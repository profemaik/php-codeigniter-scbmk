<?= $this->extend('system/auth/base_auth_form_layout') ?>

<?= $this->section('extra_meta_tags') ?>
    <meta name="description" content="Acceso al Sistema de Control Base mk">
<?= $this->endSection() ?>

<?= $this->section('head_title') ?>
    <title>SCBmk - Acceder al Sistema</title>
<?= $this->endSection() ?>

<?= $this->section('bg_class_image') ?>bg-login-image<?= $this->endSection() ?>

<?= $this->section('p5_text') ?>
    <p>Acceder al Sistema</p>
<?= $this->endSection() ?>

<?= $this->section('auth_form_id') ?>Fm_log_in<?= $this->endSection() ?>

<?= $this->section('auth_form_groups') ?>
    <div class="form-group">
        <input type="email" class="form-control form-control-user"
            id="Email" name="email" aria-describedby="emailHelp"
            placeholder="Usuario" autocomplete="username" autofocus>
        <?= $this->include('includes/form_feedbacks') ?>
    </div>
    <div class="form-group">
        <input type="password" class="form-control form-control-user"
            id="Passwd" name="passwd" placeholder="Contraseña" autocomplete="current-password">
        <?= $this->include('includes/form_feedbacks') ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('auth_form_action_buttons') ?>
    <button type="submit" class="btn btn-primary btn-user btn-block btn_action"
        data-form="#Fm_log_in" data-url="<?= route_to('login') ?>"
        data-type="json" data-method="post" data-button="btn_login">
      Acceder
    </button>
<?= $this->endSection() ?>

<?= $this->section('auth_bottom_extra_content') ?>
    <hr>
    <div class="text-center">
        <a class="small" href="<?= route_to('display_recover_psw_petition_form') ?>">¿Olvidó su contraseña?</a>
    </div>
<?= $this->endSection() ?>
