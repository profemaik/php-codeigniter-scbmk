<?= $this->extend('system/base_layout') ?>

<?= $this->section('body_attrs') ?>class="bg-gradient-primary"<?= $this->endSection() ?>

<?= $this->section('body_all_content') ?>

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block <?= $this->renderSection('bg_class_image') ?>"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">SCBmk</h1>
                                        <?= $this->renderSection('p5_text') ?>
                                    </div>
                                    <div class="alert-container" id="alert_parent" style="display: none;"></div>
                                    <form class="user" id="<?= $this->renderSection('auth_form_id') ?>" novalidate>
                                        <?= csrf_field() ?>
                                        <?= $this->renderSection('auth_form_groups') ?>
                                        <?= $this->renderSection('auth_form_action_buttons') ?>
                                    </form>
                                    <?= $this->renderSection('auth_bottom_extra_content') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Footer -->
    <footer class="sticky-footer" style="color: white;">
        <?= $this->include('includes/footer_container') ?>
    </footer>
    <!-- End of Footer -->
<?= $this->endSection() ?>
