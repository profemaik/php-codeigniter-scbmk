<!DOCTYPE html>
<html lang='es'>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>SCBmk - Recuperación de Acceso</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'>
</head>

<body>
    <div class='text-center'>
        <h1>Instrucciones de Recuperación de Acceso al SCBmk</h1>
        <p><em>La recuperación del acceso al SCBmk consiste en establecer una nueva clave o contraseña de acceso, la cual
                debe cumplir con ciertos criterios (especificados más abajo), tenga preparada una o varias claves antes
                de comenzar, ya que,</em><strong>dispondrá de aproximadamente cinco (5) minutos para realizar el cambio
                de clave.</strong>
        </p>
        <p><em>Por favor, lea completamente este mensaje antes de proceder a abrir cualquier enlace web.</em></p>
        <p><em>Todos los enlaces web caducan en 30 minutos, desde el momento en que Usted realizó la petición
                de recuperación de acceso, o al hacerse exitosa una recuperación de acceso.</em>
        </p>
        <p>El siguiente enlace abrirá una nueva pestaña que le permitirá establecer una nueva clave de acceso
            al Sistema de Control Base mk:&nbsp;<a href='<?= $url ?>' target='_blank'>CAMBIAR CLAVE</a>
        </p>
        <p>Si el enlace anterior no funciona, seleccione y copie la siguiente url:</p>
        <samp><?= $url ?></samp>
        <p>A continuación péguela en la barra de direcciones del navegador <em>en una nueva pestaña</em> y presione
            la tecla Intro/Enter para continuar.
        </p>
        <p>El SCBmk le mostrará un formulario con un único campo para que ingrese su nueva clave de acceso.<strong> Esta
            nueva clave debe tener, como mínimo, ocho (8) caracteres de longitud, de los cuales y obligatoriamente:
            <ul>
                <li>un caracter debe ser una letra minúscula,</li>
                <li>otro caracter debe ser una letra mayúscula,</li>
                <li>otro caracter debe ser un número</li>
                <li> y otros tres caracteres deben ser símbolos (!?*-.,/...)</li>
            </ul></strong>
        </p>
        <p>Una vez ingresada la nueva clave de acceso (<em>puede hacer clic en el icono del candado para mostrar la clave
            que Ud. ha escrito, haga clic nuevamente en el candado para ocultarla</em>), presione/haga clic en el botón
            CAMBIAR. El sistema procesará el cambio y le informará del resultado del mismo. Si el resultado fue exitoso,
            su nueva clave de acceso ha sido establecida. En caso contrario tendrá que volver a solicitar una nueva
            recuperación de acceso al sistema.
        </p>
        <p>Se le recuerda que dispone de aprox. cinco (5) minutos para realizar el cambio de clave. Transcurrido este
            tiempo, el SCBmk mostrará la pantalla de inicio de sesión. Si todavía no han transcurrido 30 minutos desde
            que realizó esta petición de recuperación de acceso, puede volver a usar el enlace web anterior para
            intentar nuevamente el proceso de recuperación.
        </p>
        <p>Agradecido por ser usuario del SCBmk y por haber leído completamente estas instrucciones. Tenga Usted un
            excelente día.
        </p>
    </div>
</body>

</html>