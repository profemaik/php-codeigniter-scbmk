<?= $this->extend('system/auth/base_auth_form_layout') ?>

<?= $this->section('extra_meta_tags') ?>
    <meta name="description" content="Instalación del Sistema de Control Base mk">
<?= $this->endSection() ?>

<?= $this->section('head_title') ?>
    <title>SCBmk - Preparar el Sistema</title>
<?= $this->endSection() ?>

<?= $this->section('bg_class_image') ?>bg-setup-image<?= $this->endSection() ?>

<?= $this->section('p5_text') ?>
    <p>Preparar el Sistema</p>
<?= $this->endSection() ?>

<?= $this->section('p5_text') ?>
    <p>Recuerde verificar los requisitos del sistema antes de comenzar.</p>
<?= $this->endSection() ?>

<?= $this->section('auth_form_id') ?>Fm_setup_sys<?= $this->endSection() ?>

<?= $this->section('auth_form_action_buttons') ?>
    <button type="submit" class="btn btn-primary btn-user btn-block btn_action"
        data-form="#Fm_setup_sys" data-url="<?= route_to('install_system') ?>"
        data-type="json" data-method="post" data-button="btn_install">
      Instalar
    </button>
    <button type="submit" class="btn btn-danger btn-user btn-block btn_action"
        data-form="#Fm_setup_sys" data-url="<?= route_to('reinstall_system') ?>"
        data-type="json" data-method="post" data-button="btn_reinstall">
      Reinstalar
    </button>
<?= $this->endSection() ?>

<?= $this->section('auth_bottom_extra_content') ?>
    <hr>
    <div class="text-center">
        <a class="small" href="<?= base_url() ?>">Ir a Inicio</a>
    </div>
<?= $this->endSection() ?>
