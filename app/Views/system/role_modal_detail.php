
<div id="modal_alert_parent"></div>
<nav>
  <div class="nav nav-pills flex-column flex-sm-row" id="nav-tab" role="tablist">
    <a class="flex-sm-fill text-sm-center nav-link active" id="nav-role-tab" data-toggle="tab" href="#nav-role" role="tab" aria-controls="nav-role" aria-selected="true">Rol</a>
    <a class="flex-sm-fill text-sm-center nav-link" id="nav-auth-tab" data-toggle="tab" href="#nav-auth" role="tab" aria-controls="nav-auth" aria-selected="false">Permisos</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-role" role="tabpanel" aria-labelledby="nav-role-tab">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Nombre del rol: <strong><?= esc($role->name) ?></strong></li>
        <li class="list-group-item">Descripción breve: <?= esc($role->summary) ?></li>
        <li class="list-group-item">Creado: <?= esc($role->created_at->humanize(), 'attr') ?> [<?= esc($role->created_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]</li>
        <li class="list-group-item">Desactivado: <?php if ($role->deleted_at) : ?><?= esc($role->deleted_at->humanize(), 'attr') ?> [<?= esc($role->deleted_at->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm a VVVV"), 'attr') ?>]<?php endif ?></li>
    </ul>
  </div>
  <div class="tab-pane fade" id="nav-auth" role="tabpanel" aria-labelledby="nav-auth-tab">
    <ul class="list-group list-group-flush">
        <li class="list-group-item list-group-item-dark">Permisos específicos del Rol</li>
        <?php foreach ($permission as $rule) : ?>
            <?php if ($rule->selected) : ?>
                <li class="list-group-item"><?= esc($rule->name) ?></li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
  </div>
</div>