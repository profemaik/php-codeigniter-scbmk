<?= $this->extend('system/home') ?>

<?= $this->section('page_content') ?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $this->renderSection('card_header_title') ?></h6>
        </div>
        <div class="card-body">
            <div class="alert-container" id="alert_parent" style="display: none;"></div>
            <form id="<?= $this->renderSection('form_id') ?>" <?= $this->renderSection('form_attr') ?> novalidate>
                <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">
                <?= $this->renderSection('form_content') ?>
            </form>
        </div>
    </div>
<?= $this->endSection() ?>
