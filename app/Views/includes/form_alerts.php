<div class="alert alert-primary" role="alert" id="<?= '' //esc(ALERT_TYPE_PRIMARY, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-secondary" role="alert" id="<?= '' //esc(ALERT_TYPE_SECONDARY, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-success" role="alert" id="<?= esc(ALERT_TYPE_SUCCESS, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-danger" role="alert" id="<?= esc(ALERT_TYPE_DANGER, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-warning" role="alert" id="<?= esc(ALERT_TYPE_WARNING, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-info" role="alert" id="<?= esc(ALERT_TYPE_INFO, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-light" role="alert" id="<?= '' //esc(ALERT_TYPE_LIGHT, 'attr') ?>" style="display: none;"></div>
<div class="alert alert-dark" role="alert" id="<?= '' //esc(ALERT_TYPE_DARK, 'attr') ?>" style="display: none;"></div>