<div class="form-group row d-flex justify-content-between bg-light border border-light rounded mt-4">
    <div>
        <?php if (session('is_super') === 't' || stripos(json_encode(session('user_nav_item_codenames')),'delete_') !== false) : ?>
        <button type="submit" class="btn btn-danger btn-icon-split m-1 btn_action"
            data-form="<?= esc($formId, 'attr') ?>"
            data-url="<?= esc(route_to($urlBtnDelete), 'attr') ?>"
            data-type="json" data-method="post" data-button="btn_delete">
            <span class="icon"><i class="fas fa-trash"></i></span>
            <span class="text">Eliminar permanentemente</span>
        </button>
        <?php endif ?>
        <?php if (session('is_super') === 't' || stripos(json_encode(session('user_nav_item_codenames')),'disable_') !== false) : ?>
        <button type="submit" class="btn btn-secondary btn-icon-split m-1 btn_action"
            data-form="<?= esc($formId, 'attr') ?>"
            data-url="<?= esc(route_to($urlBtnDisable), 'attr') ?>"
            data-type="json" data-method="post" data-button="btn_disable">
            <span class="icon"><i class="fas fa-ban"></i></span>
            <span class="text">Deshabilitar</span>
        </button>
        <?php endif ?>
        <?php if (session('is_super') === 't' || stripos(json_encode(session('user_nav_item_codenames')),'enable_') !== false) : ?>
        <button type="submit" class="btn btn-success btn-icon-split m-1 btn_action"
            data-form="<?= esc($formId, 'attr') ?>"
            data-url="<?= esc(route_to($urlBtnEnable), 'attr') ?>"
            data-type="json" data-method="post" data-button="btn_enable">
            <span class="icon"><i class="fas fa-check"></i></span>
            <span class="text">Habilitar</span>
        </button>
        <?php endif ?>
    </div>
    <?= $this->include('includes/actions_save') ?>
</div>