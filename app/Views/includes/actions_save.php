<div>
    <?php if (session('is_super') === 't' || stripos(json_encode(session('user_nav_item_codenames')),'add_') !== false) : ?>
    <button type="submit" class="btn btn-primary btn-icon-split m-1 btn_action"
        data-form="<?= esc($formId, 'attr') ?>"
        data-url="<?= esc(route_to($urlBtnSaveanother), 'attr') ?>"
        data-type="json" data-method="post" data-button="btn_saveanother">
        <span class="icon text-white-50"><i class="fas fa-check"></i></span>
        <span class="text">Guardar y añadir otro</span>
    </button>
    <?php endif ?>
    <?php if (session('is_super') === 't' || stripos(json_encode(session('user_nav_item_codenames')),'change_') !== false) : ?>
    <button type="submit" class="btn btn-primary btn-icon-split m-1 btn_action"
        data-form="<?= esc($formId, 'attr') ?>"
        data-url="<?= esc(route_to($urlBtnSaveNedit), 'attr') ?>"
        data-type="json" data-method="post" data-button="btn_savedit">
        <span class="icon text-white-50"><i class="fas fa-check"></i></span>
        <span class="text">Guardar y continuar editando</span>
    </button>
    <?php endif ?>
    <?php if (session('is_super') === 't' || stripos(json_encode(session('user_nav_item_codenames')),'add_') !== false) : ?>
    <button type="submit" class="btn btn-primary btn-icon-split m-1 btn_action"
        data-form="<?= esc($formId, 'attr') ?>"
        data-url="<?= esc(route_to($urlBtnSave), 'attr') ?>"
        data-type="json" data-method="post" data-button="btn_save">
        <span class="icon"><i class="fas fa-save"></i></span>
        <span class="text">Guardar</span>
    </button>
    <?php endif ?>
</div>
