<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

/**
 * Valida la sesión del usuario.
 *
 * @author    Maikel Carballo carballom26@gmail.com.
 * @copyright Maikel carballo 2020.
 */
class UserSession implements FilterInterface
{
    //--------------------------------------------------------------------

    /**
     * Valida si el usuario inició sesión en el sistema.
     *
     * Para cualquier petición (get, post, ajax, etc) se valida
     * si la variable de sesión "login" existe y si su valor es "correct".
     *
     * @param RequestInterface|\CodeIgniter\HTTP\IncomingRequest $request
     * @return CodeIgniter\HTTP\RedirectResponse Redirección HTTP
     **/
    public function before(RequestInterface $request, $arguments = null)
    {
        if (session('login') !== 'correct')
        {
            return redirect('signin');
        }

        if (session('is_psw_set') !== 't')
        {
            return redirect('display_change_password_form');
        }
    }

    /**
     * Por los momentos, no hay nada que hacer aquí.
     *
     * @param RequestInterface|\CodeIgniter\HTTP\IncomingRequest $request
     * @param ResponseInterface|\CodeIgniter\HTTP\Response       $response
     **/
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    { }
}
