<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

/**
 * Valida el método de la petición.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright Maikel Carballo 2020.
 */
class Method implements FilterInterface
{
    //--------------------------------------------------------------------

    /**
     * Verifica el método de la petición HTTP.
     *
     * @param \CodeIgniter\HTTP\IncomingRequest $request
     * @return void
     * @throws Exception
     **/
    public function before(RequestInterface $request, $arguments = null)
    {
        if (! $request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    /**
     * Por los momentos, no hay nada que hacer aquí.
     *
     * @param RequestInterface|\CodeIgniter\HTTP\IncomingRequest $request
     * @param ResponseInterface|\CodeIgniter\HTTP\Response       $response
     **/
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
    }
}
