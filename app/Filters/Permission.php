<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

/**
 * Valida si el usuario puede accesar a la url solicitada.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright Maikel Carballo 2020.
 */
class Permission implements FilterInterface
{
    /**
     * Consulta en la bd si existe el permiso necesario.
     *
     * @param \CodeIgniter\HTTP\IncomingRequest $request
     * @return void
     * @throws PageNotFoundException
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        $path = $request->uri->getPath();

        $permissionModel = new \App\Models\PermisssionModel();

        if (! $permissionModel->isPermitted($path))
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
    }
}
