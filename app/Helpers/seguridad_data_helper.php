<?php

/**
 * Ayudante para depurar los datos enviados desde peticiones HTTP.
 *
 * Contiene funciones que permiten limpiar/depurar/
 * preprocesar/preparar los datos recibidos desde los formularios
 * antes de ser almacenados en el sistema.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019 Maikel Carballo.
 * @since     octubre 30 del 2019.
 * @since     diciembre 23 del 2019 OBSOLETO.
 * @deprecated 0.2.0
 */

helper('text');

if (!function_exists('obten_texto_puro')) {
    /**
     * Permite cualquier tipo de caracter.
     *
     * No depura los datos, útil para campos de texto editables por el usuario.
     * Elimina los campos no prefijados con "txr_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos en bruto (no depurados).
     */
    function obten_texto_puro(array $campos): array
    {
        if (is_array($campos) && !empty($campos)) {
            foreach ($campos as $clave => &$valor) {
                $prefijo = strpos($clave, 'txr_');

                if ($prefijo === 0) {
                    if (is_array($valor)) {
                        foreach ($valor as &$textoPuro) {
                            $textoPuro = $textoPuro;
                        }
                    } else {
                        $valor = $valor;
                    }
                } else {
                    unset($campos[$clave]);
                }
            }
            unset($valor);
        }

        return $campos;
    }
}

if (!function_exists('obten_texto')) {
    /**
     * Depura los datos tipo string.
     *
     * Elimina los campos no prefijados con "tx_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos depurados.
     */
    function obten_texto(array $campos): array
    {
        $patrones = array('/\d[^A-Za-z[0-9] ¿?!¡;:,\*\+%\$#()&._-]/ui');

        foreach ($campos as $clave => &$valor) {
            $prefijo = strpos($clave, 'tx_');

            if ($prefijo === 0) {
                if (is_array($valor)) {
                    foreach ($valor as &$texto) {
                        $texto = preg_replace($patrones, '', filter_var(trim($texto), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
                    }
                } else {
                    $valor = preg_replace($patrones, '', filter_var(trim($valor), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
                }
            } else {
                unset($campos[$clave]);
            }
        }
        unset($valor);

        foreach ($campos as $clave => &$valor) {
            $valor = reduce_multiples($valor, ' ', true);
        }
        unset($valor);

        return $campos;
    }
}

if (!function_exists('obten_fecha')) {
    /**
     * Depura los strings permitiendo solamente carecteres válidos para expresar
     * fechas.
     *
     * Elimina los campos no prefijados con "fe_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos depurados.
     */
    function obten_fecha(array $campos): array
    {
        $patrones = array('/[^0-9\/\-.]/u');

        foreach ($campos as $clave => &$valor) {
            $prefijo = strpos($clave, 'fe_');

            if ($prefijo === 0) {
                if (is_array($valor)) {
                    foreach ($valor as &$fecha) {
                        $fecha = preg_replace($patrones, '', filter_var(trim($fecha), FILTER_SANITIZE_STRING));
                    }
                    unset($fecha);
                } else {
                    $valor = preg_replace($patrones, '', filter_var(trim($valor), FILTER_SANITIZE_STRING));
                }
            } else {
                unset($campos[$clave]);
            }
        }
        unset($valor);

        foreach ($campos as $clave => &$valor) {
            $valor = reduce_multiples($valor, ' ', true);
        }
        unset($valor);

        return $campos;
    }
}

if (!function_exists('obten_hora')) {
    /**
     * Depura un string permitiendo solamente caracteres válidos para expresar tiempo.
     *
     * Elimina los campos no prefijados con "ho_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos depurados.
     */
    function obten_hora(array $campos): array
    {
        $patrones = array('/[^0-9 :.apm]/ui');

        foreach ($campos as $clave => &$valor) {
            $prefijo = strpos($clave, 'ho_');

            if ($prefijo === 0) {
                if (is_array($valor)) {
                    foreach ($valor as &$hora) {
                        $hora = preg_replace($patrones, '', filter_var(trim($hora), FILTER_SANITIZE_STRING));
                    }
                    unset($hora);
                } else {
                    $valor = preg_replace($patrones, '', filter_var(trim($valor), FILTER_SANITIZE_STRING));
                }
            } else {
                unset($campos[$clave]);
            }
        }
        unset($valor);

        foreach ($campos as $clave => &$valor) {
            $valor = reduce_multiples($valor, ' ', true);
        }
        unset($valor);

        return $campos;
    }
}

if (!function_exists('obten_email')) {
    /**
     * Depura un string permitiendo solamente caracteres válidos para expresar emails.
     *
     * Elimina los campos no prefijados con "ce_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos depurados.
     */
    function obten_email(array $campos): array
    {
        $patrones = array('/\d[^A-Za-z[0-9] ¿?!¡;:,\*\+%\$#()&._-~@"<>\[\]\'=\\]/ui');

        foreach ($campos as $clave => &$valor) {
            $prefijo = strpos($clave, 'ce_');

            if ($prefijo === 0) {
                if (is_array($valor)) {
                    foreach ($valor as &$email) {
                        $email = preg_replace($patrones, '', filter_var(trim($email), FILTER_SANITIZE_EMAIL));
                    }
                    unset($email);
                } else {
                    $valor = preg_replace($patrones, '', filter_var(trim($valor), FILTER_SANITIZE_EMAIL));
                }
            } else {
                unset($campos[$clave]);
            }
        }
        unset($valor);

        foreach ($campos as $clave => &$valor) {
            $valor = reduce_multiples($valor, ' ', true);
        }
        unset($valor);

        return $campos;
    }
}

if (!function_exists('obten_entero')) {
    /**
     * Depura un string permitiendo solamente caracteres para expresar valores
     * numéricos enteros.
     *
     * Elimina los campos no prefijados con "ne_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos depurados.
     */
    function obten_entero(array $campos): array
    {
        $patrones = array('/[^0-9\+\-]/u');

        foreach ($campos as $clave => &$valor) {
            $prefijo = strpos($clave, 'ne_');

            if ($prefijo === 0) {
                if (is_array($valor)) {
                    foreach ($valor as &$numero) {
                        $numero = (int) preg_replace($patrones, '', filter_var(trim($numero), FILTER_SANITIZE_NUMBER_INT));
                    }
                    unset($numero);
                } else {
                    $valor = (int) preg_replace($patrones, '', filter_var(trim($valor), FILTER_SANITIZE_NUMBER_INT));
                }
            } else {
                unset($campos[$clave]);
            }
        }
        unset($valor);

        return $campos;
    }
}

if (!function_exists('obten_decimal')) {
    /**
     * Depura un string permitiendo solamente caracteres para expresar valores
     * numéricos decimales.
     *
     * Elimina los campos no prefijados con "nd_".
     *
     * @param array $campos Datos del formulario.
     * @since noviembre 15 del 2019.
     *
     * @return array $campos Datos depurados.
     */
    function obten_decimal(array $campos): array
    {
        $patrones = array('/[^0-9.,eE\+\-]/u');

        foreach ($campos as $clave => &$valor) {
            $prefijo = strpos($clave, 'nd_');

            if ($prefijo === 0) {
                if (is_array($valor)) {
                    foreach ($valor as &$numero) {
                        $numero = (float) preg_replace(
                            $patrones,
                            '',
                            filter_var(
                                trim($numero),
                                FILTER_SANITIZE_NUMBER_FLOAT,
                                FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND | FILTER_FLAG_ALLOW_SCIENTIFIC
                            )
                        );
                    }
                    unset($numero);
                } else {
                    $valor = (float) preg_replace(
                        $patrones,
                        '',
                        filter_var(
                            trim($valor),
                            FILTER_SANITIZE_NUMBER_FLOAT,
                            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND | FILTER_FLAG_ALLOW_SCIENTIFIC
                        )
                    );
                }
            } else {
                unset($campos[$clave]);
            }
        }
        unset($valor);

        return $campos;
    }
}

if (!function_exists('depura_forma')) {
    /**
     * Depura los datos de los formularios.
     *
     * La depuración se realiza en base a los siguientes tipos de datos:
     * texto puro/raw; texto básico; fechas; horas y minutos; email; números enteros
     * y decimales.
     *
     * @param array $campos Datos del formulario.
     *
     * @return array $campos Datos depurados.
     */
    function depura_forma(array $campos): array
    {
        if (is_array($campos) && !empty($campos)) {
            $arrTexto     = obten_texto($campos);
            $arrTextoPuro = obten_texto_puro($campos);
            $arrFecha     = obten_fecha($campos);
            $arrHora      = obten_hora($campos);
            $arrEmail     = obten_email($campos);
            $arrEnteros   = obten_entero($campos);
            $arrReales    = obten_decimal($campos);

            $campos = array_merge($arrTexto, $arrTextoPuro, $arrFecha, $arrHora, $arrEmail, $arrEnteros, $arrReales);

            unset($arrTexto, $arrTextoPuro, $arrFecha, $arrHora, $arrEmail, $arrEnteros, $arrReales);
        }

        return $campos;
    }
}

if (!function_exists('a_campos_bd')) {
    /**
     * Renombra los campos de un formulario a los campos de una tabla.
     *
     * @param array $forma Arreglo POST/GET de un formulario.
     * @since noviembre 30 del 2019.
     *
     * @return array $forma Arreglo con los índices renombrados.
     */
    function a_campos_bd($forma = []): array
    {
        $afijos = ['/txr_/', '/tx_/', '/fe_/', '/ho_/', '/ce_/', '/ne_/', '/nd_/', '/ne_/', '/xfxxf/'];
        $remplazos = ['', '', '', '', '', '', '', '', 'id'];

        foreach ($forma as $indice => $valor) {
            $nuevoIndice = preg_replace($afijos, $remplazos, $indice);

            if (is_int($nuevoIndice) || is_string($nuevoIndice)) {
                if (array_key_exists($indice, $forma)) {
                    $forma[$nuevoIndice] = $forma[$indice];
                    unset($forma[$indice]);
                }
            }
        }

        return $forma;
    }
}
