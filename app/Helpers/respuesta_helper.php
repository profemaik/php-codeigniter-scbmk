<?php

/**
 * Ayudante para las alertas o respuestas del sistema en los formularios.
 *
 * Este ayudante contiene funciones que permiten devolver a los JS
 * de las vistas los mensajes de respuesta a las interacciones que haga el usuario
 * en dichas vistas.
 * Todos los mensajes son devueltos en formato JSON, los JS de las vistas
 * se encargan de procesarlos y mostrar el/los mensajes respectivos.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019 Maikel Carballo.
 * @since     octubre 22 2019.
 * @deprecated 0.2.0
 */

if (! function_exists('prepara_validacion')) {
    /**
     * Captura los errores de validación detectados.
     *
     * Se verifica qué campo del formulario tiene error de validación, si lo tiene,
     * se establece el mensaje del error correspondiente para el campo verificado.
     *
     * @param array $formulario Arreglo POST/GET en bruto.
     * @since noviembre 07 del 2019.
     *
     * @return array $formulario Arreglo POST/GET con los mensajes de error detectados.
     */
    function prepara_validacion(array $formulario): array
    {
        $validacion = \Config\Services::validation();
        //$validacion->

        foreach ($formulario as $campo => &$valor) {
            if ($validacion->hasError($campo)) {
                $valor = $validacion->getError($campo);
            } else {
                $valor = '';
            }
        }
        unset($valor);

        return $formulario;
    }
}

if (! function_exists('validacion_forma')) {
    /**
     * Devuelve la respuesta JSON de la validación de un formulario.
     *
     * @param array $formulario Arreglo POST/GET.
     * @param array $errores    Arreglo de los errores encontrados.
     * @since diciembre 14 del 2019.
     *
     * @return string JSON.
     */
    function validacion_forma(array $formulario, array $errores): string
    {
        $diferencia = array_diff_assoc($formulario, $errores);

        foreach ($diferencia as $key => &$value) {
            $value = '';
        }
        unset($value);

        $camposValidados = array_merge($diferencia, $errores);

        return json_encode([
            'alert'   => 'error',
            'title'   => 'ERRORES DETECTADOS',
            'message' => 'Por favor,corrija los campos <b>resaltados en rojo</b> e intente nuevamente.',
            'code'    => EXIT_USER_INPUT,
            'campos'  => $camposValidados,
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('transaccion_exito')) {
    /**
     * Devuelve la respuesta JSON para transacciones ejecutadas correctamente.
     *
     * @param array $formulario Arreglo POST/GET.
     * @since diciembre 14 del 2019.
     *
     * @return string JSON.
     */
    function transaccion_exito($formulario = []): string
    {
        foreach ($formulario as $campo => &$valor) {
            $valor = '';
        }
        unset($valor);
        session()->remove('id_chosen_entity');

        return json_encode([
            'alert'   => 'success',
            'title'   => 'ÉXITO',
            'message' => 'Acción ejecutada correctamente.',
            'code'    => 0,
            'campos'  => $formulario,
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('transaccion_error')) {
    /**
     * Devuelve la respuesta JSON para transacciones no ejecutadas en la BD.
     *
     * @param string $msg Mensaje de la Base de Datos.
     *
     * @since diciembre 14 del 2019.
     *
     * @return string JSON.
     */
    function transaccion_error(?array $msg): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'ERROR',
            'message' => $msg ? $msg['message'] : 'Ha fallado la transacción. Por favor, intente nuevamente. Si este error persiste, contacte a Sistemas',
            'code'    => EXIT_DATABASE,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('sin_modificaciones_info')) {
    /**
     * Devuelve la respuesta JSON para cuando los datos de un registro no han sido modificados.
     *
     * @since diciembre 14 del 2019.
     *
     * @return string JSON.
     */
    function sin_modificaciones_info(): string
    {
        session()->remove('id_chosen_entity');

        return json_encode([
            'alert'   => 'info',
            'title'   => 'INFORMACIÓN',
            'message' => 'No se han detectado cambios en los datos.',
            'code'    => 0,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('supresion_error')) {
    /**
     * Devuelve la respuesta JSON para cuando falla la supresión de un registro.
     *
     * @since diciembre 16 del 2019.
     *
     * @return string JSON.
     */
    function supresion_error(): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'ERROR',
            'message' => 'Acción no ejecutada. No se recibieron los datos esperados.',
            'code'    => 0,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('validacion_formulario')) {
    /**
     * Devuelve la respuesta JSON en caso de fallo en la validación de un formulario.
     *
     * @param array $campos Campos del formulario con los errores detectados.
     * @since noviembre 08 del 2019.
     * @since diciembre 19 del 2019 Refactorización completa, con cambio de nombre.
     *
     * @return string JSON.
     */
    function validacion_formulario(array $campos): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'ERRORES DETECTADOS',
            'message' => 'Por favor,corrija los campos resaltados en rojo e intente nuevamente.',
            'code'    => EXIT_USER_INPUT,
            'campos'  => $campos,
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('validacion_sesion')) {
    /**
     * Devuelve la respuesta JSON en caso de fallo en la validación de la sesión.
     *
     * @param array $campos Campos del formulario con los errores detectados.
     * @since noviembre 08 del 2019.
     * @since diciembre 19 del 2019 Refactorización completa, con cambio de nombre.
     *
     * @return string JSON.
     */
    function validacion_sesion(): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'Sesión inválida.&nbsp;<i class="fa fa-user-times"></i>',
            'message' => 'Hay algo inusual en esta sesión, por seguridad el sistema se reiniciará.',
            'code'    => 20,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('respuesta_filtro_json')) {
    /**
     * Devuelve la respuesta JSON de las validaciones de los filtros definidos en App.
     *
     * @param string  $titulo  Encabezado del SweetAlert2.
     * @param string  $mensaje Cuerpo del SweetAlert2.
     * @param integer $estatus Número entero para indicar si es una alerta con salida automática.
     * @param string  $alerta  Tipo de icono de la alerta SweetAlert2.
     * @since noviembre 24 del 2019.
     *
     * @return string JSON.
     */
    function respuesta_filtro_json(string $titulo, string $mensaje, int $estatus, $alerta = 'error'): string
    {

        return json_encode([
            'alert'     =>  $alerta,
            'title'     =>  $titulo,
            'message'   =>  $mensaje,
            'status'    =>  $estatus
        ]);
    }
}

if (! function_exists('respuesta_recupera_acceso_ok')) {
    /**
     * Devuelve la respuesta JSON de la recuperación satisfactoria del acceso de un usuario.
     *
     * @since enero 01 del 2020.
     *
     * @return string JSON.
     */
    function respuesta_recupera_acceso_ok(): string
    {

        return json_encode([
            'alert'   => 'success',
            'title'   => 'Recuperar acceso',
            'message' => 'Una nueva contraseña ha sido enviada a su correo electrónico, úsela para ingresar al sistema.',
            'code'    => 0,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('respuesta_recupera_acceso_err_bd')) {
    /**
     * Devuelve la respuesta JSON de la recuperación fallida debido a la bd del sistema.
     *
     * @since enero 01 del 2020.
     *
     * @return string JSON.
     */
    function respuesta_recupera_acceso_err_bd(): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'Recuperar acceso falló',
            'message' => 'La acción solicitada no se pudo procesar. Por favor, intente más tarde.',
            'code'    => EXIT_DATABASE,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('respuesta_recupera_acceso_err_envio')) {
    /**
     * Devuelve la respuesta JSON de la recuperación fallida debido al envío del mensaje.
     *
     * @since enero 01 del 2020.
     *
     * @return string JSON.
     */
    function respuesta_recupera_acceso_err_envio(): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'Recuperar acceso falló',
            'message' => 'La acción solicitada no se pudo procesar. Por favor, intente más tarde.',
            'code'    => EXIT_ERROR,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('respuesta_recupera_acceso_usuario')) {
    /**
     * Devuelve la respuesta JSON de la recuperación fallida debido a que el usuario no existe.
     *
     * @since enero 01 del 2020.
     *
     * @return string JSON.
     */
    function respuesta_recupera_acceso_usuario(): string
    {

        return json_encode([
            'alert'   => 'error',
            'title'   => 'Usuario inválido',
            'message' => 'El usuario no existe.',
            'code'    => EXIT_ERROR,
            'campos'  => [],
            'xxx'     => csrf_hash()
        ]);
    }
}

if (! function_exists('resultado_json')) {
    function resultado_json($resultado, ?array $errores, array $data)
    {
        if ($resultado === false && (! array_key_exists('message', $errores))) {
            return validacion_forma($data, $errores);
        } elseif ($errores && array_key_exists('message', $errores)) {
            return transaccion_error($errores);
        } else {
            return transaccion_exito($data);
        }
    }
}

if (! function_exists('respuesta_en_json')) {
    /**
     * Devuelve el JSON para un tipo de respuesta.
     *
     * @param mixed      $resultado
     * @param array|null $errores
     * @param array      $data
     * @since enero 17 del 2020.
     *
     * @return string JSON.
     */
    function respuesta_en_json($resultado = null, ?array $errores = [], $data = [])
    {
        if ($resultado !== null) {
            return resultado_json($resultado, $errores, $data);
        } else {
            return sin_modificaciones_info();
        }
    }
}
