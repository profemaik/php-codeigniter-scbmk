<?php
/**
 * Ayudante para los elementos visuales del sistema con los que el usuario
 * puede interactuar (menús, migajas, botones/grupos de botones, etc.)
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>
 * @copyright 2020 Maikel Carballo
 * @since     0.2.0
 */

if (! function_exists('nav_menu'))
{
    /**
     * Devuelve el menú de navegación del sistema para el usuario que ha
     * iniciado sesión.
     *
     * @return string HTML del menú construido.
     */
    function nav_menu(): string
    {
        return (session('is_super') === 't')
               ? full_nav_menu()
               : permitted_nav_menu();
    }
}

//--------------------------------------------------------------------

if (! function_exists('permitted_nav_menu'))
{
    /**
     * Construye el menú de navegación del sistema según los permisos del
     * usuario que ha iniciado sesión.
     *
     * @return string HTML del menú construido.
     */
    function permitted_nav_menu(): string
    {
        $navbar = '';

        foreach (session('sys_nav_items') as $menu)
        {
            if (in_array($menu['iid'], session('user_nav_item_ids')))
            {
                $navbar .= '<hr class="sidebar-divider">' . "\n";
                $navbar .= '      <div class="sidebar-heading">' . $menu['iname'] . '</div>' . "\n";
                if ($menu['nav_lvl_1'])
                {
                    foreach ($menu['nav_lvl_1'] as $lvlm1)
                    {
                        if (in_array($lvlm1['iid'], session('user_nav_item_ids')))
                        {
                            if ($lvlm1['icontroller'] !== '#')
                            {
                                $navbar .= '      <li class="nav-item">' . "\n";
                                $navbar .= '        <a class="nav-link" href="' . base_url($lvlm1['icontroller']) . '">' . "\n";
                                $navbar .= '          <i class="fas fa-fw ' . $lvlm1['iicon'] . '"></i>' . "\n";
                                $navbar .= '          <span>' . $lvlm1['iname'] . '</span></a>' . "\n";
                                $navbar .= '      </li>' . "\n";
                            }
                            else
                            {
                                $navbar .= '      <li class="nav-item">' . "\n";
                                $navbar .= '        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse' . $lvlm1['iname'] . '" aria-expanded="true" aria-controls="collapse' . $menu['iname'] . '">' . "\n";
                                $navbar .= '          <i class="fas fa-fw ' . $lvlm1['iicon'] . '"></i>' . "\n";
                                $navbar .= '          <span>' . $lvlm1['iname'] . '</span>' . "\n";
                                $navbar .= '        </a>' . "\n";
                                $navbar .= '        <div id="collapse' . $lvlm1['iname'] . '" class="collapse" aria-labelledby="heading' . $lvlm1['iname'] . '" data-parent="#accordionSidebar">' . "\n";
                                if ($lvlm1['nav_lvl_2'])
                                {
                                    $navbar .= '          <div class="bg-white py-2 collapse-inner rounded">' . "\n";
                                    foreach ($lvlm1['nav_lvl_2'] as $lvlm2)
                                    {
                                        if (in_array($lvlm2['iid'], session('user_nav_item_ids')))
                                        {
                                            $navbar .= '            <h6 class="collapse-header">' . $lvlm2['iname'] . ':</h6>' . "\n";
                                            if ($lvlm2['nav_lvl_3'])
                                            {
                                                foreach ($lvlm2['nav_lvl_3'] as $lvlm3)
                                                {
                                                    if (in_array($lvlm3['iid'], session('user_nav_item_ids')))
                                                    {
                                                        $navbar .= '            <a class="collapse-item" href="' . base_url($lvlm3['icontroller']) . '">' . $lvlm3['iname'] . '</a>' . "\n";
                                                    }
                                                }
                                                $navbar .= '            <div class="collapse-divider"></div>' . "\n";
                                            }
                                        }
                                    }
                                    $navbar .= '          </div>';
                                }
                                $navbar .= '        </div>';
                                $navbar .= '      </li>';
                            }
                        }
                    }
                }
            }
        }

        return $navbar;
    }
}

//--------------------------------------------------------------------

if (! function_exists('full_nav_menu'))
{
    /**
     * Construye el menú de navegación del sistema sin restricciones.
     *
     * *Usado para superusuarios.*
     * @return string HTML del menú construido.
     */
    function full_nav_menu(): string
    {
        $navbar = '';

        foreach (session('sys_nav_items') as $menu)
        {
            $navbar .= '<hr class="sidebar-divider">' . "\n";
            $navbar .= '      <div class="sidebar-heading">' . $menu['iname'] . '</div>' . "\n";
            if ($menu['nav_lvl_1'])
            {
                foreach ($menu['nav_lvl_1'] as $lvlm1)
                {
                    if ($lvlm1['icontroller'] !== '#')
                    {
                        $navbar .= '      <li class="nav-item">' . "\n";
                        $navbar .= '        <a class="nav-link" href="' . base_url($lvlm1['icontroller']) . '">' . "\n";
                        $navbar .= '          <i class="fas fa-fw ' . $lvlm1['iicon'] . '"></i>' . "\n";
                        $navbar .= '          <span>' . $lvlm1['iname'] . '</span></a>' . "\n";
                        $navbar .= '      </li>' . "\n";
                    }
                    else
                    {
                        $navbar .= '      <li class="nav-item">' . "\n";
                        $navbar .= '        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse' . $lvlm1['iname'] . '" aria-expanded="true" aria-controls="collapse' . $menu['iname'] . '">' . "\n";
                        $navbar .= '          <i class="fas fa-fw ' . $lvlm1['iicon'] . '"></i>' . "\n";
                        $navbar .= '          <span>' . $lvlm1['iname'] . '</span>' . "\n";
                        $navbar .= '        </a>' . "\n";
                        $navbar .= '        <div id="collapse' . $lvlm1['iname'] . '" class="collapse" aria-labelledby="heading' . $lvlm1['iname'] . '" data-parent="#accordionSidebar">' . "\n";
                        if ($lvlm1['nav_lvl_2'])
                        {
                            $navbar .= '          <div class="bg-white py-2 collapse-inner rounded">' . "\n";
                            foreach ($lvlm1['nav_lvl_2'] as $lvlm2)
                            {
                                $navbar .= '            <h6 class="collapse-header">' . $lvlm2['iname'] . ':</h6>' . "\n";
                                if ($lvlm2['nav_lvl_3'])
                                {
                                    foreach ($lvlm2['nav_lvl_3'] as $lvlm3)
                                    {
                                        $navbar .= '            <a class="collapse-item" href="' . base_url($lvlm3['icontroller']) . '">' . $lvlm3['iname'] . '</a>' . "\n";
                                    }
                                    $navbar .= '            <div class="collapse-divider"></div>' . "\n";
                                }
                            }
                            $navbar .= '          </div>';
                        }
                        $navbar .= '        </div>';
                        $navbar .= '      </li>';
                    }
                }
            }
        }

        return $navbar;
    }
}

//--------------------------------------------------------------------

if (! function_exists('user_name'))
{
    /**
     * Devuelve una cadena de caractes que identifica, de manera amigable, al
     * usuario.
     *
     * @return string El nombre de usuario o nombres y apellidos reales.
     */
    function user_name(): string
    {
        $username = session('username');

        if (session()->get('names'))
        {
            $username = session('names');
        }
        if (session()->get('lastnames') && session()->get('names'))
        {
            $username .=  ' ' . session('lastnames');
        }
        if (session()->get('lastnames') && ! session()->get('names'))
        {
            $username = session('lastnames');
        }

        return $username;
    }
}

//--------------------------------------------------------------------

if (! function_exists('user_picture'))
{
    /**
     * Devuelve la url relativa a usar para mostrar la imagen del usuario.
     *
     * @return string
     */
    function user_picture(): ?string
    {
        $avatar = session()->get('avatar');
        $photo  = session()->get('photo');

        if ($avatar)
        {
            return base_url($avatar);
        }
        if ($photo)
        {
            return base_url($photo);
        }

        switch (session()->get('gender'))
        {
            case 'F':
                return base_url('assets/imgs/users/default/undraw_profile_1.svg');
                break;
            case 'M':
                return base_url('assets/imgs/users/default/undraw_profile_2.svg');
                break;

            default:
                return base_url('assets/imgs/users/default/undraw_profile.svg');
                break;
        }
    }
}

//--------------------------------------------------------------------

if (! function_exists('bg_color_css'))
{
    /**
     * Dado el id de un action flag, devuelve su respectivo color css.
     *
     * @param integer $flag ACTION_FLAG_*
     * @return string Color css Bootstrap 4 (primary, secondary, etc.).
     */
    function bg_color_css(int $flag)
    {
        if ($flag > 8 || $flag < 1)
        {
            return 'white';
        }

        return BG_COLOR_CSS[$flag];
    }
}