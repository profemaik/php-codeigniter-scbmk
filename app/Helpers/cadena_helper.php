<?php

/**
 * Ayudante para los datos de tipo string.
 *
 * Este ayudante contiene funciones que permiten preprocesar
 * datos de tipo string antes de ser almacenados en el sistema
 * o mostrados en las vistas al usuario.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019 Maikel Carballo.
 * @since     octubre 29 2019.
 * @deprecated 0.2.0
 */

if (! function_exists('user_name')) {
    /**
     * Añade @dominio.com a la cadena en caso de no existir.
     *
     * @param string $nombreUsuario Cadena ingresada por la persona.
     * @param string $dominio       Dominio del correo electrónico
     * @since diciembre 14 del 2019 Refactorizado para sanear $nombreUsuario.
     *
     * @return string Nombre de usuario completo (nombre-usuario@ente.com)
     */
    function user_name(string $nombreUsuario, $dominio = '@ente.com'): string
    {
        $arrcadena = explode('@', filter_var(trim($nombreUsuario), FILTER_SANITIZE_EMAIL));

        if (count($arrcadena) == 1 && ! empty($arrcadena[0])) {
            return $arrcadena[0] . $dominio;
        } elseif (count($arrcadena) == 2) {
            return $arrcadena[0] . '@' . $arrcadena[1];
        }
        unset($arrcadena);

        return filter_var(trim($nombreUsuario), FILTER_SANITIZE_EMAIL);
    }
}

if (! function_exists('formatoCedula')) {
    /**
     * Devuelve una cadena en formato XX.XXX.XXX .
     *
     * @param string $cedula
     * @since enero 09 del 2020.
     *
     * @return string
     */
    function formatoCedula(string $cedula): string
    {
        $unidad    = substr($cedula, -3, 3);
        $decena    = substr($cedula, -6, 3);
        $longuitud = strlen($cedula);

        if ($longuitud > 6) {
            $resto = ($longuitud % 3);

            switch ($resto) {
                case 2:
                    $centena = substr($cedula, 0, 1);
                    break;
                case 1:
                    $centena = substr($cedula, 0, -6);
                    break;
                case 0:
                    $centena = substr($cedula, 0, -6);
                    break;
            }

            $cedula = $centena . '.' . $decena . '.' . $unidad;
        }

        if ($longuitud == 6) {
            $centena = '';
            $cedula = $centena . $decena . '.' . $unidad;
        }

        return (string) $cedula;
    }
}
