<?php

if (! function_exists('breadcrumbs')) {
    /**
     * Obtiene las migas de pan a mostrarse en una vista.
     *
     * @param string $route En formato **camelCase**.
     * @return string JSON de las migas encontradas.
     */
    function breadcrumbs(string $route): string
    {
        $bd = \Config\Database::connect('kernel');
        $crumbs = $bd->table('menu_item_routes')
                     ->select('txt_route')
                     ->where('route', item_menu_route($route))
                     ->get()->getRowObject();
        if ($crumbs) {
            $crumbs = $crumbs->txt_route;
            return json_encode(['migajas' => format_crumbs($crumbs)]);
        } else {
            return json_encode(['migajas' => format_crumbs()]);
        }
    }
}

if (! function_exists('item_menu_route')) {
    /**
     * Devuelve la ruta de un item del menú.
     *
     * Ejemplos:
     * * App\Controllers\Persona -> persona
     * * App\Controllers\ButtonStyle -> estiloDeBoton
     *
     * @param string $className Nombre cualificado.
     * @return string En formato camelCase.
     */
    function item_menu_route($className = ''): string
    {
        if ($pos = strrpos($className, '\\')) {
            $className = lcfirst(substr($className, $pos + 1));
        }
        return (string) $className;
    }
}

if (! function_exists('format_crumbs')) {
    /**
     * Aplica el formato html.
     *
     * @param string $crumbs miga0 > miga1 > miga2
     * @return string
     */
    function format_crumbs($crumbs = ''): string
    {
        $crumbs = explode('>', $crumbs);
        switch (count($crumbs)) {
            case 3:
                return '<li><a href="#">'.trim($crumbs[0]).'</a></li>' . "\n"
                    .  '<li><a href="#">'.trim($crumbs[1]).'</a></li>' . "\n"
                    .  '<li><span>'.trim($crumbs[2]).'</span></li>';
                break;
            case 2:
                return '<li><a href="#">'.trim($crumbs[0]).'</a></li>' . "\n"
                    .  '<li><span>'.trim($crumbs[1]).'</span></li>';
                break;
            case 1:
                return '<li><a href="#">'.trim($crumbs[0]).'</a></li>';
                break;
            default:
                return '<li><a href="#"></a></li>';
                break;
        }
    }
}

if (! function_exists('toolbar')) {
    /**
     * Devuelve la barra de comandos para una vista.
     *
     * Los botones visibles dependen de los permisos asignados al rol del usuario.
     *
     * @param string  $route
     * @return string HTML.
     */
    function toolbar(string $route): string
    {
        $buttons = search_granted_commands(item_menu_route($route));
        return make_toolbar($buttons);
    }
}

if (! function_exists('search_granted_commands')) {
    /**
     * Consulta y recupera los comandos disponibles para una vista.
     *
     * @param string $route Nombre del archivo controlador.
     * @return array
     */
    function search_granted_commands(string $route): array
    {
        $db = \Config\Database::connect('kernel');

        $sql = 'SELECT 	CASE WHEN bs.tagged IS NOT TRUE THEN \'\' ELSE cmd.noun END, cmd.icon, cmd.abstract, cmd.html_id, cmd.color,
                        bs.shape, bs.outline, bs.format
                FROM 	commands cmd
                    JOIN button_styles bs ON bs.id = cmd.button_style
                    JOIN permisssions perm ON perm.command = cmd.id
                WHERE 	perm.rol = ?
                    AND perm.menu_item = (select mi.id from menu_items mi where mi.route = ?)
                    AND perm.deleted_at ISNULL
                    AND cmd.id != ?
                    AND cmd.deleted_at ISNULL
                ORDER BY cmd.sort';
        return $db->query($sql, [session('idrol_user'), $route, 1])->getResultArray();
    }
}
if (! function_exists('make_toolbar')) {
    /**
     * Crea el código HTML para un conjunto de botones dados.
     *
     * Índices que debe contener $buttons:
     * * noun
     * * icon
     * * abstract
     * * html_id
     * * color
     * * shape
     * * outline
     * * format
     *
     * @param array $buttons
     * @return string HTML
     */
    function make_toolbar($buttons = []): string
    {
        $toolbar = '';
        foreach ($buttons as $btn) {
            $toolbar .= '<button type="button" class="'.$btn['shape'].' '.$btn['outline'].$btn['color'].' '.$btn['format']
            .           '" id="'.$btn['html_id'].'"'.'title="'.$btn['abstract'].'" style="font-family: Poppins, sans-serif">'
            .           '<span class="'.$btn['icon'].'" style="font-size:16px"></span>&nbsp;'. $btn['noun']
            .           '</button>'. "\n";
        }
        return $toolbar;
    }
}