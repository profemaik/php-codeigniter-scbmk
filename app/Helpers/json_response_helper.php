<?php
/**
 * Ayudante para devolver contenido codificado en formato JSON en las
 * respuestas HTTP que así lo requieran.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>
 * @copyright 2020 Maikel Carballo
 * @since     0.2.0
 */

if (! function_exists('json_response_validation'))
{
    /**
     * Devuelve un arreglo codificado en json para mostrar la alerta y el
     * resultado de la validación de un formulario.
     *
     * @param array  $errors   Campos del formulario con errores.
     * @param string $alertMsg Cadena a mostrar como mensaje de la alerta.
     * @return string|false JSON o false si falló json_encode().
     */
    function json_response_validation(array $errors, $alertMsg = '')
    {
        $responseContent = [
            'alert' => [
                'type' => ALERT_TYPE_DANGER,
                'title' => 'Formulario incorrecto...',
                'msg' => $alertMsg ?: 'Por favor, verifique los campos resaltados en color rojo.',
            ],
            'wrong_fields' => $errors,
            'bsod' => [
                'csrf_header' => csrf_header(),
                'csrf_token' => csrf_token(),
                'csrf_hash' => csrf_hash(),
            ],
        ];

        return json_encode($responseContent);
    }
}

if (! function_exists('json_response_success'))
{
    /**
     * Devuelve un arreglo codificado en json para mostrar la alerta y el
     * mensaje de éxito de la petición.
     *
     * @param string  $alertMsg   Cadena a mostrar como mensaje de la alerta.
     * @param string  $uri        URI hacia donde redirigir.
     * @return string|false JSON o false si falló json_encode().
     */
    function json_response_success($alertMsg = '', $uri = '')
    {
        $responseContent = [
            'alert' => [
                'type' => ALERT_TYPE_SUCCESS,
                'title' => 'Éxito',
                'msg' => $alertMsg ?: 'Solicitud procesada correctamente.',
            ],
            'wrong_fields' => [],
            'url' => $uri ?: base_url(),
            'bsod' => [
                'csrf_header' => csrf_header(),
                'csrf_token' => csrf_token(),
                'csrf_hash' => csrf_hash(),
            ],
        ];

        return json_encode($responseContent);
    }
}

if (! function_exists('json_response_error'))
{
    /**
     * Devuelve un arreglo codificado en json para mostrar la alerta y el
     * mensaje de error de la petición.
     *
     * @param array  $errors   Campos del formulario con errores.
     * @param string $alertMsg Cadena a mostrar como mensaje de la alerta.
     * @return string|false JSON o false si falló json_encode().
     */
    function json_response_error($alertMsg = '')
    {
        $responseContent = [
            'alert' => [
                'type' => ALERT_TYPE_DANGER,
                'title' => 'Error',
                'msg' => $alertMsg ?: 'Solicitud no procesada.',
            ],
            'wrong_fields' => [],
            'bsod' => [
                'csrf_header' => csrf_header(),
                'csrf_token' => csrf_token(),
                'csrf_hash' => csrf_hash(),
            ],
        ];

        return json_encode($responseContent);
    }
}

if (! function_exists('json_response_info'))
{
    /**
     * Devuelve un arreglo codificado en json para mostrar la alerta y el
     * mensaje de información de la petición.
     *
     * @param string $alertMsg Cadena a mostrar como mensaje de la alerta.
     * @param string $uri      URI hacia donde redirigir.
     * @return string|false JSON o false si falló json_encode().
     */
    function json_response_info($alertMsg = '', $uri = '')
    {
        $responseContent = [
            'alert' => [
                'type' => ALERT_TYPE_INFO,
                'title' => 'Información',
                'msg' => $alertMsg ?: 'Solicitud no reconocida.',
            ],
            'wrong_fields' => [],
            'url' => $uri ?: base_url(),
            'bsod' => [
                'csrf_header' => csrf_header(),
                'csrf_token' => csrf_token(),
                'csrf_hash' => csrf_hash(),
            ],
        ];

        return json_encode($responseContent);
    }
}
