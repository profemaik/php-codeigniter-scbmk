<?php

/**
 * This file is part of the CodeIgniter 4 framework.
 *
 * (c) CodeIgniter Foundation <admin@codeigniter.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

// Time language settings
return [
	'invalidMonth'   => 'Los meses deben estar entre 1 y 12. Establecido: {0}',
	'invalidDay'     => 'Los días deben estar entre 1 y 31. Establecido: {0}',
	'invalidOverDay' => 'Los días deben estar entre 1 y {0}. Establecido: {1}',
	'invalidHours'   => 'Las horas deben estar entre 0 y 23. Establecido: {0}',
	'invalidMinutes' => 'Los minutos deben estar entre 0 y 59. Establecido: {0}',
	'invalidSeconds' => 'Los segundos deben estar entre 0 y 59. Establecido: {0}',
	'years'          => '{0, plural, =1{# año} other{# años}}',
	'months'         => '{0, plural, =1{# mes} other{# meses}}',
	'weeks'          => '{0, plural, =1{# semana} other{# semanas}}',
	'days'           => '{0, plural, =1{# día} other{# dias}}',
	'hours'          => '{0, plural, =1{# hora} other{# horas}}',
	'minutes'        => '{0, plural, =1{# minuo} other{# minutos}}',
	'seconds'        => '{0, plural, =1{# segundo} other{# segundos}}',
	'ago'            => '{0} atrás',
	'inFuture'       => 'en {0}',
	'yesterday'      => 'Ayer',
	'tomorrow'       => 'Mañana',
	'now'            => 'Ahora',
];
