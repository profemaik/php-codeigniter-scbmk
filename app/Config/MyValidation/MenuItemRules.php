<?php

namespace Config\MyValidation;

use Config\Database;

/**
 * Validaciones de los formularios que gestionan los ítems del menú.
 *
 * @author     Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright  2020 Maikel Carballo.
 */
class MenuItemRules
{
    public function is_mi_name_unique(string $noun, string $anc, array $data): bool
    {
        $bd = Database::connect($data['DBGroup'] ?? null);
        $row = $bd->table('menu_item_routes')
                   ->select('1')
                   ->where('lower(noun)', mb_strtolower(trim($noun)))
                   ->limit(1);
        $anc = $this->ancestor($data);
        if ($anc) {
            $row->where('ancestor', $anc);
        } else {
            $row->where('ancestor');
        }
        if (! empty(session('id_chosen_entity'))) {
			$row = $row->where('id !=', (int) session('id_chosen_entity'));
        }
        return (bool) ($row->get()->getRow() === null);
    }

    private function ancestor(array $data = [])
    {
        if (! isset($data['ancestor'])) {
            if (isset($data['id'])) {
                $bd = Database::connect($data['DBGroup'] ?? null);
                return $bd->table('menu_item_routes')
                           ->select('ancestor')
                           ->where('id', $data['id'])
                           ->limit(1)
                           ->get()->getRowObject()->ancestor;
            }
            return null;
        }
        if (isset($data['ancestor']) && ! empty($data['ancestor'])) {
            return $data['ancestor'];
        } else {
            return null;
        }
    }

    public function exists_mi_name(string $anc, string $name, array $data): bool
    {
        $bd = Database::connect($data['DBGroup'] ?? null);
        $row = $bd->table('menu_item_routes');

        if (! array_key_exists('noun', $data)) {
            $noun = $row->select('noun')->where('id', $data['id'])->get()->getRowObject();
            $noun = $noun->noun;
        } else {
            $noun = trim($data[$name]);
        }

        $row = $row->select('1')
                   ->where('lower(noun)', mb_strtolower($noun))
                   ->where('ancestor', $anc)
                   ->limit(1);
        if (! empty(session('id_chosen_entity'))) {
			$row = $row->where('id !=', (int) session('id_chosen_entity'));
        }
        return (bool) ($row->get()->getRow() === null);
    }

    public function is_mi_icon_unique(string $icon, string $lvl, array $data): bool
    {
        $bd = Database::connect($data['DBGroup'] ?? null);
        $fila = $bd->table('menu_item_routes')
                   ->select('1')
                   ->where('icon', trim($icon))
                   ->where('lvl', $lvl)
                   ->limit(1);
        if (! empty(session('id_chosen_entity'))) {
            $fila = $fila->where('id !=', (int) session('id_chosen_entity'));
        }
        return (bool) ($fila->get()->getRow() === null);
    }

    public function is_mi_route_valid(?string $str)
    {
        if (empty($str)) {
            return true;
        }
        if (! preg_match('/^([a-z]+)(?=[A-Za-z])|#/', $str)) {
			return false;
        }
        return true;
    }
}
