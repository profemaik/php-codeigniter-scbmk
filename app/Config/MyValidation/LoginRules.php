<?php namespace Config\MyValidation;

use App\Models\UserDataModel;

/**
 * Validaciones del formulario de inicio de sesión.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 * @since     0.2.0
 */
class LoginRules
{
    /**
     * Verifica si existe el usuario registrado en el sistema.
     *
     * @param string $email Nombre de usuario, teléfono o correo electrónico.
     * @return boolean
     */
    public function is_user(string $email): bool
    {
        $user = new UserDataModel();

        return $user->isUser($email);
    }

    /**
     * Verifica si la contraseña y el usuario dados en el formulario de inicio
     * de sesión coinciden con algún registro de usuario en la base de datos.
     *
     * @param string $psw   Contraseña.
     * @param string $email Nombre de usuario, teléfono o correo electrónico.
     * @param array  $form  Todos los datos enviados en el formulario.
     * @param string $error Mensaje a mostrar en caso de error.
     * @return boolean
     */
    public function psw_match(string $psw, string $email, array $form, string &$error = null): bool
    {
        $user = new UserDataModel();
        $user = $user->getUserHash($form[$email]);

        if (! is_null($user))
        {
            $error = 'Esta Contraseña es incorrecta.';

            return password_verify($psw, $user->password);
        }

        $error = 'Esta Contraseña no se pudo verificar.';

        return false;
    }
}
