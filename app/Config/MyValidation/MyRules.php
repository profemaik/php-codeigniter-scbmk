<?php

namespace Config\MyValidation;

use Beeflow\Passwd\Passwd;

/**
 * Reglas de validación aplicables a cualquier formulario del sistema.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019 Maikel Carballo.
 */
class MyRules
{
	/**
	 * Permite saber si una cadena es un icono web válido.
	 *
	 * Para este sistema son válidos los siguientes tipos de iconos:
	 * * Themify
	 * * Font Awesome
	 *
	 * @param string $str
	 * @return bool
	 */
	public function is_web_icon(string $str): bool
	{
		if (empty($str)) {
			return true;
		}
		$webIconPattern = '/^(fa\sfa\-|fas\sfa\-|ti\-){1,1}([a-z]){2,}([a-z0-9\-])*/';
		if (preg_match($webIconPattern, $str)) {
			return true;
		}
		return false;
	}

	public function old_pswd_match(string $str): bool
	{
		if (password_verify($str, session('psw_user'))) {
			return true;
		} else {
			return false;
		}
	}

	public function valid_psw(string $str, string &$error = null): bool
	{
		$pswPolicy = [
			'lowerCharsCount' => 1,
			'upperCharsCount' => 1,
			'specialCharsCount' => 3,
			'numbersCount' => 1,
			'minimumPasswordLength' => 8
		];
		$password = new Passwd($pswPolicy);
		if (! $password->check($str))
		{
			$error = 'La Contraseña es ';
			$error .= $password->getStrengthInfo();

			return false;
		}

		return true;
	}
}
