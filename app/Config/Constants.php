<?php

/*
 | --------------------------------------------------------------------
 | App Namespace
 | --------------------------------------------------------------------
 |
 | This defines the default Namespace that is used throughout
 | CodeIgniter to refer to the Application directory. Change
 | this constant to change the namespace that all application
 | classes should use.
 |
 | NOTE: changing this will require manually modifying the
 | existing namespaces of App\* namespaced-classes.
 */
defined('APP_NAMESPACE') || define('APP_NAMESPACE', 'App');

/*
 | --------------------------------------------------------------------------
 | Composer Path
 | --------------------------------------------------------------------------
 |
 | The path that Composer's autoload file is expected to live. By default,
 | the vendor folder is in the Root directory, but you can customize that here.
 */
defined('COMPOSER_PATH') || define('COMPOSER_PATH', ROOTPATH . 'vendor/autoload.php');

/*
 |--------------------------------------------------------------------------
 | Timing Constants
 |--------------------------------------------------------------------------
 |
 | Provide simple ways to work with the myriad of PHP functions that
 | require information to be in seconds.
 */
defined('SECOND') || define('SECOND', 1);
defined('MINUTE') || define('MINUTE', 60);
defined('HOUR')   || define('HOUR', 3600);
defined('DAY')    || define('DAY', 86400);
defined('WEEK')   || define('WEEK', 604800);
defined('MONTH')  || define('MONTH', 2592000);
defined('YEAR')   || define('YEAR', 31536000);
defined('DECADE') || define('DECADE', 315360000);

/*
 | --------------------------------------------------------------------------
 | Exit Status Codes
 | --------------------------------------------------------------------------
 |
 | Used to indicate the conditions under which the script is exit()ing.
 | While there is no universal standard for error codes, there are some
 | broad conventions.  Three such conventions are mentioned below, for
 | those who wish to make use of them.  The CodeIgniter defaults were
 | chosen for the least overlap with these conventions, while still
 | leaving room for others to be defined in future versions and user
 | applications.
 |
 | The three main conventions used for determining exit status codes
 | are as follows:
 |
 |    Standard C/C++ Library (stdlibc):
 |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
 |       (This link also contains other GNU-specific conventions)
 |    BSD sysexits.h:
 |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
 |    Bash scripting:
 |       http://tldp.org/LDP/abs/html/exitcodes.html
 |
 */
defined('EXIT_SUCCESS')        || define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          || define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         || define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   || define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  || define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     || define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       || define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      || define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      || define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| Alertas para Respuestas HTTP
|--------------------------------------------------------------------------
|
| Usadas en las peticiones AJAX e indican el tipo de alerta que se debe
| mostrar en el HTML.
*/
defined('ALERT_TYPE_PRIMARY')   || define('ALERT_TYPE_PRIMARY', 'primary');
defined('ALERT_TYPE_SECONDARY') || define('ALERT_TYPE_SECONDARY', 'secondary');
defined('ALERT_TYPE_SUCCESS')   || define('ALERT_TYPE_SUCCESS', 'success');
defined('ALERT_TYPE_DANGER')    || define('ALERT_TYPE_DANGER', 'error');
defined('ALERT_TYPE_WARNING')   || define('ALERT_TYPE_WARNING', 'warning');
defined('ALERT_TYPE_INFO')      || define('ALERT_TYPE_INFO', 'info');
defined('ALERT_TYPE_LIGHT')     || define('ALERT_TYPE_LIGHT', 'light');
defined('ALERT_TYPE_DARK')      || define('ALERT_TYPE_DARK', 'dark');

/*
|--------------------------------------------------------------------------
| Códigos para los indicadores de acciones (action flags)
|--------------------------------------------------------------------------
|
| Usadas para indicar el tipo de acción ejecutado en el sistema.
*/
defined('ACTION_FLAG_VIEW')    || define('ACTION_FLAG_VIEW', 1);
defined('ACTION_FLAG_CHANGE')  || define('ACTION_FLAG_CHANGE', 2);
defined('ACTION_FLAG_DISABLE') || define('ACTION_FLAG_DISABLE', 3);
defined('ACTION_FLAG_ENABLE')  || define('ACTION_FLAG_ENABLE', 4);
defined('ACTION_FLAG_DELETE')  || define('ACTION_FLAG_DELETE', 5);
defined('ACTION_FLAG_ADD')     || define('ACTION_FLAG_ADD', 6);
defined('ACTION_FLAG_LOGIN')   || define('ACTION_FLAG_LOGIN', 7);
defined('ACTION_FLAG_LOGOUT')  || define('ACTION_FLAG_LOGOUT', 8);

/*
|--------------------------------------------------------------------------
| Géneros sociales
|--------------------------------------------------------------------------
|
| Usadas para definir los géneros sociales más comunes.
*/
defined('SOCIAL_GENDER') ||
define('SOCIAL_GENDER', [
                         ''  => 'Sin especificar',
                         'X' => 'Ser Humano',
                         'F' => 'Femenino',
                         'M' => 'Masculino',
                        ]
);

/*
|--------------------------------------------------------------------------
| Condición de Ciudadanía
|--------------------------------------------------------------------------
|
| Usadas para definir el tipo de ciudadanía de los usuarios.
*/
defined('NATIONALITIES') ||
define('NATIONALITIES', [
                         ''  => 'Sin especificar',
                         'V' => 'Venezolano',
                         'E' => 'Extranjero',
                        ]
);

/*
|--------------------------------------------------------------------------
| Colores CSS estándares de Bootstrap 4
|--------------------------------------------------------------------------
*/
defined('BG_COLOR_CSS') ||
define('BG_COLOR_CSS', [
                        6 => 'primary',
                        3 => 'secondary',
                        4 => 'success',
                        5 => 'error',
                        2 => 'warning',
                        7 => 'info',
                        8 => 'dark',
                        1 => 'light',
                       ]
);
