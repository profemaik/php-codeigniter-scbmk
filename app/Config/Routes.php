<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('autenticarse', 'Home::displayLoginForm', ['as' => 'signin']);
$routes->get('salir', 'Home::logout', ['as' => 'logout']);
$routes->get('cambiar-clave-de-acceso', 'Home::displayChangePasswordForm', ['as' => 'display_change_password_form']);
$routes->post('change-password', 'Home::setUserPassword', ['as' => 'change_password']);
$routes->post('login', 'Home::login', ['as' => 'login']);
$routes->get('mi-historial-de-actividades', 'Home::displayUserLog', ['as' => 'display_full_user_log']);
$routes->get('show-dtb-userlog', 'ActivityLog::index', ['as' => 'display_dtb_user_log']);

$routes->get('preparar-sistema', 'Setup::index', ['as' => 'setup_system']);
$routes->post('setup/install', 'Setup::install', ['as' => 'install_system']);
$routes->post('setup/reinstall', 'Setup::reinstall', ['as' => 'reinstall_system']);

$routes->get('recuperar-clave-de-acceso', 'AccessRecovery::index', ['as' => 'display_recover_psw_petition_form']);
$routes->get('verificar-cambio-de-clave-de-acceso', 'AccessRecovery::checkRecovery', ['as' => 'check_psw_change_request']);
$routes->post('recover-password', 'AccessRecovery::recoveryRequest', ['as' => 'recover_password']);

$routes->group('sistema', function ($routes)
{
	$routes->group('usuarios', function ($routes)
	{
		$routes->get('/', 'System\User::index', ['as' => 'users']);
		$routes->get('desactivados', 'System\User::index/true', ['as' => 'all_users']);
		$routes->get('ver-usuario/1', 'System\User::displayUserDetails', ['as' => 'dtb_display_user_details']);
		$routes->get('agregar-usuario', 'System\User::displayAddUserForm', ['as' => 'display_add_user_form']);
		$routes->get('modificar-usuario', 'System\User::displayChangeUserForm', ['as' => 'display_change_user_form']);
		$routes->get(
			'modificar-usuario/2', 'System\User::displayChangeUserFormFromDtb', ['as' => 'dtb_display_change_user_form']
		);
		$routes->post('save-new', 'System\User::processSavingUserNew', ['as' => 'save_new_user']);
		$routes->post('save-modified', 'System\User::processSavingUserChanges', ['as' => 'save_modified_user']);
		$routes->post('enable-user', 'System\User::enableUser', ['as' => 'enable_user']);
		$routes->post('enable-user/4', 'System\User::enableUser/4', ['as' => 'dtb_enable_user']);
		$routes->post('disable-user', 'System\User::disableUser', ['as' => 'disable_user']);
		$routes->post('disable-user/3', 'System\User::disableUser/3', ['as' => 'dtb_disable_user']);
		$routes->post('delete-user', 'System\User::deleteUser', ['as' => 'delete_user']);
		$routes->post('delete-user/5', 'System\User::deleteUser/5', ['as' => 'dtb_delete_user']);
		$routes->post('upload-image', 'System\User::processUserImageUpload', ['as' => 'save_image_user']);
	});
	$routes->group('roles', function ($routes)
	{
		$routes->get('/', 'System\Role::index', ['as' => 'roles']);
		$routes->get('desactivados', 'System\Role::index/true', ['as' => 'all_roles']);
		$routes->get('ver-rol/1', 'System\Role::displayRoleDetails', ['as' => 'dtb_display_role_details']);
		$routes->get('agregar-rol', 'System\Role::displayAddRoleForm', ['as' => 'display_add_role_form']);
		$routes->get('modificar-rol', 'System\Role::displayChangeRoleForm', ['as' => 'display_change_role_form']);
		$routes->get(
			'modificar-rol/2', 'System\Role::displayChangeRoleFormFromDtb', ['as' => 'dtb_display_change_role_form']
		);
		$routes->post('save-new', 'System\Role::processSavingRoleNew', ['as' => 'save_new_role']);
		$routes->post('save-modified', 'System\Role::processSavingRoleChanges', ['as' => 'save_modified_role']);
		$routes->post('enable-role', 'System\Role::enableRole', ['as' => 'enable_role']);
		$routes->post('enable-role/4', 'System\Role::enableRole/4', ['as' => 'dtb_enable_role']);
		$routes->post('disable-role', 'System\Role::disableRole', ['as' => 'disable_role']);
		$routes->post('disable-role/3', 'System\Role::disableRole/3', ['as' => 'dtb_disable_role']);
		$routes->post('delete-role', 'System\Role::deleteRole', ['as' => 'delete_role']);
		$routes->post('delete-role/5', 'System\Role::deleteRole/5', ['as' => 'dtb_delete_role']);
	});
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
