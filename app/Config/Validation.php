<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var string[]
	 */
	public $ruleSets = [
		Rules::class,
		FormatRules::class,
		FileRules::class,
		CreditCardRules::class,
		\Config\MyValidation\LoginRules::class,
		\Config\MyValidation\MenuItemRules::class,
		\Config\MyValidation\MyRules::class
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array<string, string>
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		//'single' => 'CodeIgniter\Validation\Views\single',
		'single' => 'errors/error_simple',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
	/**
	 * Validaciones de la vista alfa.
	 *
	 * @var array
	 */
	public $login = [
		'email' => [
			'label'	 => 'Usuario',
			'rules'	 => 'required|is_user',
			'errors' => [
				'is_user' => 'Este usuario no existe.'
			]
		],
		'passwd' => [
			'label'	 => 'Contraseña',
			'rules'	 => 'required|string|min_length[8]|psw_match[email]',
		]
	];

	/**
	 * Validaciones de la vista permisos.
	 *
	 * @var array
	 */
	public $permissions = [
        'rol' => [
            'label' => 'Rol',
			'rules' => 'required|is_natural_no_zero',
        ],
        'menu_item' => [
            'label' => 'Item del Menú',
            'rules' => 'required|is_natural_no_zero'
        ]
	];
	public $changePsw = [
		'password' => [
			'label'  => 'Contraseña',
			'rules'  => 'required|string|min_length[8]|matches[password2]|valid_psw',
			'errors' => [
				'valid_psw' => 'El campo {field} es incorrecto.'
			]
			],
			'password2' => [
				'label'  => 'Repita Contraseña',
				'rules'  => 'required|string|min_length[8]|matches[password]|valid_psw',
				'errors' => [
					'valid_psw' => 'El campo {field} es incorrecto.'
				]
			]
	];

	public $recoverPsw = [
		'username' => [
			'label'	 => 'Usuario',
			'rules'	 => 'required|is_user',
			'errors' => [
				'is_user' => 'Este usuario no existe.'
			]
		]
	];

	public $addUser = [
		'id_card_num' => [
			'label' => 'Número de CI',
			'rules' => 'required|regex_match[/^[0-9]{1,3}\.[0-9]{3,3}\.[0-9]{3,3}$/]|is_unique[user_personal_data.id_card_num]',
		],
		'username' => [
			'label' => 'Nombre de Usuario',
			'rules' => 'required|string|min_length[3]|is_unique[user_data.username]',
		],
	];

	public $changeUser = [
		'nationality' => [
            'label' => 'Nacionalidad',
			'rules' => 'permit_empty|alpha|exact_length[1]|regex_match[/[ve]/i]'
        ],
        'id_card_num' => [
            'label' => 'Número de CI',
			'rules' => 'required|regex_match[/^[0-9]{1,3}\.[0-9]{3,3}\.[0-9]{3,3}$/]|is_unique[user_personal_data.id_card_num,id_card_num,{id_card_num}]'
        ],
        'names' => [
            'label' => 'Nombres',
			'rules' => 'permit_empty|string|min_length[3]'
        ],
        'lastnames' => [
            'label' => 'Apellidos',
            'rules' => 'permit_empty|string|min_length[3]'
        ],
        'gender' => [
            'label' => 'Género',
			'rules' => 'permit_empty|alpha|exact_length[1]|regex_match[/[odmhx]/i]'
        ],
        'emails.main' => [
            'label' => 'Correo Electrónico Principal',
            'rules' => 'permit_empty|valid_email'
        ],
        'emails.aux' => [
            'label' => 'Correo Electrónico Auxiliar',
            'rules' => 'permit_empty|valid_email'
        ],
        'phone_nums.main' => [
            'label' => 'Número Telefónico Principal',
            'rules' => 'permit_empty|string'
        ],
        'phone_nums.aux' => [
            'label' => 'Número Telefónico Auxiliar',
            'rules' => 'permit_empty|string'
        ],
        'photo' => [
            'label' => 'Foto',
            'rules' => 'permit_empty|string'
		],
		'username' => [
            'label' => 'Nombre de Usuario',
			'rules' => 'required|string|is_unique[user_data.username,username,{username}]'
        ],
        'is_super' => [
            'label' => 'Es Superusuario',
			'rules' => 'permit_empty|alpha|max_length[1]|regex_match[/t/i]'
        ],
        'is_admin' => [
            'label' => 'Es Admin',
			'rules' => 'permit_empty|alpha|max_length[1]|regex_match[/t/i]'
        ],
	];
}
