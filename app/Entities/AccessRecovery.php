<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **access_recovery**.
 *
 * Representa la *tupla* de **access_recovery**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>
 * @copyright 2020 Maikel Carballo
 */
class AccessRecovery extends Entity
{
    protected $id;
    protected $username;
    protected $token;
    protected $created_at;
    protected $updated_at;

    protected $cast = [
        'id'         => 'integer',
        'username'   => 'string',
        'token'      => 'string',
        'created_at' => 'timestamp',
        'updated_at' => '?timestamp',
    ];

    protected $attributes = [
        'id'         => null,
        'username'   => null,
        'token'      => null,
        'created_at' => null,
        'updated_at' => null,
    ];

    public function setUserName(string $username)
    {
        $this->attributes['username'] = $username;

        return $this;
    }

    public function setToken(string $token)
    {
        $this->attributes['token'] = $token;

        return $this;
    }
}
