<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **permisssion**
 *
 * Representa la *tupla* de **permisssion**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019-2020 Maikel Carballo.
 */
class Permisssion extends Entity
{
	protected $attributes = [
		'id'          => null,
		'nav_item_id' => null,
		'name'        => '',
		'route'       => '',
		'codename'    => ''
	];

	protected $casts = [
        'id'          => 'integer',
		'nav_item_id' => 'integer',
		'name'        => 'string',
        'route'       => 'string',
		'codename'    => 'string'
	];

	public function setNavItemId(int $id)
	{
		$this->attributes['nav_item_id'] = $id;

		return $this;
	}

	public function setName(string $name)
	{
		$this->attributes['name'] = $name;

		return $this;
	}

	public function setRoute(string $route)
	{
		$this->attributes['route'] = $route;

		return $this;
	}

	public function setCodename(string $str)
	{
		$this->attributes['codename'] = $str;

		return $this;
	}
}
