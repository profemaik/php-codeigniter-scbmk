<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **user_log**.
 *
 * Representa la *tupla* de **user_log**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2021 Maikel Carballo.
 * @since     Version 0.2.0
 */
class UserLog extends Entity
{
    protected $attributes = [
        'id'          => null,
        'user_id'     => null,
        'user_ip'     => null,
        'object_id'   => null,
        'object_repr' => null,
        'object_type' => null,
        'action_flag' => null,
        'message'     => null,
        'created_at'  => null,
    ];

    protected $casts = [
        'id'          => 'integer',
        'user_id'     => 'integer',
        'user_ip'     => 'string',
        'object_id'   => 'integer',
        'object_repr' => 'string',
        'object_type' => 'string',
        'action_flag' => 'integer',
        'message'     => 'string',
        'created_at'  => 'timestamp'
    ];

    protected $dates = ['created_at'];

    public function setUserId(int $userId)
    {
        $this->attributes['user_id'] = $userId;

        return $this;
    }

    public function setUserIp(string $userIp)
    {
        $this->attributes['user_ip'] = $userIp;

        return $this;
    }

    public function setObjectId(int $objectId)
    {
        $this->attributes['object_id'] = $objectId;

        return $this;
    }

    public function setObjectRepr(string $str)
    {
        $this->attributes['object_repr'] = $str;

        return $this;
    }

    public function setObjectType(string $str)
    {
        $this->attributes['object_type'] = $str;

        return $this;
    }

    public function setActionFlag(int $actionId)
    {
        $this->attributes['action_flag'] = $actionId;

        return $this;
    }

    public function setMessage(string $message)
    {
        $this->attributes['message'] = $message;

        return $this;
    }
}
