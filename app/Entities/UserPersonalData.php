<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **user_personal_data**.
 *
 * Representa la *tupla* de **user_personal_data**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UserPersonalData extends Entity
{
    protected $attributes = [
        'id'           => null,
        'user_data_id' => null,
        'nationality'  => '',
        'id_card_num'  => '',
        'names'        => '',
        'lastnames'    => '',
        'gender'       => '',
        'emails'       => '',
        'phone_nums'   => '',
        'photo'        => '',
        'created_at'   => null,
        'updated_at'   => null,
        'deleted_at'   => null,
    ];

    protected $casts = [
        'id'           => 'integer',
        'user_data_id' => 'integer',
        'nationality'  => 'string',
        'id_card_num'  => 'string',
        'names'        => 'string',
        'lastnames'    => 'string',
        'gender'       => 'string',
        'emails'       => 'array',
        'phone_nums'   => 'array',
        'photo'        => 'string',
        'created_at'   => 'timestamp',
        'updated_at'   => '?timestamp',
        'deleted_at'   => '?timestamp',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function setUserDataId(int $userlDataId)
    {
        $this->attributes['user_data_id'] = $userlDataId;

        return $this;
    }

    public function setNationality(string $nationality = '')
    {
        $this->attributes['nationality'] = $nationality;

        return $this;
    }

    public function setIdCardNum(string $idCardNum)
    {
        $this->attributes['id_card_num'] = $idCardNum;

        return $this;
    }

    public function setNames(string $names = '')
    {
        $this->attributes['names'] = $names;

        return $this;
    }

    public function setLastnames(string $lastnames = '')
    {
        $this->attributes['lastnames'] = $lastnames;

        return $this;
    }

    public function setGender(string $gender = '')
    {
        $this->attributes['gender'] = $gender;

        return $this;
    }

    public function setEmails(array $emails = [])
    {
        foreach ($emails as &$email)
        {
            $email = filter_var(trim($email), FILTER_SANITIZE_EMAIL);
        }
        unset($email);
        $this->attributes['emails'] = serialize($emails);

        return $this;
    }

    public function setPhoneNums(array $phoneNums = [])
    {
        foreach ($phoneNums as &$phoneNum)
        {
            $phoneNum = filter_var(trim($phoneNum), FILTER_SANITIZE_STRING);
        }
        unset($phoneNum);
        $this->attributes['phone_nums'] = serialize($phoneNums);

        return $this;
    }

    public function setPhoto(string $path = '')
    {
        $this->attributes['photo'] = $path;

        return $this;
    }
}
