<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **roles_permissions**.
 *
 * Representa la *tupla* de **roles_permissions**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class RolesPermissions extends Entity
{
    protected $attributes = [
        'id'            => null,
        'role_id'       => null,
        'permission_id' => null,
        'created_at'    => null,
        'updated_at'    => null,
        'deleted_at'    => null
    ];

    protected $casts = [
        'id'            => 'integer',
        'role_id'       => 'integer',
        'permission_id' => 'integer',
        'created_at'    => 'timestamp',
        'updated_at'    => '?timestamp',
        'deleted_at'    => '?timestamp'
    ];

    public function setRoleId(int $roleId)
    {
        $this->attributes['role_id'] = $roleId;

        return $this;
    }

    public function setPermissionId(int $permissionId)
    {
        $this->attributes['permission_id'] = $permissionId;

        return $this;
    }
}
