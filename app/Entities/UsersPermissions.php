<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **users_permissions**.
 *
 * Representa la *tupla* de **users_permissions**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UsersPermissions extends Entity
{
    protected $attributes = [
        'id'            => null,
        'user_id'       => null,
        'permission_id' => null,
        'created_at'    => null,
        'updated_at'    => null,
        'deleted_at'    => null
    ];

    protected $casts = [
        'id'            => 'integer',
        'user_id'       => 'integer',
        'permission_id' => 'integer',
        'created_at'    => 'timestamp',
        'updated_at'    => '?timestamp',
        'deleted_at'    => '?timestamp'
    ];

    public function setUserId(int $userId)
    {
        $this->attributes['user_id'] = $userId;

        return $this;
    }

    public function setPermissionId(int $permissionId)
    {
        $this->attributes['permission_id'] = $permissionId;

        return $this;
    }
}
