<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **nav_item**.
 *
 * Representa la *tupla* de **nav_item**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>
 * @copyright 2020 Maikel Carballo.
 */
class NavItem extends Entity
{
    protected $id;
    protected $name;
    protected $controller;
    protected $icon;
    protected $sort;
    protected $ascendant;

    protected $cast = [
        'id'         => 'integer',
        'name'       => 'string',
        'controller' => 'string',
        'icon'       => '?string',
        'sort'       => 'integer',
        'ascendant'  => '?integer'
    ];
    protected $attributes = [
        'id'         => null,
        'name'       => null,
        'controller' => null,
        'icon'       => null,
        'sort'       => null,
        'ascendant'  => null
    ];

    public function setName(string $name)
    {
        $this->attributes['name'] = $name;

        return $this;
    }

    public function setController(string $controller)
    {
        $this->attributes['controller'] = $controller;

        return $this;
    }

    public function setIcon(?string $icon)
    {
        $this->attributes['icon'] = $icon;

        return $this;
    }

    public function setSort(int $num)
    {
        $this->attributes['sort'] = $num;

        return $this;
    }

    public function setAscendant(?string $ascendant)
    {
        $this->attributes['ascendant'] = $ascendant;

        return $this;
    }
}
