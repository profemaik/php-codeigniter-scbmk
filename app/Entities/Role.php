<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **role**.
 *
 * Representa la *tupla* de **role**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019-2020 Maikel Carballo.
 */
class Role extends Entity
{
    protected $attributes = [
		'id'         => null,
		'name'       => null,
		'summary'    => null,
		'created_at' => null,
		'updated_at' => null,
		'deleted_at' => null
	];

	protected $casts = [
        'id'         => 'integer',
        'name'       => 'string',
        'summary'    => '?string',
        'created_at' => 'timestamp',
        'updated_at' => '?timestamp',
        'deleted_at' => '?timestamp'
	];

	public function setName(string $name)
	{
		$this->attributes['name'] = $name;

		return $this;
	}

	public function setSummary(string $summary)
	{
		$this->attributes['summary'] = $summary;

		return $this;
	}
}
