<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **log**.
 *
 * Representa la *tupla* de **log**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class Log extends Entity
{
    protected $id;
    protected $user_id;
    protected $user_ip;
    protected $object_id;
    protected $object_repr;
    protected $action_flag;
    protected $model;
    protected $message;
    protected $created_at;

    protected $casts = [
        'id'          => 'integer',
        'user_id'     => 'integer',
        'user_ip'     => 'string',
        'object_id'   => 'string',
        'object_repr' => 'boolean',
        'action_flag' => 'string',
        'model'       => 'boolean',
        'message'     => 'boolean',
        'created_at'  => 'timestamp'
    ];

    protected $attributes = [
        'id'          => null,
        'user_id'     => null,
        'user_ip'     => null,
        'object_id'   => null,
        'object_repr' => null,
        'action_flag' => null,
        'model'       => null,
        'message'     => null,
        'created_at'  => null
    ];

    public function setUserId(int $userId)
    {
        $this->attributes['user_id'] = $userId;

        return $this;
    }

    public function setUserIp(string $userIp)
    {
        $this->attributes['user_ip'] = $userIp;

        return $this;
    }

    public function setObjectId(int $objectId)
    {
        $this->attributes['object_id'] = $objectId;

        return $this;
    }

    public function setObjectRepr(string $str)
    {
        $this->attributes['object_repr'] = $str;

        return $this;
    }

    public function setActionFlag(int $actionId)
    {
        $this->attributes['action_flag'] = $actionId;

        return $this;
    }

    public function setModel(string $modelName)
    {
        $this->attributes['model'] = $modelName;

        return $this;
    }

    public function setMessage(string $message)
    {
        $this->attributes['message'] = $message;

        return $this;
    }
}
