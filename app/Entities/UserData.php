<?php namespace App\Entities;

use CodeIgniter\Entity;

/**
 * Entidad de la tabla **user_data**.
 *
 * Representa la *tupla* de **user**. Puede implementar:
 * - lógica de negocio
 * - mapeo de datos
 * - mutadores.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UserData extends Entity
{
    protected $attributes = [
        'id'         => null,
        'username'   => null,
        'password'   => null,
        'is_psw_set' => 'false',
        'is_super'   => 'false',
        'is_admin'   => 'false',
        'last_login' => null,
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    protected $casts = [
        'id'         => 'integer',
        'username'   => 'string',
        'password'   => 'string',
        'is_psw_set' => 'boolean',
        'is_super'   => 'string',
        'is_admin'   => 'string',
        'last_login' => '?timestamp',
        'created_at' => 'timestamp',
        'updated_at' => '?timestamp',
        'deleted_at' => '?timestamp',
    ];

    protected $dates = ['last_login', 'created_at', 'updated_at', 'deleted_at'];

    public function setUsername(string $username)
    {
        $this->attributes['username'] = $username;

        return $this;
    }

    public function setPassword(string $password = '12345678')
    {
        $this->attributes['password'] = password_hash($password, PASSWORD_DEFAULT);

        return $this;
    }

    public function setIsPswSet(string $status = 'false')
    {
        $this->attributes['is_psw_set'] = $status;

        return $this;
    }

    public function setIsSuper(string $status = 'f')
    {
        $this->attributes['is_super'] = $status;

        return $this;
    }

    public function setIsAdmin(string $status = 'f')
    {
        $this->attributes['is_admin'] = $status;

        return $this;
    }

    public function setLastLogin(string $timestamp)
    {
        $this->attributes['last_login'] = $timestamp;

        return $this;
    }
}
