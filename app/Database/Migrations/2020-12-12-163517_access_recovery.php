<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AccessRecovery extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'username'   => [
                'type'       => 'VARCHAR',
                'constraint' => '40'
            ],
            'token'      => [
                'type'       => 'VARCHAR',
                'constraint' => '64'
            ],
            'created_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'updated_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ]
        ]);
        $this->forge->createTable('access_recovery');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('access_recovery');
    }
}
