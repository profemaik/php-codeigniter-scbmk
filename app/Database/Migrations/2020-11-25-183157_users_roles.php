<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UsersRoles extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'user_id'    => [
                'type'    => 'INT'
            ],
            'role_id'    => [
                'type'    => 'INT'
            ],
            'created_at' => [
                'type'    => 'TIMESTAMPTZ',
                'default' => 'NOW()'
            ],
            'updated_at' => [
                'type'    => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'deleted_at' => [
                'type'    => 'TIMESTAMPTZ',
                'null'    => true
            ],
        ]);
        $this->forge->addForeignKey('user_id', 'user_data', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('role_id', 'role', 'id', '', 'CASCADE');
        $this->forge->createTable('users_roles');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('users_roles');
    }
}
