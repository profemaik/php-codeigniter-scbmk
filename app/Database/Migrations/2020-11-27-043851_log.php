<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Log extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'user_id'     => [
                'type'    => 'INT'
            ],
            'user_ip'     => [
                'type'    => 'INET'
            ],
            'object_id'   => [
                'type'    => 'INT'
            ],
            'object_repr' => [
                'type'    => 'VARCHAR'
            ],
            'action_flag' => [
                'type'    => 'INT'
            ],
            'model'       => [
                'type'    => 'VARCHAR'
            ],
            'message'     => [
                'type'    => 'VARCHAR'
            ],
            'created_at'  => [
                'type'    => 'TIMESTAMPTZ',
                'default' => 'NOW()'
            ]
        ]);
        $this->forge->addForeignKey('user_id', 'user_data', 'id', '', 'CASCADE');
        $this->forge->createTable('log');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('log');
    }
}
