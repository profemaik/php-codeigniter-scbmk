<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PermissionRoutes extends Migration
{
	public function up()
	{
		$this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY')
					->addField(['permission_id' => ['type' => 'INT']])
					->addField("sys_route VARCHAR NOT NULL CHECK (sys_route <> '')");
        $this->forge->addForeignKey('permission_id', 'permission', 'id', '', 'CASCADE');

		$this->forge->createTable('permission_routes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('permission_routes');
	}
}
