<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UsersPermissions extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'user_id'       => [
                'type'    => 'INT'
            ],
            'permission_id' => [
                'type'    => 'INT'
            ],
            'created_at'    => [
                'type'    => 'TIMESTAMPTZ',
                'default' => 'NOW()'
            ],
            'updated_at'    => [
                'type'    => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'deleted_at'    => [
                'type'    => 'TIMESTAMPTZ',
                'null'    => true
            ]
        ]);
        $this->forge->addForeignKey('user_id', 'user_data', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('permission_id', 'permission', 'id', '', 'CASCADE');
        $this->forge->createTable('users_permissions');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('users_permissions');
    }
}
