<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserLog extends Migration
{
	public function up()
	{
		$this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'user_id'     => [
                'type'    => 'INT'
            ],
            'user_ip'     => [
                'type'    => 'INET'
            ],
            'object_id'   => [
                'type'    => 'INT'
            ],
            'object_repr' => [
                'type'    => 'VARCHAR'
			],
            'object_type' => [
                'type'    => 'VARCHAR'
			],
		]);
		$this->forge->addField("action_flag INT NOT NULL CHECK (action_flag >= 0)");
		$this->forge->addField([
            'message'     => [
                'type'    => 'VARCHAR'
            ],
            'created_at'  => [
                'type'    => 'TIMESTAMPTZ',
                'default' => 'NOW()'
            ]
        ]);
        $this->forge->addForeignKey('user_id', 'user_data', 'id', '', 'CASCADE');
        $this->forge->createTable('user_log');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_log');
	}
}
