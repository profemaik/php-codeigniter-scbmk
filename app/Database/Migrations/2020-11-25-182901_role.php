<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Role extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '40',
                'unique'     => true
            ],
            'summary'    => [
                'type'       => 'VARCHAR',
                'constraint' => '512',
            ],
            'created_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'updated_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'deleted_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ],
        ]);
        $this->forge->createTable('role');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('role');
    }
}
