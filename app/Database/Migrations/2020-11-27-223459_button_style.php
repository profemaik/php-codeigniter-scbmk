<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ButtonStyle extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '30'
            ],
            'summary'    => [
                'type'       => 'VARCHAR',
                'constraint' => '512'
            ],
            'shape'      => [
                'type'       => 'VARCHAR',
                'constraint' => '15'
            ],
            'format'     => [
                'type'       => 'VARCHAR',
                'constraint' => '15'
            ],
            'outline'    => [
                'type'       => 'VARCHAR',
                'constraint' => '15'
            ],
            'is_active'  => [
                'type'       => 'BOOL',
                'default'    => false
            ],
            'has_label'  => [
                'type'       => 'BOOL',
                'null'       => true
            ],
            'created_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'updated_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ],
            'deleted_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ]
        ]);
        $this->forge->createTable('button_style');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('button_style');
    }
}
