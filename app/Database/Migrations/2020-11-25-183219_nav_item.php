<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class NavItem extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '50'
            ],
            'controller' => [
                'type'       => 'VARCHAR',
                'constraint' => '40'
            ],
            'icon'       => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
            ],
            'sort'       => [
                'type'       => 'INT'
            ],
            'ascendant'  => [
                'type'       => 'INT',
                'null'       => true
            ]
        ]);
        $this->forge->addForeignKey('ascendant', 'nav_item', 'id', '', 'CASCADE');
        $this->forge->createTable('nav_item');
        $this->db->query(
            'CREATE OR REPLACE VIEW navitem_view
            WITH (
              security_barrier=true
            ) AS
             WITH RECURSIVE niv(ilevel, iid, iname, icontroller, iicon, isort, iascendant) AS (
                     SELECT 0,
                        nav_item.id,
                        nav_item.name,
                        nav_item.controller,
                        nav_item.icon,
                        nav_item.sort,
                        nav_item.ascendant
                       FROM nav_item
                      WHERE nav_item.ascendant IS NULL
                    UNION
                     SELECT niv.ilevel + 1,
                         nav_item.id,
                         nav_item.name,
                         nav_item.controller,
                         nav_item.icon,
                         nav_item.sort,
                         nav_item.ascendant
                       FROM nav_item
                         JOIN niv ON nav_item.ascendant = niv.iid
                    )
             SELECT niv.ilevel,
                niv.iid,
                niv.iname,
                niv.icontroller,
                niv.iicon,
                niv.isort,
                niv.iascendant
               FROM niv;'
        );
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('nav_item', false, true);
    }
}
