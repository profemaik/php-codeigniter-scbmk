<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Permission extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'nav_item_id' => [
                'type'       => 'INT',
            ],
            'name'        => [
                'type'       => 'VARCHAR',
                'constraint' => '120',
                'unique'     => true
            ],
            'codename'    => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'unique'     => true
            ]
        ]);
        $this->forge->addForeignKey('nav_item_id', 'nav_item', 'id', '', 'CASCADE');
        $this->forge->createTable('permission');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('permission');
    }
}
