<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserPersonalData extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY')
                    ->addField(['user_data_id' => ['type' => 'INT']])
                    ->addField(
                        "nationality VARCHAR(1) NOT NULL
                        CHECK (nationality = '' OR nationality = 'V' OR nationality = 'E')"
                    )
                    ->addField([
                        'id_card_num' => [
                            'type'       => 'VARCHAR',
                            'constraint' => '11',
                            'unique'     => true
                        ],
                        'names'       => [
                            'type'       => 'VARCHAR',
                            'constraint' => '40',
                        ],
                        'lastnames'   => [
                            'type'       => 'VARCHAR',
                            'constraint' => '40',
                        ],
                    ])
                    ->addField(
                        "gender VARCHAR(1) NOT NULL
                        CHECK (gender = '' OR gender = 'X' OR gender = 'F' OR gender = 'M')"
                    )
                    ->addField([
                        'emails'      => [
                            'type'       => 'VARCHAR',
                        ],
                        'phone_nums'  => [
                            'type'       => 'VARCHAR',
                        ],
                        'photo'       => [
                            'type'       => 'VARCHAR',
                        ],
                        'created_at'  => [
                            'type'       => 'TIMESTAMPTZ',
                            'default'    => 'NOW()'
                        ],
                        'updated_at'  => [
                            'type'       => 'TIMESTAMPTZ',
                            'default'    => 'NOW()'
                        ],
                        'deleted_at'  => [
                            'type'       => 'TIMESTAMPTZ',
                            'null'       => true
                        ]
                    ])
                    ->addForeignKey('user_data_id', 'user_data', 'id', '', 'CASCADE')
                    ->createTable('user_personal_data');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('user_personal_data');
    }
}
