<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Button extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'label'      => [
                'type'       => 'VARCHAR',
                'constraint' => '30'
            ],
            'html_title' => [
                'type'       => 'VARCHAR',
                'constraint' => '512'
            ],
            'icon'       => [
                'type'       => 'VARCHAR',
                'constraint' => '15'
            ],
            'html_id'    => [
                'type'       => 'VARCHAR',
                'constraint' => '15'
            ],
            'sort'       => [
                'type'       => 'INT'
            ],
            'color'      => [
                'type'       => 'VARCHAR',
                'constraint' => '15'
            ],
            'style_id'   => [
                'type'       => 'INT'
            ],
            'created_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'updated_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ],
            'deleted_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ]
        ]);
        $this->forge->addForeignKey('style_id', 'button_style', 'id', '', 'CASCADE');
        $this->forge->createTable('button');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('button');
    }
}
