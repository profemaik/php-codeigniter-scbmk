<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserData extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'username'   => [
                'type'       => 'VARCHAR',
                'constraint' => '40',
                'unique'     => true
            ],
            'password'   => [
                'type'       => 'VARCHAR',
                'constraint' => '512'
            ],
            'is_psw_set' => [
                'type'       => 'BOOL',
                'default'    => false
            ],
            'is_super'   => [
                'type'       => 'BOOL',
                'default'    => false
            ],
            'is_admin'   => [
                'type'       => 'BOOL',
                'default'    => false
            ],
            'last_login' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ],
            'created_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'updated_at' => [
                'type'       => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'deleted_at' => [
                'type'       => 'TIMESTAMPTZ',
                'null'       => true
            ]
        ]);
        $this->forge->createTable('user_data');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('user_data');
    }
}
