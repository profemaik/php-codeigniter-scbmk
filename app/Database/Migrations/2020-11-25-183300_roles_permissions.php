<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RolesPermissions extends Migration
{
    public function up()
    {
        $this->forge->addField('id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');
        $this->forge->addField([
            'role_id'       => [
                'type'    => 'INT'
            ],
            'permission_id' => [
                'type'    => 'INT'
            ],
            'created_at'    => [
                'type'    => 'TIMESTAMPTZ',
                'default' => 'NOW()'
            ],
            'updated_at'    => [
                'type'    => 'TIMESTAMPTZ',
                'default'    => 'NOW()'
            ],
            'deleted_at'    => [
                'type'    => 'TIMESTAMPTZ',
                'null'    => true
            ]
        ]);
        $this->forge->addForeignKey('role_id', 'role', 'id', '', 'CASCADE');
        $this->forge->addForeignKey('permission_id', 'permission', 'id', '', 'CASCADE');
        $this->forge->createTable('roles_permissions');
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->dropTable('roles_permissions');
    }
}
