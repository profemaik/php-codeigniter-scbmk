<?php namespace App\Database\Seeds;

class DevNavItemSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'name'       => 'Sistema',
            'controller' => '#',
            'icon'       => '',
            'sort'       => 99,
            'ascendant'  => null
        ];
        $this->db->table('nav_item')->insert($data);

        # ID 2
        $data = [
            'name'       => 'Roles',
            'controller' => 'roles',
            'icon'       => 'fa-user-tag',
            'sort'       => 1,
            'ascendant'  => 1
        ];
        $this->db->table('nav_item')->insert($data);

        # ID 3
        $data = [
            'name'       => 'Usuarios',
            'controller' => 'usuarios',
            'icon'       => 'fa-users',
            'sort'       => 2,
            'ascendant'  => 1
        ];
        $this->db->table('nav_item')->insert($data);
    }
}
