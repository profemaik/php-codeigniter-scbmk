<?php namespace App\Database\Seeds;

class DevUsersPermissionsSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'user_id'       => 2,
            'permission_id' => 1
        ];
        $this->db->table('users_permissions')->insert($data);

        # ID 2
        $data = [
            'user_id'       => 2,
            'permission_id' => 2
        ];
        $this->db->table('users_permissions')->insert($data);

        # ID 3
        $data = [
            'user_id'       => 2,
            'permission_id' => 3
        ];
        $this->db->table('users_permissions')->insert($data);
    }
}
