<?php namespace App\Database\Seeds;

class DevPermissionSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'name'        => 'Puede ver roles',
            'nav_item_id' => 2,
            'codename'    => 'view_role'
        ];
        $this->db->table('permission')->insert($data);

        # ID 2
        $data = [
            'name'        => 'Puede agregar roles',
            'nav_item_id' => 2,
            'codename'    => 'add_role'
        ];
        $this->db->table('permission')->insert($data);

        # ID 3
        $data = [
            'name'        => 'Puede modificar roles',
            'nav_item_id' => 2,
            'codename'    => 'change_role'
        ];
        $this->db->table('permission')->insert($data);

        # ID 4
        $data = [
            'name'        => 'Puede habilitar roles',
            'nav_item_id' => 2,
            'codename'    => 'enable_role'
        ];
        $this->db->table('permission')->insert($data);

        # ID 5
        $data = [
            'name'        => 'Puede deshabilitar roles',
            'nav_item_id' => 2,
            'codename'    => 'disable_role'
        ];
        $this->db->table('permission')->insert($data);

        # ID 6
        $data = [
            'name'        => 'Puede eliminar permanentemente roles',
            'nav_item_id' => 2,
            'codename'    => 'delete_role'
        ];
        $this->db->table('permission')->insert($data);

        # ID 7
        $data = [
            'name'        => 'Puede ver usuarios',
            'nav_item_id' => 3,
            'codename'    => 'view_user'
        ];
        $this->db->table('permission')->insert($data);

        # ID 8
        $data = [
            'name'        => 'Puede agregar usuarios',
            'nav_item_id' => 3,
            'codename'    => 'add_user'
        ];
        $this->db->table('permission')->insert($data);

        # ID 9
        $data = [
            'name'        => 'Puede modificar usuarios',
            'nav_item_id' => 3,
            'codename'    => 'change_user'
        ];
        $this->db->table('permission')->insert($data);

        # ID 10
        $data = [
            'name'        => 'Puede habilitar usuarios',
            'nav_item_id' => 3,
            'codename'    => 'enable_user'
        ];
        $this->db->table('permission')->insert($data);

        # ID 11
        $data = [
            'name'        => 'Puede deshabilitar usuarios',
            'nav_item_id' => 3,
            'codename'    => 'disable_user'
        ];
        $this->db->table('permission')->insert($data);

        # ID 12
        $data = [
            'name'        => 'Puede eliminar permanentemente usuarios',
            'nav_item_id' => 3,
            'codename'    => 'delete_user'
        ];
        $this->db->table('permission')->insert($data);
    }
}
