<?php namespace App\Database\Seeds;

class DevPermissionRoutesSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'permission_id' => 1,
            'sys_route'     => 'sistema/roles',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 2
        $data = [
            'permission_id' => 1,
            'sys_route'     => 'sistema/roles/ver-rol/1',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 3
        $data = [
            'permission_id' => 1,
            'sys_route'     => 'sistema/roles/desactivados',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 4
        $data = [
            'permission_id' => 2,
            'sys_route'     => 'sistema/roles/agregar-rol',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 5
        $data = [
            'permission_id' => 2,
            'sys_route'     => 'sistema/roles/save-new',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 6
        $data = [
            'permission_id' => 3,
            'sys_route'     => 'sistema/roles/modificar-rol',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 7
        $data = [
            'permission_id' => 3,
            'sys_route'     => 'sistema/roles/save-modified',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 8
        $data = [
            'permission_id' => 4,
            'sys_route'     => 'sistema/roles/enable-role',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 9
        $data = [
            'permission_id' => 4,
            'sys_route'     => 'sistema/roles/enable-role/4',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 10
        $data = [
            'permission_id' => 5,
            'sys_route'     => 'sistema/roles/disable-role',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 11
        $data = [
            'permission_id' => 5,
            'sys_route'     => 'sistema/roles/disable-role/3',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 12
        $data = [
            'permission_id' => 6,
            'sys_route'     => 'sistema/roles/delete-role',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 13
        $data = [
            'permission_id' => 6,
            'sys_route'     => 'sistema/roles/delete-role/5',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 14
        $data = [
            'permission_id' => 7,
            'sys_route'     => 'sistema/usuarios',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 15
        $data = [
            'permission_id' => 7,
            'sys_route'     => 'sistema/usuarios/ver-usuario/1',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 16
        $data = [
            'permission_id' => 7,
            'sys_route'     => 'sistema/usuarios/desactivados',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 17
        $data = [
            'permission_id' => 8,
            'sys_route'     => 'sistema/usuarios/agregar-usuario',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 18
        $data = [
            'permission_id' => 8,
            'sys_route'     => 'sistema/usuarios/save-new',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 19
        $data = [
            'permission_id' => 9,
            'sys_route'     => 'sistema/usuarios/modificar-usuario',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 20
        $data = [
            'permission_id' => 9,
            'sys_route'     => 'sistema/usuarios/save-modified',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 21
        $data = [
            'permission_id' => 10,
            'sys_route'     => 'sistema/usuarios/enable-user',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 22
        $data = [
            'permission_id' => 10,
            'sys_route'     => 'sistema/usuarios/enable-user/4',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 23
        $data = [
            'permission_id' => 11,
            'sys_route'     => 'sistema/usuarios/disable-user',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 24
        $data = [
            'permission_id' => 11,
            'sys_route'     => 'sistema/usuarios/disable-user/4',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 25
        $data = [
            'permission_id' => 12,
            'sys_route'     => 'sistema/usuarios/delete-user',
        ];
        $this->db->table('permission_routes')->insert($data);

        # ID 26
        $data = [
            'permission_id' => 12,
            'sys_route'     => 'sistema/usuarios/delete-user/5',
        ];
        $this->db->table('permission_routes')->insert($data);
    }
}
