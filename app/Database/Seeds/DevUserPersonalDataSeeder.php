<?php namespace App\Database\Seeds;

class DevUserPersonalDataSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'user_data_id' => 1,
            'nationality'  => '',
            'id_card_num'  => '000.000.000',
            'names'        => 'Super',
            'lastnames'    => 'Usuario',
            'gender'       => '',
            'emails'       => 'a:2:{s:4:"main";s:0:"";s:3:"aux";s:0:"";}',
            'phone_nums'   => 'a:2:{s:4:"main";s:0:"";s:3:"aux";s:0:"";}',
            'photo'        => '',
        ];
        $this->db->table('user_personal_data')->insert($data);

        # ID 2
        $data = [
            'user_data_id' => 2,
            'nationality'  => 'E',
            'id_card_num'  => '12.345.678',
            'names'        => 'Aurora',
            'lastnames'    => 'Boreal',
            'gender'       => 'F',
            'emails'       => 'a:2:{s:4:"main";s:0:"";s:3:"aux";s:0:"";}',
            'phone_nums'   => 'a:2:{s:4:"main";s:0:"";s:3:"aux";s:0:"";}',
            'photo'        => '',
        ];
        $this->db->table('user_personal_data')->insert($data);

        # ID 3
        $data = [
            'user_data_id' => 3,
            'nationality'  => 'V',
            'id_card_num'  => '12.345.679',
            'names'        => 'Aurora',
            'lastnames'    => 'Austral',
            'gender'       => 'F',
            'emails'       => 'a:2:{s:4:"main";s:0:"";s:3:"aux";s:0:"";}',
            'phone_nums'   => 'a:2:{s:4:"main";s:0:"";s:3:"aux";s:0:"";}',
            'photo'        => '',
        ];
        $this->db->table('user_personal_data')->insert($data);
    }
}
