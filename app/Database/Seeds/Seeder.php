<?php namespace App\Database\Seeds;

use CodeIgniter\Exceptions\CriticalError;

class Seeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        if (ENVIRONMENT !== 'production')
        {
            $this->develSeeder();
        }
        else
        {
            throw new CriticalError("SCBmk no está preparado para entornos de producción.");
        }
    }

    private function develSeeder()
    {
        $this->call('DevUserDataSeeder');
        $this->call('DevUserPersonalDataSeeder');
        $this->call('DevRoleSeeder');
        $this->call('DevUsersRolesSeeder');
        $this->call('DevNavItemSeeder');
        $this->call('DevPermissionSeeder');
        $this->call('DevUsersPermissionsSeeder');
        $this->call('DevRolesPermissionsSeeder');
        $this->call('DevPermissionRoutesSeeder');
    }
}
