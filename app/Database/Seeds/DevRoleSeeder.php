<?php namespace App\Database\Seeds;

class DevRoleSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        #ID 1
        $data = [
            'name'    => 'Secretario(a)',
            'summary' => ''
        ];
        $this->db->table('role')->insert($data);

        # ID 2
        $data = [
            'name'    => 'Jefe(a) de Área',
            'summary' => ''
        ];
        $this->db->table('role')->insert($data);

        # ID 3
        $data = [
            'name'    => 'Director(a) de Área',
            'summary' => ''
        ];
        $this->db->table('role')->insert($data);

        # ID 4
        $data = [
            'name'    => 'Director(a) General',
            'summary' => ''
        ];
        $this->db->table('role')->insert($data);
    }
}
