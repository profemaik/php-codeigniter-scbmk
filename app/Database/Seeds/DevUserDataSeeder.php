<?php namespace App\Database\Seeds;

class DevUserDataSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'username'   => 'super',
            'password'   => '$2y$10$2cieb283BXlsqMf4tXRfuu/KhWueNoUaCfCPjkygYmWyoe5S1nXwe',
            'is_psw_set' => true,
            'is_super'   => true,
            'is_admin'   => true,
        ];
        $this->db->table('user_data')->insert($data);

        # ID 2
        $data = [
            'username'   => 'aurora.boreal',
            'password'   => '$2y$10$2cieb283BXlsqMf4tXRfuu/KhWueNoUaCfCPjkygYmWyoe5S1nXwe',
        ];
        $this->db->table('user_data')->insert($data);

        # ID 3
        $data = [
            'username'   => 'aurora.austral',
            'password'   => '$2y$10$2cieb283BXlsqMf4tXRfuu/KhWueNoUaCfCPjkygYmWyoe5S1nXwe',
        ];
        $this->db->table('user_data')->insert($data);
    }
}
