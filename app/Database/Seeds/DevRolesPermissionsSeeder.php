<?php namespace App\Database\Seeds;

class DevRolesPermissionsSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'role_id'       => 1,
            'permission_id' => 1
        ];
        $this->db->table('roles_permissions')->insert($data);

        # ID 2
        $data = [
            'role_id'       => 1,
            'permission_id' => 3
        ];
        $this->db->table('roles_permissions')->insert($data);

        # ID 3
        $data = [
            'role_id'       => 2,
            'permission_id' => 4
        ];
        $this->db->table('roles_permissions')->insert($data);

        # ID 4
        $data = [
            'role_id'       => 2,
            'permission_id' => 5
        ];
        $this->db->table('roles_permissions')->insert($data);

        # ID 5
        $data = [
            'role_id'       => 3,
            'permission_id' => 2
        ];
        $this->db->table('roles_permissions')->insert($data);

        # ID 6
        $data = [
            'role_id'       => 3,
            'permission_id' => 6
        ];
        $this->db->table('roles_permissions')->insert($data);
    }
}
