<?php namespace App\Database\Seeds;

class DevUsersRolesSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        # ID 1
        $data = [
            'user_id' => 2,
            'role_id' => 1
        ];
        $this->db->table('users_roles')->insert($data);

        # ID 2
        $data = [
            'user_id' => 3,
            'role_id' => 2
        ];
        $this->db->table('users_roles')->insert($data);
    }
}
