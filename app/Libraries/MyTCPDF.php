<?php

namespace App\Libraries;

use TCPDF;

class MyTCPDF extends TCPDF
{
	public function Header() {
        $image_file = base_url('assets/imgs/icon/logo_header.png');
		$this->Image($image_file, 10, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetFont('helvetica', 'B', 20);
		$this->Cell(0, 15, 'Reporte de Auditoría', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}

	public function Footer() {
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 8);
		$this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    public function ColoredTable($header,$data) {
		// Colors, line width and bold font
		$this->SetFillColor(255, 0, 0);
		$this->SetTextColor(255);
		$this->SetDrawColor(128, 0, 0);
		$this->SetLineWidth(0.3);
		$this->SetFont('', 'B');
		// Header
		$w = array(30, 20, 25, 15, 15, 30, 40, 45, 45);
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
		}
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(224, 235, 255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) {
            //var_dump($data); exit;
			$this->MultiCell($w[0], 20, $row['db_object'], 'LR', 'L', $fill, 0);
			$this->MultiCell($w[1], 20, $row['db_user_id'], 'LR', 'L', $fill, 0);
			$this->MultiCell($w[2], 20, $row['sys_user'], 'LR', 'L', $fill, 0);
            $this->MultiCell($w[3], 20, $row['sys_user_ip'], 'LR', 'L', $fill, 0);
            $this->MultiCell($w[4], 20, $row['operation'], 'LR', 'L', $fill, 0);
            $this->MultiCell($w[5], 20, $row['tstamp'], 'LR', 'L', $fill, 0);
            $this->MultiCell($w[6], 20, $row['old_data'], 'LR', 'L', $fill, 0);
            $this->MultiCell($w[7], 20, $row['new_data'], 'LR', 'L', $fill, 0);
            $this->MultiCell($w[8], 20, $row['query'], 'LR', 'L', $fill, 0);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');
	}
}
