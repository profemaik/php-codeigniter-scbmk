<?php

namespace App\Libraries;

class JsonResponse
{
    private $icon;
    private $title;
    private $html;
    private $errorFields;
    private $validateFields;
    private $filterCode;

    public function __construct() {
        $this->icon = '';
        $this->title = '';
        $this->html = '';
        $this->errorFields = [];
        $this->validateFields = '';
    }

    public function validation(array $fields)
    {
        $this->setIcon('error');
        $this->setTitle('ERROR(ES) DETECTADO(S)');
        $this->setHtml('Por favor,corrija los campos <b>resaltados en rojo</b> e intente nuevamente.');
        $this->setErrorFields($fields);

        return $this->response();
    }

    public function success()
    {
        $this->setIcon('success');
        $this->setTitle('ÉXITO');
        $this->setHtml('Acción ejecutada correctamente.');
        $this->setErrorFields(['xxx' => 'xxx']);
        $this->setValidateFields('sí');

        return $this->response();
    }

    public function error(string $errorMsg)
    {
        $this->setIcon('error');
        $this->setTitle('ERROR');
        $this->setHtml($errorMsg);
        $this->setValidateFields();

        return $this->response();
    }

    public function info()
    {
        $this->setIcon('info');
        $this->setTitle('INFORMACIÓN');
        $this->setHtml('No se han detectado cambios en los datos.');
        $this->setValidateFields();

        return $this->response();
    }

    private function setIcon(string $icon)
    {
        $this->icon = $icon;
    }
    private function setTitle(string $title)
    {
        $this->title = $title;
    }
    private function setHtml(string $html)
    {
        $this->html = $html;
    }
    private function setErrorFields(array $errorFields)
    {
        $this->errorFields = $errorFields;
    }
    private function setValidateFields(string $value = 'no')
    {
        $this->validateFields = $value;
    }
    private function setFilterCode(int $code)
    {
        $this->filterCode = $code;
    }

    public function response()
    {
        return json_encode([
            'title'    => $this->getTitle(),
            'icon'     => $this->getIcon(),
            'html'     => $this->getHtml(),
            'fields'   => $this->getErrorFields(),
            'noFields' => $this->getValidateFields(),
            'bsod'     => csrf_hash()
        ]);
    }

    private function getTitle(): string
    {
        return $this->title;
    }
    private function getIcon(): string
    {
        return $this->icon;
    }
    private function getHtml(): string
    {
        return $this->html;
    }
    private function getErrorFields(): array
    {
        return $this->errorFields;
    }
    private function getValidateFields(): string
    {
        return $this->validateFields;
    }
    private function getFilterCode(): int
    {
        return $this->filterCode;
    }
}
