<?php namespace App\Models;

use App\Entities\UserLog;
use CodeIgniter\Model;

/**
 * Modelo de la tabla **user_personal_data**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UserPersonalDataModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'user_personal_data';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\UserPersonalData';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'user_data_id',
        'nationality',
        'id_card_num',
        'names',
        'lastnames',
        'gender',
        'emails',
        'phone_nums',
        'photo',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [
        'sanitizeUserDataId',
        'sanitizeNationality',
        'sanitizeIdCardNum',
        'sanitizeNames',
        'sanitizeLastnames',
        'sanitizeGender',
        'sanitizePhoto'
    ];
    protected $beforeUpdate = [
        'sanitizeUserDataId',
        'sanitizeNationality',
        'sanitizeIdCardNum',
        'sanitizeNames',
        'sanitizeLastnames',
        'sanitizeGender',
        'sanitizePhoto'
    ];

    protected $afterUpdate = ['logUpdateTrace'];
    //protected $afterDelete = ['logDeleteTrace'];

    //--------------------------------------------------------------------

    public function sanitizeUserDataId(array $data)
    {
        if (! isset($data['data']['user_data_id'])) return $data;

        $data['data']['user_data_id'] = filter_var(preg_replace('/[^0-9]/','',trim($data['data']['user_data_id'])), FILTER_SANITIZE_NUMBER_INT);

        return $data;
    }

    public function sanitizeNationality(array $upd)
    {
        if (! isset($upd['data']['nationality'])) return $upd;

        $upd['data']['nationality'] = preg_replace('/[^VE]/', '', $upd['data']['nationality']);

        return $upd;
    }

    public function sanitizeIdCardNum(array $upd)
    {
        if (! isset($upd['data']['id_card_num'])) return $upd;

        $upd['data']['id_card_num'] = filter_var(preg_replace('/[^0-9.]/','',trim($upd['data']['id_card_num'])), FILTER_SANITIZE_STRING);


        return $upd;
    }

    public function sanitizeNames(array $upd)
    {
        if (! isset($upd['data']['names'])) return $upd;

        $upd['data']['names'] = filter_var(trim($upd['data']['names']), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

        return $upd;
    }

    public function sanitizeLastnames(array $upd)
    {
        if (! isset($upd['data']['lastnames'])) return $upd;

        $upd['data']['lastnames'] = filter_var(trim($upd['data']['lastnames']), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

        return $upd;
    }

    public function sanitizeGender(array $upd)
    {
        if (! isset($upd['data']['gender'])) return $upd;

        $upd['data']['gender'] = preg_replace('/[^XFM]/', '', $upd['data']['gender']);
        return $upd;
    }

    public function sanitizePhoto(array $upd)
    {
        if (! isset($upd['data']['photo'])) return $upd;

        $upd['data']['photo'] = filter_var(trim($upd['data']['photo']), FILTER_SANITIZE_STRING);

        return $upd;
    }

    protected function logUpdateTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $username = $this->getUserName($data['id'][0]);

        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id'][0]),
            'object_repr' => $username,
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
        ];

        $msg = 'modificado';
        $chain = false;

        foreach ($data['data'] as $key => $value)
        {
            switch ($key) {
                case 'nationality':
                    $msg .= $chain ? ', nacionalidad [' . $value .']' :
                                     ' nacionalidad [' . $value .']';
                    $chain = true;
                    break;
                case 'id_card_num':
                    $msg .= $chain ? ', número de Cédula [' . $value .']' :
                                     ' número de Cédula [' . $value .']';
                    $chain = true;
                    break;
                case 'names':
                    $msg .= $chain ? ', nombres [' . $value .']' :
                                     ' nombres [' . $value .']';
                    $chain = true;
                    break;
                case 'lastnames':
                    $msg .= $chain ? ', apellidos [' . $value .']' :
                                     ' apellidos [' . $value .']';
                    $chain = true;
                    break;
                case 'gender':
                    $msg .= $chain ? ', género ' :
                                     ' género ';
                    $msg .= '[' . SOCIAL_GENDER[$value] . ']';
                    $chain = true;
                    break;
                case 'emails':
                    $emails = unserialize($value);
                    $msg .= $chain ? ', correos electrónicos ' :
                                     ' correos electrónicos ';
                    $msg .= 'principal [' . $emails['main'] . '] ';
                    $msg .= 'auxiliar [' . $emails['aux'] . ']';
                    $chain = true;
                    break;
                case 'phone_nums':
                    $phones = unserialize($value);
                    $msg .= $chain ? ', teléfonos ' :
                                     ' teléfonos ';
                    $msg .= 'principal [' . $phones['main'] . '] ';
                    $msg .= 'auxiliar [' . $phones['aux'] . ']';
                    $chain = true;
                    break;
                case 'photo':
                    $msg .= $chain ? ', imagen del usuario ' :
                                     ' imagen del usuario ';
                    $msg .= '[' . $value . ']';
                    $chain = true;
                    break;
                case 'updated_at':
                    break;
                case 'deleted_at':
                    //$msg = 'habilitado datos personales [' . $username . ']';
                    //$trace['action_flag'] = ACTION_FLAG_ENABLE;
                    break;

                default:
                    $msg = 'no ha cambiado ningún campo';
                    break;
            }
        }

        $msg .= '.';
        $trace['message'] = $msg;
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logDeleteTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if ($data['purge']) return $data;
        if (! $data['result']) return $data;

        $username = $this->getUserName($data['id'][0]);
        $msg = 'deshabilitado datos personales [' . $username . '].';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id'][0]),
            'object_repr' => $username,
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_DISABLE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    /**
     * Busca el nombre del usuario segun el ID de la data personal del usuario.
     *
     * @param integer $id ID de la data personal del usuario.
     * @return string Nombre del usuario o una cadena vacío si no se encontró.
     */
    private function getUserName(int $id)
    {
        $sql = 'SELECT ud.username FROM user_data ud
                inner join user_personal_data upd
                    on upd.user_data_id = ud.id
                WHERE upd.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->username;
    }

    /**
     * Busca el ID del usuario segun el ID de la data personal del usuario.
     *
     * @param integer $id ID de la data personal del usuario.
     * @return integer|null ID del usuario o null si no se encontró.
     */
    private function getUserID(int $id)
    {
        $sql = 'SELECT ud.id FROM user_data ud
                inner join user_personal_data upd
                    on upd.user_data_id = ud.id
                WHERE upd.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return null;
        }

        return $result->id;
    }
}
