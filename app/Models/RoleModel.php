<?php namespace App\Models;

use App\Entities\UserLog;
use CodeIgniter\Model;

/**
 * Tabla **role**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019 Maikel Carballo.
 */
class RoleModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'role';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\Role';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'name',
        'summary'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [
        'name' => [
            'label' => 'Nombre',
            'rules' => "required|string|min_length[3]|is_unique[role.name,id,{id}]"
        ],
        'summary' => [
            'label' => 'Descripción breve',
            'rules' => 'permit_empty|min_length[13]',
        ]
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $beforeInsert = [
        'sanitize_noun',
        'sanitize_abstract'
    ];
    protected $beforeUpdate = [
        'sanitize_noun',
        'sanitize_abstract'
    ];

    protected $afterInsert = ['logInsertTrace'];
    protected $afterUpdate = ['logUpdateTrace'];
    protected $afterDelete = ['logDeleteTrace'];

    public function getAssignedRol(int $userID)
    {
        $sql = <<<SQL
            SELECT *,
                CASE
                    WHEN r1.id in (
                        select ur.role_id
                        from users_roles ur
                        inner join user_data ud on ud.id = ur.user_id
                        where ur.user_id = ?
                            and ur.deleted_at isnull) THEN 'selected'
                    ELSE ''
                END selected
            FROM "role" r1 order by r1."name" ;
        SQL;

        return $this->db->query($sql, [$userID])->getResultObject();
    }

    /**
     * Consulta en la BD si el nombre y el id de un rol coinciden.
     *
     * Si el campo deleted_at no es nulo, es tomado en cuenta.
     *
     * @param array $row Datos de la fila del datatable.
     * @return boolean true si los datos coinciden, false en caso contrario.
     */
    public function isValidDtbRow(array $row): bool
    {
        $this->builder('role r')
             ->where('r.name', $row['name'])
             ->where('r.id', $row['id']);

        if (! empty($row['deletedAt'])) {
            $this->builder('role r')->where('r.deleted_at', $row['deletedAt']);
        }

        return (bool) $this->allowCallbacks(false)->builder()->get()->getResultObject();
    }

    /**
     * Deshace la eliminación de un rol actualizando el campo deleted_at.
     *
     * @param integer $id ID del rol.
     * @return boolean
     */
    public function undoSoftDelete(int $id)
    {
        $this->allowedFields = ['deleted_at'];

        return $this->update($id, ['deleted_at' => null]);
    }

    //--------------------------------------------------------------------

    public function sanitize_noun(array $data)
    {
        if (! isset($data['data']['name'])) return $data;

        $data['data']['name'] = filter_var(trim($data['data']['name']), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        return $data;
    }

    public function sanitize_abstract(array $data)
    {
        if (! isset($data['data']['summary'])) return $data;

        $data['data']['summary'] = filter_var(trim($data['data']['summary']), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        return $data;
    }

    protected function logInsertTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $msg = 'ingresado ' . $data['data']['name'] . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['id'],
            'object_repr' => $data['data']['name'],
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_ADD,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logUpdateTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['id'][0],
            'object_repr' => $this->getRolName($data['id'][0]),
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_CHANGE,
        ];

        $msg = 'modificado';
        $chain = false;

        foreach ($data['data'] as $key => $value)
        {
            switch ($key) {
                case 'name':
                    $msg .= $chain ? ', nombre del Rol [' . $value .']' :
                                     ' nombre del Rol [' . $value .']';
                    $chain = true;
                    break;
                case 'summary':
                    $msg .= $chain ? ', descripción breve del Rol [' . $value .']' :
                                     ' descripción breve del Rol [' . $value .']';
                    $chain = true;
                    break;
                case 'updated_at':
                    break;
                case 'deleted_at':
                    $msg = 'rol habilitado ['
                           . $this->getRolName($data['id'][0]) . ']';
                    $trace['action_flag'] = ACTION_FLAG_ENABLE;
                    break;

                default:
                    $msg = 'no ha cambiado ningún campo';
                    break;
            }
        }

        $msg .= '.';
        $trace['message'] = $msg;
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logDeleteTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if ($data['purge']) return $data;
        if (! $data['result']) return $data;

        $msg = 'rol deshabilitado [' . $this->getRolName($data['id'][0]) . '].';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['id'][0],
            'object_repr' => $this->getRolName($data['id'][0]),
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_DISABLE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    /**
     * Busca el nombre del rol segun el ID del rol asignado al usuario.
     *
     * @param integer $id ID del rol asignado al usuario.
     * @return string Nombre del rol o una cadena vacía si no se encontró.
     */
    private function getRolName(int $id)
    {
        $sql = 'SELECT r.name FROM "role" r
                inner join users_roles ur
                    on ur.role_id = r.id
                WHERE ur.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->name;
    }
}
