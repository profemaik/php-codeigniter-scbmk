<?php namespace App\Models;

use App\Entities\UserLog;
use CodeIgniter\Model;

/**
 * Tabla **roles_permissions**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class RolesPermissionsModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'roles_permissions';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\RolesPermissions';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'role_id',
        'permission_id',
        'deleted_at',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

    protected $afterInsert = ['logInsertTrace'];
    protected $afterUpdate = ['logUpdateTrace'];
    protected $afterDelete = ['logDeleteTrace'];

    /**
     * Determina si un permiso ha sido eliminado para un rol particular.
     *
     * @param object $entity Entidad RolesPermissions.
     * @return boolean true si ha sido eliminado, false en caso contrario.
     */
    public function hasBeenDeleted(object $entity): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('role_id', $entity->role_id)
                           ->where('permission_id', $entity->permission_id)
                           ->onlyDeleted()->first();
    }

    /**
     * Actualiza el campo deleted_at a valor nulo de un permiso asignado a un
     * rol.
     *
     * @param object $entity Entidad RolesPermissions.
     * @return void
     */
    public function rePermit(object $entity)
    {
        $rolePermission = $this->allowCallbacks(false)
                               ->onlyDeleted()
                               ->where('role_id', $entity->role_id)
                               ->where('permission_id', $entity->permission_id)
                               ->first();

        $this->update($rolePermission->id, ['deleted_at' => null]);
    }

    /**
     * Determina si un permiso ya ha sido asignado a un rol.
     *
     * @param object $entity Entidad RolesPermissions.
     * @return boolean true si ya ha sido asignado, false en caso contrario.
     */
    public function isRoleAlreadyPermitted(object $entity): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('role_id', $entity->role_id)
                           ->where('permission_id', $entity->permission_id)
                           ->first();
    }

    protected function logInsertTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $msg = 'permiso asignado ['
               . $this->getPermissionName($data['id'])
               . '].';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getRoleID($data['id']),
            'object_repr' => $this->getRolName($data['id']),
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_CHANGE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logUpdateTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getRoleID($data['id'][0]),
            'object_repr' => $this->getRolName($data['id'][0]),
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_CHANGE,
        ];

        $msg = 'modificado';
        $chain = false;

        foreach ($data['data'] as $key => $value)
        {
            switch ($key) {
                case 'role_id':
                    $msg .= $chain ? ', ID del Rol [' . $value .']' :
                                     ' ID del Rol [' . $value .']';
                    $chain = true;
                    break;
                case 'permission_id':
                    $msg .= $chain ? ', ID del Permiso [' . $value .']' :
                                     ' ID del Permiso [' . $value .']';
                    $chain = true;
                    break;
                case 'updated_at':
                    break;
                case 'deleted_at':
                    $msg = 'permiso reasignado ['
                           . $this->getPermissionName($data['id'][0])
                           . ']';
                    //$trace['action_flag'] = ACTION_FLAG_ENABLE;
                    break;

                default:
                    $msg = 'no ha cambiado ningún campo';
                    break;
            }
        }

        $msg .= '.';
        $trace['message'] = $msg;
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logDeleteTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if ($data['purge']) return $data;
        if (! $data['result']) return $data;

        $msg = 'permiso desasignado [' . $this->getPermissionName($data['id'][0]) . '].';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getRoleID($data['id'][0]),
            'object_repr' => $this->getRolName($data['id'][0]),
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_CHANGE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    /**
     * Busca el nombre del rol segun el ID del permiso asignado al rol.
     *
     * @param integer $id ID del permiso asignado al rol.
     * @return string Nombre del rol o una cadena vacío si no se encontró.
     */
    private function getRolName(int $id)
    {
        $sql = 'SELECT r.name FROM "role" r
                inner join roles_permissions rp
                    on rp.role_id = r.id
                WHERE rp.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->name;
    }

    /**
     * Busca el nombre del permiso segun el ID del permiso del usuario.
     *
     * @param integer $id ID del permiso del usuario.
     * @return string Nombre del permiso o una cadena vacío si no se encontró.
     */
    private function getPermissionName(int $id)
    {
        $sql = 'SELECT p.name FROM permission p
                inner join roles_permissions rp
                    on rp.permission_id = p.id
                WHERE rp.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->name;
    }

    /**
     * Busca el ID del rol segun el ID del permiso del rol.
     *
     * @param integer $id ID del permiso del rol.
     * @return integer|null ID del rol o null si no se encontró.
     */
    private function getRoleID(int $id)
    {
        $sql = 'SELECT r.id FROM "role" r
                inner join roles_permissions rp
                    on rp.role_id = r.id
                WHERE rp.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return null;
        }

        return $result->id;
    }
}
