<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Tabla **nav_item**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class NavItemModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'nav_item';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\NavItem';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];
    protected $useTimestamps = false;
    protected $createdField  = '';
    protected $updatedField  = '';
    protected $deletedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

    /**
     * Consulta y devuelve los elementos del menú de navegación según el nivel
     * dado.
     *
     * @param integer $ascendant Nivel del menú.
     * @return array
     */
    public function getNavItemLvl(int $ascendant): array
    {
        if ($ascendant === 0)
        {
            $sql = 'SELECT ilevel, iid, iname, icontroller, iicon, isort, iascendant
                    FROM navitem_view
                    WHERE iascendant ISNULL ORDER BY isort';

            return $this->db->query($sql)->getResultArray();
        }

        $sql = 'SELECT ilevel, iid, iname, icontroller, iicon, isort, iascendant
                FROM navitem_view
                WHERE iascendant = ? ORDER BY isort';

        return $this->db->query($sql, $ascendant)->getResultArray();
    }

    /**
     * Consulta y devuelve los id de cada uno de los elementos del menú de
     * navegación a los que el usuario puede interactuar.
     *
     * @param integer $ascendant Nivel del menú.
     * @return array
     */
    public function getUserNavItems(array $permissions): array
    {
        $sql = 'WITH RECURSIVE user_menu(ilvl, iid, iname, icontroller, iicon, isort, iascendant) AS (
                        SELECT 0,
                           nav_item.id,
                           nav_item.name,
                           nav_item.controller,
                           nav_item.icon,
                           nav_item.sort,
                           nav_item.ascendant
                          FROM nav_item
                         WHERE nav_item.id in ?
                       UNION
                        SELECT um.ilvl + 1,
                           ni.id,
                           ni.name,
                           ni.controller,
                           ni.icon,
                           ni.sort,
                           ni.ascendant
                          FROM user_menu um, nav_item ni
                          where ni.id = um.iascendant
                       )
                SELECT iid, iname
                  FROM user_menu';

        $result= $this->db->query($sql, [$permissions])->getResultArray();
        foreach ($result as $row)
        {
            $userNavItemIds[] = $row['iid'];
            $userNavItemIds[] = $row['iname'];
        }

        return $userNavItemIds;
    }
}
