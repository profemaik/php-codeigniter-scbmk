<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Tabla **access_recovery**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>
 * @copyright 2020 Maikel Carballo.
 */
class AccessRecoveryModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'access_recovery';
    protected $primaryKey = 'id';

    protected $returnType = 'App\Entities\AccessRecovery';
    protected $useSoftDeletes = false;

    protected $allowedFields  = [
        'username',
        'token'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
