<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Tabla **permisssion**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019 Maikel Carballo.
 */
class PermisssionModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'permisssion';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\Permisssion';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];

    protected $useTimestamps = true;
    protected $createdField  = '';
    protected $updatedField  = '';
    protected $deletedField  = '';

    protected $skipValidation = false;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

    /**
     * Consulta y devuelve los id de los elementos del menú de navegación a los
     * que el usuario puede interactuar.
     *
     * @param array $user Datos del usuario.
     * @return array
     */
    public function getPermissionIds(array $user): array
    {
        if ($user['is_super'] === 't')
        {
            return [];
        }

        $sql = 'SELECT	p.nav_item_id
                FROM 	"permission" p
                    inner join users_permissions up on up.permission_id = p.id
                    inner join user_data ud on up.user_id = ud.id
                WHERE ud.id = ?
                UNION
                SELECT 	p.nav_item_id
                FROM 	"permission" p
                    inner join roles_permissions rp on rp.permission_id = p.id
                    inner join "role" r on r.id = rp.role_id
                    inner join users_roles ur on ur.role_id = r.id
                    inner join user_data ud on ud.id = ur.user_id
                WHERE ud.id = ?';

        $result = $this->db->query($sql, [$user['id'], $user['id']])->getResultArray();

        foreach ($result as $row)
        {
            $perms[] = $row['nav_item_id'];
        }

        return $perms;
    }

    /**
     * Consulta y devuelve los nombres clave (codenames) de cada uno de los
     * elementos del menú de navegación a los que el usuario puede interactuar.
     *
     * @param integer $userId Id del usuario.
     * @return array
     */
    public function getPermissionCodenames(int $userId): ?array
    {
        $sql = 'SELECT	p.codename
                FROM 	"permission" p
                    inner join users_permissions up on up.permission_id = p.id
                    inner join user_data ud on ud.id = up.user_id
                WHERE ud.id = ?
                UNION
                SELECT 	p.codename
                FROM 	"permission" p
                    inner join roles_permissions rp on rp.permission_id = p.id
                    inner join "role" r on r.id = rp.role_id
                    inner join users_roles ur on ur.role_id = r.id
                    inner join user_data ud on ud.id = ur.user_id
                WHERE ud.id = ?';

        $result = $this->db->query($sql, [$userId, $userId])->getResultArray();
        $perms = [];

        foreach ($result as $row)
        {
            $perms[] = $row['codename'];
        }

        return $perms;
    }

    public function getUserPermission(int $userID)
    {
        $sql = <<<SQL
            SELECT *,
                CASE
                    WHEN p.id in (
                        select up.permission_id
                        from users_permissions up
                        inner join "permission" p2 on p2.id = up.permission_id
                        where up.user_id = ?
                            and up.deleted_at isnull) THEN 'selected'
                    ELSE ''
                END selected
            FROM "permission" p order by p.codename ;
        SQL;

        return $this->db->query($sql, [$userID])->getResultObject();
    }

    public function getRolePermission(int $roleID)
    {
        $sql = <<<SQL
            SELECT *,
                CASE
                    WHEN p.id in (
                        select rp.permission_id
                        from roles_permissions rp
                        inner join "permission" p2 on p2.id = rp.permission_id
                        where rp.role_id = ?
                            and rp.deleted_at isnull) THEN 'selected'
                    ELSE ''
                END selected
            FROM "permission" p order by p.codename ;
        SQL;

        return $this->db->query($sql, [$roleID])->getResultObject();
    }

    /**
     * Determina si una URI es accesible para un usuario que haya iniciado
     * sesión en el sistema.
     *
     * @param string $path URI solicitada en la petición HTTP.
     * @return boolean true si es accesible, false en caso contrario.
     */
    public function isPermitted(string $path): bool
    {
        if (session('is_super') === 't')
        {
            return true;
        }

        $sql = 'SELECT	1
                FROM 	permission_routes pr
                    inner join users_permissions up on up.permission_id = pr.permission_id
                    inner join user_data ud on up.user_id = ud.id
                WHERE ud.id = :id: and pr.sys_route = :path:
                UNION
                SELECT 	1
                FROM 	permission_routes pr
                    inner join roles_permissions rp on rp.permission_id = pr.permission_id
                    inner join "role" r on rp.role_id = r.id
                    inner join users_roles ur on r.id = ur.role_id
                    inner join user_data ud on ur.user_id = ud.id
                WHERE ud.id = :id: and pr.sys_route = :path:';

        return (bool) $this->db->query($sql, ['id'   => session('id'), 'path' => $path])->getResult();
    }
}
