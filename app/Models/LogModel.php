<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Tabla **log**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class LogModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'log';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\Log';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'user_id',
        'user_ip',
        'object_id',
        'object_repr',
        'action_flag',
        'model',
        'message'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = '';
    protected $deletedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

}