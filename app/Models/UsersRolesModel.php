<?php namespace App\Models;

use App\Entities\UserLog;
use CodeIgniter\Model;

/**
 * Tabla **users_roles**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UsersRolesModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'users_roles';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\UsersRoles';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'user_id',
        'role_id',
        'deleted_at',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

    protected $afterInsert = ['logInsertTrace'];
    protected $afterUpdate = ['logUpdateTrace'];
    protected $afterDelete = ['logDeleteTrace'];

    public function isUserAlreadyInRole(object $entity): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('user_id', $entity->user_id)
                           ->where('role_id', $entity->role_id)
                           ->first();
    }

    public function hasBeenDeleted(object $entity): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('user_id', $entity->user_id)
                           ->where('role_id', $entity->role_id)
                           ->onlyDeleted()->first();
    }

    public function reRol(object $entity): bool
    {
        $userRol = $this->allowCallbacks(false)
                        ->onlyDeleted()
                        ->where('user_id', $entity->user_id)
                        ->where('role_id', $entity->role_id)
                        ->first();

        return $this->update($userRol->id, ['deleted_at' => null]);
    }

    protected function logInsertTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $msg = 'rol asignado: '
               . $this->getRolName($data['id'])
               . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id']),
            'object_repr' => $this->getUserName($data['id']),
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logUpdateTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id'][0]),
            'object_repr' => $this->getUserName($data['id'][0]),
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
        ];

        $msg = 'modificado';
        $chain = false;

        foreach ($data['data'] as $key => $value)
        {
            switch ($key) {
                case 'user_id':
                    $msg .= $chain ? ', ID del Usuario [' . $value .']' :
                                     ' ID del Usuario [' . $value .']';
                    $chain = true;
                    break;
                case 'role_id':
                    $msg .= $chain ? ', ID del Rol [' . $value .']' :
                                     ' ID del Rol [' . $value .']';
                    $chain = true;
                    break;
                case 'updated_at':
                    break;
                case 'deleted_at':
                    $msg = 'rol reasignado: '
                           . $this->getRolName($data['id'][0]);
                    //$trace['action_flag'] = ACTION_FLAG_ENABLE;
                    break;

                default:
                    $msg = 'no ha cambiado ningún campo';
                    break;
            }
        }

        $msg .= '.';
        $trace['message'] = $msg;
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logDeleteTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if ($data['purge']) return $data;
        if (! $data['result']) return $data;

        $msg = 'rol desasignado ' . $this->getRolName($data['id'][0])
               . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id'][0]),
            'object_repr' => $this->getUserName($data['id'][0]),
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    /**
     * Busca el nombre de usuario segun el ID del rol asignado al usuario.
     *
     * @param integer $id ID del rol asignado al usuario.
     * @return string Nombre de usuario o una cadena vacía si no se encontró.
     */
    private function getUserName(int $id)
    {
        $sql = 'SELECT ud.username FROM user_data ud
                inner join users_roles ur
                    on ur.user_id = ud.id
                WHERE ur.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->username;
    }

    /**
     * Busca el nombre del rol segun el ID del rol asignado al usuario.
     *
     * @param integer $id ID del rol asignado al usuario.
     * @return string Nombre del rol o una cadena vacía si no se encontró.
     */
    private function getRolName(int $id)
    {
        $sql = 'SELECT r.name FROM "role" r
                inner join users_roles ur
                    on ur.role_id = r.id
                WHERE ur.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->name;
    }

    /**
     * Busca el ID del usuario segun el ID del rol del usuario.
     *
     * @param integer $id ID del rol del usuario.
     * @return integer|null ID del usuario o null si no se encontró.
     */
    private function getUserID(int $id)
    {
        $sql = 'SELECT ud.id FROM user_data ud
                inner join users_roles ur
                    on ur.user_id = ud.id
                WHERE ur.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return null;
        }

        return $result->id;
    }
}
