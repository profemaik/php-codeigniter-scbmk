<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Tabla **user_log**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2021 Maikel Carballo.
 * @since     Version 0.2.0
 */
class UserLogModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'user_log';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\UserLog';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'user_id',
        'user_ip',
        'object_id',
        'object_repr',
        'object_type',
        'action_flag',
        'message',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = '';
    protected $deletedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

}