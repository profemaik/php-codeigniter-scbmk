<?php namespace App\Models;

use App\Entities\UserLog;
use CodeIgniter\Model;

/**
 * Tabla **user_data**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UserDataModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'user_data';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\UserData';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'user_pd_id',
        'username',
        'password',
        'is_psw_set',
        'is_super',
        'is_admin',
        'last_login'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [
        'sanitizeUsername',
        'sanitizePassword',
        'sanitizeIsPswSet',
        'sanitizeIsSuper',
        'sanitizeIsAdmin',
        'sanitizeLastLogin'
    ];
    protected $beforeUpdate = [
        'sanitizeUsername',
        'sanitizePassword',
        'sanitizeIsPswSet',
        'sanitizeIsSuper',
        'sanitizeIsAdmin',
        'sanitizeLastLogin'
    ];

    protected $afterUpdate = ['logUpdateTrace'];
    protected $afterDelete = ['logDeleteTrace'];

    /**
     * Busca un nombre de usuario.
     *
     * @param string $str Nombre de usuario a ser buscado.
     * @return boolean true si encontró el nombre, false en caso contrario.
     */
    public function isUser(string $str): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('username', $str)
                           ->findAll();
    }

    /**
     * Busca el hash de la contraseña de un usuario.
     *
     * @param string $str Nombre de usuario.
     * @return object|null Entidad user_data o null si no existe.
     */
    public function getUserHash(string $str): ?object
    {
        return $this->allowCallbacks(false)
                    ->select('password')
                    ->where('username', $str)
                    ->first();
    }

    /**
     * Busca los datos propios y personales de un usuario.
     *
     * @param string $username Nombre de usuario.
     * @return array Datos encontrados del usuario.
     */
    public function getUserData(string $username): array
    {
        return $this->allowCallbacks(false)
                    ->asArray()
                    ->where('username', $username)
                    ->join('user_personal_data upd', 'upd.user_data_id = user_data.id')
                    ->first();
    }

    /**
     * Determina si los datos provenientes de un datatable son válidos.
     *
     * @param array $row Datos de una fila del datatable.
     * @return boolean true si los datos coinciden, false en caso contrario.
     */
    public function isValidDtbRow(array $row): bool
    {
        $this->builder('user_data ud')
             ->where('ud.username', $row['username'])
             ->where('upd.id_card_num', $row['idCardNum'])
             ->where('upd.names', $row['names'])
             ->where('upd.lastnames', $row['lastnames'])
             ->where('ud.id', $row['id'])
             ->join('user_personal_data upd', 'upd.user_data_id = ud.id');

        if (! empty($row['deletedAt'])) {
            $this->builder('user_data ud')->where('ud.deleted_at', $row['deletedAt']);
        }

        return (bool) $this->allowCallbacks(false)->builder()->get()->getResultObject();
    }

    /**
     * Actualiza el campo deleted_at a valor null.
     *
     * @param integer $id ID del usuario.
     * @return boolean true si actualizó, false en caso contrario.
     */
    public function undoSoftDelete(int $id)
    {
        $this->allowedFields = ['deleted_at'];

        return $this->update($id, ['deleted_at' => null]);
    }

    //--------------------------------------------------------------------

    public function sanitizeUsername(array $data)
    {
        if (! isset($data['data']['username'])) return $data;

        $data['data']['username'] = filter_var(trim($data['data']['username']), FILTER_SANITIZE_STRING);

        return $data;
    }

    public function sanitizePassword(array $data)
    {
        if (! isset($data['data']['password'])) return $data;

        $data['data']['password'] = filter_var(trim($data['data']['password']), FILTER_SANITIZE_STRING);

        return $data;
    }

    public function sanitizeIsPswSet(array $data)
    {
        if (! isset($data['data']['is_psw_set'])) return $data;

        $data['data']['is_psw_set'] = filter_var(trim($data['data']['is_psw_set']), FILTER_SANITIZE_STRING);

        return $data;
    }

    public function sanitizeIsSuper(array $data)
    {
        if (! isset($data['data']['is_super'])) return $data;

        $data['data']['is_super'] = filter_var(trim($data['data']['is_super']), FILTER_SANITIZE_STRING);

        return $data;
    }

    public function sanitizeIsAdmin(array $data)
    {
        if (! isset($data['data']['is_admin'])) return $data;

        $data['data']['is_admin'] = filter_var(trim($data['data']['is_admin']), FILTER_SANITIZE_STRING);

        return $data;
    }

    public function sanitizeLastLogin(array $data)
    {
        if (! isset($data['data']['last_login'])) return $data;

        $data['data']['last_login'] = filter_var(trim($data['data']['last_login']), FILTER_SANITIZE_STRING);

        return $data;
    }

    protected function logUpdateTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $username = $this->getUserName($data['id'][0]);

        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['id'][0],
            'object_repr' => $username,
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
        ];

        $msg = 'modificado';
        $chain = false;

        foreach ($data['data'] as $key => $value)
        {
            switch ($key) {
                case 'username':
                    $msg .= $chain ? ', Nombre de Usuario [' . $value .']' :
                                     ' Nombre de Usuario [' . $value .']';
                    $chain = true;
                    break;
                case 'password':
                    $msg .= $chain ? ', Contraseña' :
                                     ' Contraseña';
                    $chain = true;
                    break;
                case 'is_psw_set':
                    $msg = 'establecida Contraseña propia';
                    break;
                case 'is_super':
                    $msg .= $chain ? ', estatus de Superusuario ' :
                                     ' estatus de Superusuario ';
                    $msg .= ($value === 't') ? '[activo]' : '[inactivo]';
                    $chain = true;
                    break;
                case 'is_admin':
                    $msg .= $chain ? ', estatus de Admin ' :
                                     ' estatus de Admin ';
                    $msg .= ($value === 't') ? '[activo]' : '[inactivo]';
                    $chain = true;
                    break;
                case 'last_login':
                    $msg = 'ha iniciado sesión';
                    $trace['action_flag'] = ACTION_FLAG_LOGIN;
                    $chain = true;
                    break;
                case 'updated_at':
                    break;
                case 'deleted_at':
                    $msg = 'usuario habilitado [' . $username . ']';
                    $trace['action_flag'] = ACTION_FLAG_ENABLE;
                    break;

                default:
                    $msg = 'no ha cambiado ningún campo';
                    break;
            }
        }

        $msg .= '.';
        $trace['message'] = $msg;
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logDeleteTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if ($data['purge']) return $data;
        if (! $data['result']) return $data;

        $username = $this->getUserName($data['id'][0]);
        $msg = 'usuario deshabilitado [' . $username . '].';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['id'][0],
            'object_repr' => $username,
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_DISABLE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    /**
     * Busca el nombre del usuario segun el ID del usuario.
     *
     * @param integer $id ID del usuario.
     * @return string Nombre del usuario o una cadena vacío si no se encontró.
     */
    private function getUserName(int $id)
    {
        $sql = 'SELECT ud.username FROM user_data ud
                WHERE ud.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->username;
    }
}
