<?php namespace App\Models;

use App\Entities\UserLog;
use CodeIgniter\Model;

/**
 * Tabla **users_permissions**.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class UsersPermissionsModel extends Model
{
    protected $DBGroup    = 'default';
    protected $table      = 'users_permissions';
    protected $primaryKey = 'id';

    protected $returnType     = 'App\Entities\UsersPermissions';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'user_id',
        'permission_id',
        'deleted_at',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $beforeInsert = [];
    protected $beforeUpdate = [];

    protected $afterInsert = ['logInsertTrace'];
    protected $afterUpdate = ['logUpdateTrace'];
    protected $afterDelete = ['logDeleteTrace'];

    public function isUserAlreadyPermitted(object $entity): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('user_id', $entity->user_id)
                           ->where('permission_id', $entity->permission_id)
                           ->first();
    }

    public function hasBeenDeleted(object $entity): bool
    {
        return (bool) $this->allowCallbacks(false)
                           ->where('user_id', $entity->user_id)
                           ->where('permission_id', $entity->permission_id)
                           ->onlyDeleted()->first();
    }

    public function rePermit(object $entity)
    {
        $userPermission = $this->allowCallbacks(false)
                               ->withDeleted()
                               ->where('user_id', $entity->user_id)
                               ->where('permission_id', $entity->permission_id)
                               ->first();

        return $this->update($userPermission->id, ['deleted_at' => null]);
    }

    protected function logInsertTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $msg = 'permiso asignado: '
               . $this->getPermissionName($data['id'])
               . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id']),
            'object_repr' => $this->getUserName($data['id']),
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logUpdateTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if (! $data['result']) return $data;

        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id'][0]),
            'object_repr' => $this->getUserName($data['id'][0]),
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
        ];

        $msg = 'modificado';
        $chain = false;

        foreach ($data['data'] as $key => $value)
        {
            switch ($key) {
                case 'user_id':
                    $msg .= $chain ? ', ID del Usuario [' . $value .']' :
                                     ' ID del Usuario [' . $value .']';
                    $chain = true;
                    break;
                case 'permission_id':
                    $msg .= $chain ? ', ID del Permiso [' . $value .']' :
                                     ' ID del Permiso [' . $value .']';
                    $chain = true;
                    break;
                case 'updated_at':
                    break;
                case 'deleted_at':
                    $msg = 'permiso reasignado: '
                           . $this->getPermissionName($data['id'][0]);
                    //$trace['action_flag'] = ACTION_FLAG_ENABLE;
                    break;

                default:
                    $msg = 'no ha cambiado ningún campo';
                    break;
            }
        }

        $msg .= '.';
        $trace['message'] = $msg;
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    protected function logDeleteTrace(array $data)
    {
        if (! isset($data['id'])) return $data;
        if ($data['purge']) return $data;
        if (! $data['result']) return $data;

        $msg = 'permiso desasignado ' . $this->getPermissionName($data['id'][0])
               . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $this->getUserID($data['id'][0]),
            'object_repr' => $this->getUserName($data['id'][0]),
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_CHANGE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);

        return $data;
    }

    /**
     * Busca el nombre del usuario segun el ID del permiso del usuario.
     *
     * @param integer $id ID del permiso del usuario.
     * @return string Nombre del usuario o una cadena vacío si no se encontró.
     */
    private function getUserName(int $id)
    {
        $sql = 'SELECT ud.username FROM user_data ud
                inner join users_permissions up
                    on up.user_id = ud.id
                WHERE up.id = :upid:';
        $result = $this->db->query($sql, ['upid' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->username;
    }

    /**
     * Busca el nombre del permiso segun el ID del permiso del usuario.
     *
     * @param integer $id ID del permiso del usuario.
     * @return string Nombre del permiso o una cadena vacío si no se encontró.
     */
    private function getPermissionName(int $id)
    {
        $sql = 'SELECT p.name FROM permission p
                inner join users_permissions up
                    on up.permission_id = p.id
                WHERE up.id = :upid:';
        $result = $this->db->query($sql, ['upid' => $id])->getRow();

        if (! $result)
        {
            return '';
        }

        return $result->name;
    }

    /**
     * Busca el ID del usuario segun el ID del permiso del usuario.
     *
     * @param integer $id ID del permiso del usuario.
     * @return integer|null ID del usuario o null si no se encontró.
     */
    private function getUserID(int $id)
    {
        $sql = 'SELECT ud.id FROM user_data ud
                inner join users_permissions up
                    on up.user_id = ud.id
                WHERE up.id = :id:';
        $result = $this->db->query($sql, ['id' => $id])->getRow();

        if (! $result)
        {
            return null;
        }

        return $result->id;
    }
}
