<?php namespace App\Controllers\System;

use App\Controllers\BaseController;
use App\Entities\Role as RoleEntity;
use App\Entities\RolesPermissions;
use App\Entities\UserLog;
use App\Models\PermisssionModel;
use App\Models\RoleModel;
use App\Models\RolesPermissionsModel;
use App\Models\UserLogModel;

/**
 * Permite gestionar los grupos de rols (roles) del sistema.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2019-2020 Maikel Carballo.
 */
class Role extends BaseController
{
    public function index($withDeleted = '')
    {
        session()->remove('updating_role_id');
        $data['roles'] = $this->getRoles($withDeleted);
        $data['withDeleted'] = $withDeleted;

        return view('system/role_list', $data);
    }

    /**
     * Busca y devuelve los registros de los roles almacenados en la base
     * de datos.
     *
     * @param string $withDeleted Indica si debe buscar registros eliminados.
     * @return object Entidad Role.
     */
    private function getRoles($withDeleted = '')
    {
        $roleModel = new RoleModel();

        if ($withDeleted)
        {
            return $roleModel->select('id, name, summary, deleted_at')
                             ->withDeleted()->findAll();
        }

        return $roleModel->select('id, name, summary, deleted_at')
                         ->findAll();
    }

    /**
     * Despliega el formulario simple para añadir nuevos roles al sistema.
     *
     * @return string HTML del formulario.
     */
    public function displayAddRoleForm()
    {
        $data = [
            'formId' => '#Fm_role',
            'urlBtnSaveanother' => 'save_new_role',
            'urlBtnSaveNedit' => 'save_new_role',
            'urlBtnSave' => 'save_new_role',
            'urlBtnDelete' => 'delete_role',
            'urlBtnDisable' => 'disable_role',
            'urlBtnEnable' => 'enable_role',
        ];

        return view('system/role_form', $data);
    }

    /**
     * Procesa el ingreso de nuevos roles al sistema.
     *
     * @return string|false JSON o false si falló json_encode().
     */
    public function processSavingRoleNew()
    {
        session()->set('ip_request', $this->request->getIPAddress());
        $postData = $this->request->getPost();
        $roleEntity = new RoleEntity($postData);
        $roleModel = new RoleModel();
        $roleID =  $roleModel->insert($roleEntity);

        if (! $roleID)
        {
            return json_response_validation($roleModel->errors());
        }

        $notification = [
            'msg'  => $postData['name'] . ' ha sido registrado correctamente como rol.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);
        session()->set('updating_role_id', $roleID);

        if (isset($postData['edit']))
        {
            return json_response_success('', route_to('display_change_role_form'));
        }

        if (isset($postData['another']))
        {
            return json_response_success('', route_to('display_add_role_form'));
        }

        return json_response_success('', route_to('roles'));
    }

    /**
     * Renderiza el formulario avanzado con los datos actuales del rol para
     * modificarlos.
     *
     * @return string HTML.
     */
    public function displayChangeRoleForm()
    {
        $updatingRoleID = session()->get('updating_role_id');
        $updatingRoleData = $this->getAllRoleData($updatingRoleID);

        if (! $updatingRoleData['role'])
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $data = [
            'formId' => '#Fm_ch_role',
            'urlBtnSaveanother' => 'save_modified_role',
            'urlBtnSaveNedit' => 'save_modified_role',
            'urlBtnSave' => 'save_modified_role',
            'urlBtnDelete' => 'delete_role',
            'urlBtnDisable' => 'disable_role',
            'urlBtnEnable' => 'enable_role',
            'role' => $updatingRoleData['role'],
            'permission' => $updatingRoleData['permission'],
        ];

        return view('system/role_change_form', $data);

    }

    /**
     * Devuelve todos los datos aociados a un rol (nombre, sumario y permisos)
     * almacenados en la base de datos.
     *
     * @param integer $roleID
     * @return array
     */
    private function getAllRoleData(int $roleID): array
    {
        $roleModel = new RoleModel();
        $role = $roleModel->withDeleted()->find($roleID);
        $permissionModel = new PermisssionModel();
        $availablePermissions = $permissionModel->getRolePermission($roleID);

        return [
            'role'       => $role,
            'permission' => $availablePermissions,
        ];
    }

    /**
     * Procesa la modificación de los datos del rol.
     *
     * @return string|false JSON o false si falló json_encode().
     */
    public function processSavingRoleChanges()
    {
        session()->set('ip_request', $this->request->getIPAddress());
        $postData = $this->request->getPost();
        $updatingRoleID = session()->get('updating_role_id');

        if (! $updatingRoleID)
        {
            return json_response_error('Datos del Rol no encontrados.');
        }
        $roleModel = new RoleModel();
        $rol = $roleModel->withDeleted()->find($updatingRoleID);
        $rol->setName($postData['name']);
        $rol->setSummary($postData['summary']);

        if ($rol->hasChanged())
        {
            if (! $roleModel->save($rol))
            {
                return json_response_validation($roleModel->errors());
            }
        }

        $permissionIDs = isset($postData['permission_id']) ? $postData['permission_id'] : null;

        $this->processPermissionAssignment($updatingRoleID, $permissionIDs);

        $notification = [
            'msg'  => $postData['name'] . ' ha sido modificado correctamente.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);

        if (isset($postData['edit']))
        {
            return json_response_success('', route_to('display_change_role_form'));
        }

        if (isset($postData['another']))
        {
            return json_response_success('', route_to('display_add_role_form'));
        }

        return json_response_success('', route_to('roles'));
    }

    /**
     * Asigna, desasigna o reasigna los permisos al rol.
     *
     * @param integer    $roleID
     * @param array|null $permissionID
     * @return void
     */
    private function processPermissionAssignment(int $roleID, ?array $permissionID)
    {
        if ($roleID)
        {
            $rolePermissionModel = new RolesPermissionsModel();
            $rolePermissionEntity = new RolesPermissions();
            $rolePermissionEntity->setRoleId($roleID);

            if (isset($permissionID))
            {
                foreach ($permissionID as $permissionId)
                {
                    $rolePermissionEntity->setPermissionId($permissionId);

                    if ($rolePermissionModel->hasBeenDeleted($rolePermissionEntity))
                    {
                        $rolePermissionModel->rePermit($rolePermissionEntity);
                    }

                    if (! $rolePermissionModel->isRoleAlreadyPermitted($rolePermissionEntity))
                    {
                        $rolePermissionModel->insert($rolePermissionEntity);
                    }
                }

                $unassignedPermission = $rolePermissionModel->allowCallbacks(false)
                                                            ->where('role_id', $rolePermissionEntity->role_id)
                                                            ->whereNotIn('permission_id', $permissionID)
                                                            ->findAll();
                foreach ($unassignedPermission as $permission)
                {
                    $rolePermissionModel->delete($permission->id);
                }
            }
            else
            {
                $unassignedPermission = $rolePermissionModel->allowCallbacks(false)
                                                            ->where('role_id', $rolePermissionEntity->role_id)
                                                            ->findAll();
                foreach ($unassignedPermission as $permission)
                {
                    $rolePermissionModel->delete($permission->id);
                }
            }
        }
    }

    /**
     * Devuelve el HTML del modal con todos los datos asociados al rol.
     *
     * @return string HTML del modal.
     */
    public function displayRoleDetails()
    {
        $selectedRow = $this->request->getGet();
        $this->checkDtbRow($selectedRow);

        return view('system/role_modal_detail', $this->getAllRoleData($selectedRow['id']));
    }

    /**
     * Devuelve un JSON con la uri para redireccionar al formulario avanzado
     * de modificación de un rol.
     *
     * @return string JSON o false si falló json_encode().
     */
    public function displayChangeRoleFormFromDtb()
    {
        $selectedRow = $this->request->getGet();
        $this->checkDtbRow($selectedRow);

        session()->set('updating_role_id', $selectedRow['id']);
        $uri = route_to('display_change_role_form');

        return json_response_success('', $uri);
    }

    /**
     * Habilita un rol anteriormente desactivado.
     *
     * @param integer $dtbFlag Determina si se invoca desde el datatable.
     * @return void
     */
    public function enableRole(int $dtbFlag = null)
    {
        $roleModel = new RoleModel();
        session()->set('ip_request', $this->request->getIPAddress());
        $roleID = session('updating_role_id');
        $uri = route_to('display_change_role_form');

        if ($dtbFlag)
        {
            $selectedRow = $this->request->getPost();
            $this->checkDtbRow($selectedRow);
            $roleID = $selectedRow['id'];
            $uri = route_to('roles');
        }

        $roleName = $roleModel->withDeleted()->find($roleID)->name;

        if (! $roleModel->undoSoftDelete($roleID))
        {
            $notification = [
                'msg'  => 'No se pudo habilitar el rol ' . $roleName,
                'type' => 'warning',
            ];
        }

        $notification = [
            'msg'  => 'El rol: ' . $roleName . ', está habilitado.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);

        return json_response_success('', $uri);
    }

    /**
     * Deshabilita un rol.
     *
     * @param integer $dtbFlag Determina si se invoca desde el datatable.
     * @return void
     */
    public function disableRole(int $dtbFlag = null)
    {
        $roleModel = new RoleModel();
        session()->set('ip_request', $this->request->getIPAddress());
        $roleID = session('updating_role_id');
        $uri = route_to('display_change_role_form');

        if ($dtbFlag)
        {
            $selectedRow = $this->request->getPost();
            $this->checkDtbRow($selectedRow);
            $roleID = $selectedRow['id'];
            $uri = route_to('roles');
        }

        $roleName = $roleModel->withDeleted()->find($roleID)->name;

        if (! $roleModel->delete($roleID))
        {
            $notification = [
                'msg'  => 'No se pudo deshabilitar el rol ' . $roleName,
                'type' => 'warning',
            ];
        }

        $notification = [
            'msg'  => 'El rol: ' . $roleName . ', está deshabilitado.',
            'type' => 'success'
        ];
        session()->setFlashdata('page_notification', $notification);
        return json_response_success('', $uri);
    }

    /**
     * Elimina **permanentemente** un rol.
     *
     * @param integer $dtbFlag Determina si se invoca desde el datatable.
     * @return void
     */
    public function deleteRole(int $dtbFlag = null)
    {
        $roleModel = new RoleModel();
        session()->set('ip_request', $this->request->getIPAddress());
        $roleID = session('updating_role_id');
        $uri = route_to('display_change_role_form');

        if ($dtbFlag)
        {
            $selectedRow = $this->request->getPost();
            $this->checkDtbRow($selectedRow);
            $roleID = $selectedRow['id'];
            $uri = route_to('roles');
        }

        $roleName = $roleModel->withDeleted()->find($roleID)->name;

        if (! $roleModel->delete($roleID, true))
        {
            $notification = [
                'msg'  => 'No se pudo eliminar permanentemente el rol ' . $roleName,
                'type' => 'warning',
            ];
        }

        $trace = ['role_id' => $roleID, 'role_name' => $roleName,];
        $this->logDeleteTrace($trace);

        $notification = [
            'msg'  => 'El rol: ' . $roleName . ', ha sido eliminado permanentemente.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);

        return json_response_success('', $uri);
    }

    /**
     * Valida los datos enviados desde un datatable.
     *
     * @param array $dtbRow Datos de la fila del datatable.
     * @throws PageNotFoundException Si no existen los campos de la entidad.
     * @return void
     */
    private function checkDtbRow(array $dtbRow)
    {
        if (! isset($dtbRow['name'], $dtbRow['id']))
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        if (! $dtbRow['id'])
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $roleModel = new RoleModel();

        if (! $roleModel->isValidDtbRow($dtbRow))
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    /**
     * Registra en la BD la traza del eliminado permanente ejecutado
     * por el usuario.
     *
     * @param array $data
     * @return void
     */
    private function logDeleteTrace(array $data)
    {
        $msg = 'eliminado permanentemente ' . $data['role_name'] . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['role_id'],
            'object_repr' => $data['role_name'],
            'object_type' => 'rol',
            'action_flag' => ACTION_FLAG_DELETE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);
    }
}
