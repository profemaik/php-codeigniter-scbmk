<?php namespace App\Controllers\System;

use App\Controllers\BaseController;
use App\Models\UserDataModel;
use App\Entities\UserData;
use App\Entities\UserLog;
use App\Entities\UserPersonalData;
use App\Entities\UsersPermissions;
use App\Entities\UsersRoles;
use App\Models\PermisssionModel;
use App\Models\RoleModel;
use App\Models\UserLogModel;
use App\Models\UserPersonalDataModel;
use App\Models\UsersPermissionsModel;
use App\Models\UsersRolesModel;

/**
 * Gestiona los usuarios del sistema.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class User extends BaseController
{
    public function index($withDeleted = '')
    {
        session()->remove('updating_user_id');
        $data['users'] = $this->getUsers($withDeleted);
        $data['withDeleted'] = $withDeleted;

        return view('system/user_list', $data);
    }

    /**
     * Busca y devuelve los registros de los usuarios almacenados en la base
     * de datos.
     *
     * @param string $withDeleted Indica si debe buscar registros eliminados.
     * @return object Entidad UserData.
     */
    private function getUsers($withDeleted = '')
    {
        $userModel = new UserDataModel();

        if ($withDeleted)
        {
            return $userModel->select('user_data.id, username, id_card_num, names, lastnames, user_data.deleted_at')
                             ->join('user_personal_data upd', 'upd.user_data_id = user_data.id')
                             ->withDeleted()
                             ->findAll();
        }

        return $userModel->select('user_data.id, username, id_card_num, names, lastnames, user_data.deleted_at')
                         ->join('user_personal_data upd', 'upd.user_data_id = user_data.id')
                         ->findAll();
    }

    /**
     * Despliega el formulario simple para añadir nuevos usuarios al sistema.
     *
     * @return string HTML del formulario.
     */
    public function displayAddUserForm()
    {
        $data = [
            'formId' => '#Fm_user',
            'urlBtnSaveanother' => 'save_new_user',
            'urlBtnSaveNedit' => 'save_new_user',
            'urlBtnSave' => 'save_new_user',
            'urlBtnDelete' => 'delete_user',
            'urlBtnDisable' => 'disable_user',
            'urlBtnEnable' => 'enable_user',
        ];

        return view('system/user_form', $data);
    }

    /**
     * Procesa el ingreso de nuevos usuarios al sistema.
     *
     * @return string|false JSON o false si falló json_encode().
     */
    public function processSavingUserNew()
    {
        if (! $this->validate('addUser'))
        {
            return json_response_validation($this->validator->getErrors());
        }

        session()->set('ip_request', $this->request->getIPAddress());
        $postData = $this->request->getPost();
        $userID = $this->saveUserNew($postData);

        if (! $userID)
        {
            return json_response_error();
        }

        $trace = [
            'user_id'     => $userID,
            'username'    => $postData['username'],
            'id_card_num' => $postData['id_card_num'],
        ];
        $this->logInsertTrace($trace);

        $notification = [
            'msg'  => $postData['username'] . ' ha sido registrado correctamente como usuario.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);
        session()->set('updating_user_id', $userID);

        if (isset($postData['edit']))
        {
            return json_response_success('', route_to('display_change_user_form'));
        }

        if (isset($postData['another']))
        {
            return json_response_success('', route_to('display_add_user_form'));
        }

        return json_response_success('', route_to('users'));
    }

    /**
     * Registra el número de cédula y el nombre de usuario en la base de datos.
     *
     * @param array $inputData Datos enviados desde el formulario simple.
     * @return integer|false ID del usuario o false si no se creó.
     */
    private function saveUserNew(array $inputData)
    {
        $userEntity = new UserData($inputData);
        $userEntity->setPassword();
        $userModel = new UserDataModel();
        $userID = $userModel->insert($userEntity);

        if (! $userID)
        {
            return false;
        }

        $userPdataEntity = new UserPersonalData($inputData);
        $userPdataEntity->setUserDataId($userID)
                        ->setEmails(['main' => '', 'aux' => ''])
                        ->setPhoneNums(['main' => '', 'aux' => '']);
        $userPdataModel = new UserPersonalDataModel();
        $userPdataID = $userPdataModel->insert($userPdataEntity);

        if (! $userPdataID)
        {
            $userModel->delete($userID, true);
            return false;
        }

        return $userID;
    }

    /**
     * Registra en la BD la traza del ingreso de un nuevo usuario.
     *
     * @param array $data Contiene ID del registro, nombre de usuario y número
     *                    de cédula.
     * @return void
     */
    private function logInsertTrace(array $data)
    {
        $msg = 'ingresado con número de cédula: '
               . $data['id_card_num'] . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['user_id'],
            'object_repr' => $data['username'],
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_ADD,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);
    }

    /**
     * Renderiza el formulario avanzado con los datos actuales del usuario para
     * modificarlos.
     *
     * @return string HTML.
     */
    public function displayChangeUserForm()
    {
        $updatingUserID = session()->get('updating_user_id');
        $updatingUserData = $this->getAllUserData($updatingUserID);

        if (! $updatingUserData['user'])
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $data = [
            'formId' => '#Fm_ch_user',
            'urlBtnSaveanother' => 'save_modified_user',
            'urlBtnSaveNedit' => 'save_modified_user',
            'urlBtnSave' => 'save_modified_user',
            'urlBtnDelete' => 'delete_user',
            'urlBtnDisable' => 'disable_user',
            'urlBtnEnable' => 'enable_user',
            'user' => $updatingUserData['user'],
            'upd' => $updatingUserData['upd'],
            'roles' => $updatingUserData['roles'],
            'permission' => $updatingUserData['permission'],
        ];

        return view('system/user_change_form', $data);

    }

    /**
     * Devuelve toda la data del usuario (data personal, roles,
     * permisos,usuario) almacenada en la base de datos.
     *
     * @param integer $userID
     * @return array
     */
    private function getAllUserData(int $userID): array
    {
        $userDataModel = new UserDataModel();
        $user = $userDataModel->allowCallbacks(false)
                              ->withDeleted()
                              ->find($userID);
        $userPdataModel = new UserPersonalDataModel();
        $upd = $userPdataModel->allowCallbacks(false)
                              ->withDeleted()
                              ->where('user_data_id', $userID)
                              ->first();
        $roleModel = new RoleModel();
        $availableRoles = $roleModel->getAssignedRol($userID);
        $permissionModel = new PermisssionModel();
        $availablePermissions = $permissionModel->getUserPermission($userID);

        return [
            'user'       => $user,
            'upd'        => $upd,
            'roles'      => $availableRoles,
            'permission' => $availablePermissions,
        ];
    }

    /**
     * Procesa la modificación de los datos del usuario.
     *
     * @return string|false JSON o false si falló json_encode().
     */
    public function processSavingUserChanges()
    {
        if (! $this->validate('changeUser'))
        {
            return json_response_validation($this->validator->getErrors());
        }

        session()->set('ip_request', $this->request->getIPAddress());
        $postData = $this->request->getPost();
        $updatingUserID = session()->get('updating_user_id');

        if (! $this->saveUserChanges($postData, $updatingUserID))
        {
            return json_response_error('Datos del Usuario no encontrados.');
        }

        $roleIDs = isset($postData['role_id']) ? $postData['role_id'] : null;
        $permissionIDs = isset($postData['permission_id']) ? $postData['permission_id'] : null;

        $this->processRoleAssignment($updatingUserID, $roleIDs);
        $this->processPermissionAssignment($updatingUserID, $permissionIDs);

        $notification = [
            'msg'  => $postData['username'] . ' ha sido modificado correctamente.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);

        if (isset($postData['edit']))
        {
            return json_response_success('', route_to('display_change_user_form'));
        }

        if (isset($postData['another']))
        {
            return json_response_success('', route_to('display_add_user_form'));
        }

        return json_response_success('', route_to('users'));
    }

    /**
     * Persiste, en la base de datos, los cambios realizados en los datos
     * personales y propios del usuario.
     *
     * @param array $inputData Datos enviados desde el formulario avanzado.
     * @param int $userID Identificador de la entidad user_data.
     * @return boolean
     */
    private function saveUserChanges(array $inputData, int $userID)
    {
        if (! $userID)
        {
            return false;
        }

        $userDataModel = new UserDataModel();
        $userData = $userDataModel->allowCallbacks(false)
                                  ->withDeleted()
                                  ->find($userID);
        $userData->setUsername($inputData['username']);
        isset($inputData['is_super']) ? $userData->setIsSuper('t') : $userData->setIsSuper();
        isset($inputData['is_admin']) ? $userData->setIsAdmin('t') : $userData->setIsAdmin();

        if ($userData->hasChanged())
        {
            if (! $userDataModel->save($userData))
            {
                return false;
            }
        }

        $userPersonalDataModel = new UserPersonalDataModel();
        $userPersonalData = $userPersonalDataModel->allowCallbacks(false)
                                                  ->withDeleted()
                                                  ->where('user_data_id', $userID)
                                                  ->first();
        $userPersonalData->setNationality($inputData['nationality']);
        $userPersonalData->setIdCardNum($inputData['id_card_num']);
        $userPersonalData->setNames($inputData['names']);
        $userPersonalData->setLastnames($inputData['lastnames']);
        $userPersonalData->setGender($inputData['gender']);
        $userPersonalData->setEmails($inputData['emails']);
        $userPersonalData->setPhoneNums($inputData['phone_nums']);

        if ($userPersonalData->hasChanged())
        {
            if (! $userPersonalDataModel->save($userPersonalData))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Asigna, desasigna o reasigna los roles al usuario.
     *
     * @param integer    $userID
     * @param array|null $roleID
     * @return void
     */
    private function processRoleAssignment(int $userID, ?array $roleID)
    {
        if ($userID)
        {
            $userRolesModel = new UsersRolesModel();
            $userRolesEntity = new UsersRoles();
            $userRolesEntity->setUserId($userID);

            if (isset($roleID))
            {
                foreach ($roleID as $rolId)
                {
                    $userRolesEntity->setRoleId($rolId);

                    if ($userRolesModel->hasBeenDeleted($userRolesEntity))
                    {
                        $userRolesModel->reRol($userRolesEntity);
                    }

                    if (! $userRolesModel->isUserAlreadyInRole($userRolesEntity))
                    {
                        $userRolesModel->insert($userRolesEntity);
                    }
                }

                $unassignedRoles = $userRolesModel->allowCallbacks(false)
                                                  ->where('user_id', $userRolesEntity->user_id)
                                                  ->whereNotIn('role_id', $roleID)
                                                  ->findAll();
                foreach ($unassignedRoles as $role)
                {
                    $userRolesModel->delete($role->id);
                }
            }
            else
            {
                $unassignedRoles = $userRolesModel->allowCallbacks(false)
                                                  ->where('user_id', $userRolesEntity->user_id)
                                                  ->findAll();
                foreach ($unassignedRoles as $role)
                {
                    $userRolesModel->delete($role->id);
                }
            }
        }
    }

    /**
     * Asigna, desasigna o reasigna los permisos al usuario.
     *
     * @param integer    $userID
     * @param array|null $permissionID
     * @return void
     */
    private function processPermissionAssignment(int $userID, ?array $permissionID)
    {
        if ($userID)
        {
            $userPermissionModel = new UsersPermissionsModel();
            $userPermissionEntity = new UsersPermissions();
            $userPermissionEntity->setUserId($userID);

            if (isset($permissionID))
            {
                foreach ($permissionID as $permissionId)
                {
                    $userPermissionEntity->setPermissionId($permissionId);

                    if ($userPermissionModel->hasBeenDeleted($userPermissionEntity))
                    {
                        $userPermissionModel->rePermit($userPermissionEntity);
                    }

                    if (! $userPermissionModel->isUserAlreadyPermitted($userPermissionEntity))
                    {
                        $userPermissionModel->insert($userPermissionEntity);
                    }
                }

                $unassignedPermission = $userPermissionModel->allowCallbacks(false)
                                                            ->where('user_id', $userPermissionEntity->user_id)
                                                            ->whereNotIn('permission_id', $permissionID)
                                                            ->findAll();
                foreach ($unassignedPermission as $permission)
                {
                    $userPermissionModel->delete($permission->id);
                }
            }
            else
            {
                $unassignedPermission = $userPermissionModel->allowCallbacks(false)
                                                            ->where('user_id', $userPermissionEntity->user_id)
                                                            ->findAll();
                foreach ($unassignedPermission as $permission)
                {
                    $userPermissionModel->delete($permission->id);
                }
            }
        }
    }

    /**
     * Devuelve el HTML del modal con todos los datos asociados al usuario.
     *
     * @return string HTML del modal.
     */
    public function displayUserDetails()
    {
        $selectedRow = $this->request->getGet();
        $this->checkDtbRow($selectedRow);

        return view('system/user_modal_detail', $this->getAllUserData($selectedRow['id']));
    }

    /**
     * Devuelve un JSON con la uri para redireccionar al formulario avanzado
     * de modificación de un usuario.
     *
     * @return string JSON o false si falló json_encode().
     */
    public function displayChangeUserFormFromDtb()
    {
        $selectedRow = $this->request->getGet();
        $this->checkDtbRow($selectedRow);

        session()->set('updating_user_id', $selectedRow['id']);
        $uri = route_to('display_change_user_form');

        return json_response_success('', $uri);
    }

    /**
     * Habilita un rol anteriormente desactivado.
     *
     * @param integer $dtbFlag Determina si se invoca desde el datatable.
     * @return void
     */
    public function enableUser(int $dtbFlag = null)
    {
        session()->set('ip_request', $this->request->getIPAddress());
        $userID = session('updating_user_id');
        $uri = route_to('display_change_user_form');

        if ($dtbFlag)
        {
            $selectedRow = $this->request->getPost();
            $this->checkDtbRow($selectedRow);
            $userID = $selectedRow['id'];
            $uri = route_to('users');
        }

        $userModel = new UserDataModel();
        $username = $userModel->allowCallbacks(false)
                              ->onlyDeleted()
                              ->find($userID)->username;

        if (! $userModel->undoSoftDelete($userID))
        {
            $notification = [
                'msg'  => 'No se pudo habilitar el usuario ' . $username,
                'type' => 'warning',
            ];
        }

        $notification = [
            'msg'  => 'El usuario: ' . $username . ', está habilitado.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);

        return json_response_success('', $uri);
    }

    /**
     * Deshabilita un usuario.
     *
     * @param integer $dtbFlag Determina si se invoca desde el datatable.
     * @return void
     */
    public function disableUser(int $dtbFlag = null)
    {
        $userModel = new UserDataModel();
        session()->set('ip_request', $this->request->getIPAddress());
        $userID = session('updating_user_id');
        $uri = route_to('display_change_user_form');

        if ($dtbFlag)
        {
            $selectedRow = $this->request->getPost();
            $this->checkDtbRow($selectedRow);
            $userID = $selectedRow['id'];
            $uri = route_to('users');
        }

        $username = $userModel->allowCallbacks(false)->find($userID)->username;

        if (! $userModel->delete($userID))
        {
            $notification = [
                'msg'  => 'No se pudo deshabilitar el usuario ' . $username,
                'type' => 'warning',
            ];
        }

        $notification = [
            'msg'  => 'El usuario: ' . $username . ', está deshabilitado.',
            'type' => 'success'
        ];
        session()->setFlashdata('page_notification', $notification);

        return json_response_success('', $uri);
    }

    /**
     * Elimina **permanentemente** un usuario.
     *
     * @param integer $dtbFlag Determina si se invoca desde el datatable.
     * @return void
     */
    public function deleteUser(int $dtbFlag = null)
    {
        $userModel = new UserDataModel();
        $userPersonalDataModel = new UserPersonalDataModel();
        session()->set('ip_request', $this->request->getIPAddress());
        $userID = session('updating_user_id');
        $uri = route_to('users');

        if ($dtbFlag)
        {
            $selectedRow = $this->request->getPost();
            $this->checkDtbRow($selectedRow);
            $userID = $selectedRow['id'];
        }

        $userData = $userModel->allowCallbacks(false)
                              ->withDeleted()
                              ->find($userID);
        $username = $userData->username;

        if (! $userModel->delete($userID, true))
        {
            $notification = [
                'msg'  => 'No se pudo eliminar permanentemente el usuario ' . $username,
                'type' => 'warning',
            ];
        }

        $trace = ['user_id' => $userID, 'username' => $username,];
        $this->logDeleteTrace($trace);

        $notification = [
            'msg'  => 'El usuario: ' . $username . ', ha sido eliminado permanentemente.',
            'type' => 'success',
        ];
        session()->setFlashdata('page_notification', $notification);

        return json_response_success('', $uri);
    }

    /**
     * Valida los datos enviados desde un datatable.
     *
     * @param array $dtbRow Datos de la fila del datatable.
     * @throws PageNotFoundException Si no existen los campos de la entidad.
     * @return void
     */
    private function checkDtbRow(array $dtbRow)
    {
        $dtbFields = isset($dtbRow['username'], $dtbRow['idCardNum'],
                           $dtbRow['names'], $dtbRow['lastnames'],
                           $dtbRow['id']);

        if (! $dtbFields)
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        if (! $dtbRow['id'])
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $userModel = new UserDataModel();

        if (! $userModel->isValidDtbRow($dtbRow))
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    /**
     * Registra en la BD la traza del eliminado permanente ejecutado
     * por el usuario.
     *
     * @param array $data
     * @return void
     */
    private function logDeleteTrace(array $data)
    {
        $msg = 'eliminado permanentemente ' . $data['username'] . '.';
        $trace = [
            'user_id'     => session('id'),
            'user_ip'     => session('ip_request'),
            'object_id'   => $data['user_id'],
            'object_repr' => $data['username'],
            'object_type' => 'usuario',
            'action_flag' => ACTION_FLAG_DELETE,
            'message'     => $msg,
        ];
        $userLogEntity = new UserLog($trace);
        $userLogModel = new UserLogModel();
        $userLogModel->save($userLogEntity);
    }

    /**
     * Procesa la subida y guardado del archivo de imagen de un usuario.
     *
     * @return string JSON o false si falló json_encode().
     */
    public function processUserImageUpload()
    {
        $file = $this->request->getFile('photo');

        if (! $file)
        {
            $errorMsg = 'No se reconoce el tipo de archivo.';

            return json_response_validation(['photo' => $errorMsg]);
        }

        if ($file->getSizeByUnit() > 120000)
        {
            $errorMsg = 'El tamaño (o peso) del archivo debe ser igual o menor a 120 kB.';

            return json_response_validation(['photo' => $errorMsg]);
        }

        if (! $file->isValid())
        {
            $errorMsg = $file->getErrorString().'('.$file->getError().')';

            return json_response_validation(['photo' => $errorMsg]);
        }

        $ext = $file->getExtension();
        $isValidExt = (
            $ext === 'jpg'
            || $ext === 'jpeg'
            || $ext === 'png'
            || $ext === 'svg'
            || $ext === 'bmp'
        );

        if (! $isValidExt)
        {
            $errorMsg = 'El tipo de archivo no es de Imagen.';

            return json_response_validation(['photo' => $errorMsg]);
        }

        $newPath = $file->store('assets/imgs/imgs_users/');

        if ($newPath)
        {
            $userID = session()->get('updating_user_id');

            $userPdataModel = new UserPersonalDataModel();
            $upd = $userPdataModel->allowCallbacks(false)
                                  ->withDeleted()
                                  ->where('user_data_id', $userID)
                                  ->first();

            $oldPath = $upd->photo;

            if (! $this->deleteOldFile($oldPath))
            {
                unlink(WRITEPATH . 'uploads/' . $newPath);

                return json_response_error('No se pudo borrar la imagen anterior');
            }

            $upd->setPhoto($newPath);

            if ($userPdataModel->save($upd) === false)
            {
                return json_response_validation($userPdataModel->errors());
            }
        }

        $uri = route_to('display_change_user_form');

        return json_response_success('', $uri);
    }

    /**
     * Debe eliminar el actual archivo de imagen de un usuario.
     *
     * @param string $path ruta relativa del archivo de imagen a borrar.
     * @return boolean true si se eliminó, false en caso contrario.
     */
    private function deleteOldFile(string $path)
    {
        if (! $path)
        {
            return true;
        }

        $oldFile = new \CodeIgniter\Files\File($path);

        if (! $oldFile->isFile())
        {
            return true;
        }

        if (! ($oldFile->isWritable() && unlink(WRITEPATH . 'uploads/' . $oldFile)))
        {
            return false;
        }

        return true;
    }
}
