<?php namespace App\Controllers;

use App\Entities\AccessRecovery as AccessRecoveryEntity;
use App\Models\AccessRecoveryModel;
use CodeIgniter\Encryption\Encryption;
use CodeIgniter\I18n\Time;

/**
 * Gestiona la recuperación del acceso al sistema por parte del usuario.
 *
 * @todo      Contabilizar los intentos fallidos de recuperación. Al tercer
 *            intento bloquear: ¿al usuario o las peticiones del usuario?.
 *            El punto es, que el acceso lo debe dar un admin del sistema.
 *            Además, cambiar el mensaje html por una vista propiamente.
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class AccessRecovery extends BaseController
{
    public function index()
    {
        echo view('system/auth/recover_password');
    }

	/**
	 * Procesa la petición de recuperación de la contraseña de un usuario.
	 *
	 * @return string|false JSON o false si json_encode() falló.
	 */
    public function recoveryRequest()
	{
		if (! $this->validate('recoverPsw'))
		{
			return json_response_validation($this->validator->getErrors());
		}

		$username = $this->request->getPost('username', FILTER_SANITIZE_STRING);
		$recoveryData = $this->getRecoveryData($username);
		$accessRecoveryModel = new AccessRecoveryModel();
		$pswRecoveryRequest = $accessRecoveryModel->where('username', $username)->first();
		if ($pswRecoveryRequest)
		{
			$pswRecoveryRequest->setToken($recoveryData['token']);
		}
		else
		{
			$pswRecoveryRequest = new AccessRecoveryEntity();
			$pswRecoveryRequest->setUserName($username);
			$pswRecoveryRequest->setToken($recoveryData['token']);
		}
		if ($accessRecoveryModel->save($pswRecoveryRequest))
		{
			return $this->sendRecoveryEmail($recoveryData);
		}
		else
		{
			echo 'No se guardó en la bd';exit;
			return json_response_error($accessRecoveryModel->errors(true));
		}
	}

	/**
	 * Pepara y devuelve el token y la url a ser usados para recuperar
	 * la contraseña de un usuario.
	 *
	 * @see checkRecovery
	 * @param string $username Nombre del usuario que solicitó la recuperación.
	 * @return array ['url' => 'http...', 'token' => 'cadenaHexadecimal']
	 */
	private function getRecoveryData(string $username): array
	{
		$token = bin2hex(Encryption::createKey(64));
		$uri = route_to('check_psw_change_request') . '?user=' . urlencode($username) . '&token=' . urlencode($token);
		$url = base_url($uri);

		return ['url' => $url, 'token' => $token, 'user' => $username];
	}

    /**
     * Envía un mensaje con la url de recuperación al correo electrónico del usuario.
     *
     * @todo Mejorar el html del mensaje.
     * @param array $url
     * @return string|false JSON o false si json_encode() falló.
     */
	private function sendRecoveryEmail(array $url)
	{
		$email = \Config\Services::email();
		$email->setFrom('carballom26@gmail.com', 'Maikel Carballo');
		//$email->setTo($usuario);
		$email->setTo('mcarballo@tutanota.com');

		$email->setSubject('SCBmk: Recuperación de acceso');
		$html = view('system/auth/email_recovery_content', ['url' => $url['url']]);
		$email->setMessage($html);

		if ($email->send())
		{
			return json_response_success(
				'La solicitud fue procesada correctamente. Puede cerrar esta ventana.'
			);
		}

		$model = new AccessRecoveryModel();
		$model->where('username', $url['user'])->delete();
		$msg = 'En estos momentos el servicio de correos está presentando fallas.';

		return json_response_error($msg);
	}

	/**
	 * Verifica si los datos de una petición de recuperación de contraseña
	 * existen en la base de datos del sistema.
	 *
	 * @return string Código HTML de la vista si las verificaciones fueron ok.
	 */
	public function checkRecovery()
    {
		$data = $this->request->getGet();

		if (! isset($data['user'], $data['token']))
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

		$model = new AccessRecoveryModel();
		$recoveryRequested = $model->where('username', $data['user'])
								 ->where('token', $data['token'])
								 ->first();

		if (! $recoveryRequested)
		{
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$createdAt = new Time($recoveryRequested->created_at);
		$diff = $createdAt->difference(Time::now());

		if ($diff->getMinutes() > 30)
		{
			$model->delete($recoveryRequested->id);
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		session()->setTempdata($data);

		return $this->response->redirect('display_change_password_form');

		//return view('system/auth/change_password');
    }
}
