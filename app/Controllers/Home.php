<?php

namespace App\Controllers;

use App\Models\NavItemModel;
use App\Models\PermisssionModel;
use App\Models\RoleModel;
use App\Models\UserDataModel;
use App\Models\UserLogModel;
use CodeIgniter\I18n\Time;

/**
 * Controlador por defecto del sistema.
 *
 * Gestiona el acceso y la salida de usuarios al sistema.
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class Home extends BaseController
{
    public function index()
    {
        return view('system/dashboard', $this->homeData());
    }

    /**
     * Recupera datos para ser mostrados/usados en la vista home.
     *
     * @return array
     */
    private function homeData(): array
    {
        $userModel = new UserDataModel();
        $roleModel = new RoleModel();
        $userLogModel = new UserLogModel();
        $usersCount = count($userModel->findAll());
        $rolesCount = count($roleModel->findAll());
        $userActivities = $userLogModel->where('user_id', session('id'))
                                       ->orderBy('created_at', 'desc')
                                       ->findAll(10);

        return [
            'usersCount'     => $usersCount,
            'rolesCount'     => $rolesCount,
            'userActivities' => $userActivities,
        ];
    }

    /**
     * Devuelve el HTML del formulario de inicio de sesión.
     *
     * @return string HTML del formulario de inicio de sesión.
     */
    public function displayLoginForm()
    {
        return view('system/auth/login');
    }

    /**
     * Devuelve el HTML del formulario de cambio de contraseña.
     *
     * @return string HTML del formulario de cambio de contraseña.
     */
    public function displayChangePasswordForm()
    {
        return view('system/auth/change_password');
    }

    /**
     * @todo Registrar logueos fallidos y correctos de los usuarios.
     * @return string|false JSON o false si no pudo crear la cadena.
     */
    public function login()
    {
        if (! $this->validate('login'))
        {
            return json_response_validation($this->validator->getErrors());
        }

        session()->set('ip_request', $this->request->getIPAddress());
        $inputUsername = $this->request->getPost('email', FILTER_SANITIZE_STRING);
        $userModel = new UserDataModel();
        $user = $userModel->getUserData($inputUsername);

        return $this->userSession($user);
    }

    /**
     * Obtiene los permisos del usuario (en caso de que no sea super)
     * e inicia la sesión de éste.
     *
     * @param array $user Datos del usuario registrado.
     * @return string|false JSON para la vista.
     */
    private function userSession(array $user)
    {
        if ($user['is_super'] === 'f')
        {
            $permissionModel = new PermisssionModel();
            $userPermissionIds = $permissionModel->getPermissionIds($user);
            $navItemModel = new NavItemModel();
            $user['user_nav_items'] = $navItemModel->getUserNavItems($userPermissionIds);
            $user['user_nav_item_codenames'] = $permissionModel->getPermissionCodenames($user['id']);
        }
        $user['sys_nav_items'] = $this->superUserMenu();

        return $this->initSession($user);
    }

    /**
     * Construye el menú completo del sistema.
     *
     * @return array
     */
    private function superUserMenu(): array
    {
        $navItemModel = new NavItemModel();
        $lvlZeroNavItems = $navItemModel->getNavItemLvl(0);
        foreach ($lvlZeroNavItems as &$ilvlZero)
        {
            $lvlOneNavItems = $navItemModel->getNavItemLvl($ilvlZero['iid']);
            foreach ($lvlOneNavItems as &$ilvlOne)
            {
                $lvlTwoNavItems = $navItemModel->getNavItemLvl($ilvlOne['iid']);
                foreach ($lvlTwoNavItems as &$ilvlTwo)
                {
                    $lvlThreeNavItems = $navItemModel->getNavItemLvl($ilvlTwo['iid']);
                    $ilvlTwo['nav_lvl_3'] = $lvlThreeNavItems;
                }
                unset($ilvlTwo);
                $ilvlOne['nav_lvl_2'] = $lvlTwoNavItems;
            }
            unset($ilvlOne);
            $ilvlZero['nav_lvl_1'] = $lvlOneNavItems;
        }
        unset($ilvlZero);

        return $lvlZeroNavItems;
    }

    /**
     * Establece en sesión los datos del usuario.
     *
     * @param array $user Datos del usuario.
     * @throws RuntimeException Si no existe el id del usuario en sesión.
     * @return string|false JSON o false si no pudo crear la cadena.
     */
    private function initSession(array $user)
    {
        unset($user['password']);
        $session = session();
        $session->set($user);

        if (! $session->has('id'))
        {
            throw new \RuntimeException('No se pudo establecer el ID del usuario.');
        }

        $session->set('login', 'correct');
        $userModel = new UserDataModel();
        $userData = $userModel->find(session('id'));
        $userData->setLastLogin(Time::now()->toDateTimeString());

        if ($userData->hasChanged())
        {
            $userModel->save($userData);
        }

        return json_response_success();
    }

    /**
     * Cambia la contraseña de un usuario.
     *
     * @return string|false JSON o false si falló json_encode().
     */
    public function setUserPassword()
    {
		if (! $this->validate('changePsw'))
		{
			return json_response_validation($this->validator->getErrors());
        }

        $userModel = new UserDataModel();
		$user = $userModel->find(session('id'));
		$newPassword = $this->request->getPost('password');
        $user->setPassword($newPassword)->setIsPswSet('t');

		if (! $userModel->save($user))
		{
            return json_response_validation($userModel->errors());
        }

		session()->set(['is_psw_set' => 't']);

		return json_response_success();
    }

    public function logout()
    {
        session()->destroy();
        $this->response->redirect('/');
    }

    /**
     * Devuelve el HTML del datatable de las actividades del usuario.
     *
     * @return string HTML.
     */
    public function displayUserLog()
    {
        return view('system/activity_log');
    }
}
