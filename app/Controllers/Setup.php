<?php

namespace App\Controllers;

/**
 * Instalador del sistema.
 *
 * Versión previa.
 *
 * @author    Maikel Carballo <mcarballo@tutanota.com>.
 * @copyright 2020 Maikel Carballo.
 */
class Setup extends BaseController
{
    public function index()
    {
        echo view('system/setup');
    }

    /**
     * Ejecuta los procesos para instalar la Base de Datos del sistema.
     *
     * @todo Refactorizar ya que Docker se encarga de inicializar la base de datos.
     * @return string|false JSON o false si falló json_encode().
     */
    public function install()
    {
        try
        {
            /*if (! $this->create_db())
            {
                return json_response_error('No se pudo crear la base de datos.');
            }*/

            $migrate = \Config\Services::migrations();

            if (! $migrate->latest('default'))
            {
                return json_response_error('No se pudo migrar la base de datos.');
            }

            $seeder = \Config\Database::seeder();
            $seeder->call('Seeder');

            return json_response_success();
        }
        catch (\Throwable $th)
        {
            return json_response_error($th->getMessage());
        }
    }

    /**
     * Prepara la Base de Datos del sistema.
     *
     * @deprecated 0.2.0
     * @return BaseResult|Query|false
     */
    private function setup_db ()
    {
        if ($this->create_db())
        {
            return $this->create_db_schemas();
        }

        return $this->create_db();
    }

    /**
     * Crea la Base de Datos del sistema usando la configuración establecida en
     * el grupo de conexiones *installer*.
     *
     * @return boolean true si se creó la BD o false en caso contrario.
     */
    private function create_db()
    {
        $forge = \Config\Database::forge('installer');

        return $forge->createDatabase('scbmk');
    }

    /**
     * Crea los esquemas necesarios dentro de la Base de Datos.
     *
     * @deprecated 0.2.0
     * @return BaseResult|Query|false
     */
    private function create_db_schemas()
    {
        $db = \Config\Database::connect('default');

        if (! $db->query('CREATE SCHEMA IF NOT EXISTS "kernel"'))
        {
            return false;
        }

        if (! $db->query('CREATE SCHEMA IF NOT EXISTS "audit"'))
        {
            return false;
        }

        if (! $db->query('CREATE SCHEMA IF NOT EXISTS "common"'))
        {
            return false;
        }

        return (bool) $db->query('ALTER DATABASE scbmk SET search_path TO "$user",public,kernel,common,audit');
    }

    /**
     * Destruye la Base de Datos y luego la vuelve a crear en su estado inicial.
     *
     * @return string|false JSON o false si falló json_encode().
     */
    public function reinstall()
    {
        if ($this->destroy_db())
        {
            $this->install();

            return json_response_success();
        }
    }

    /**
     * Destruye la base de datos usando la configuración establecida en
     * el grupo de conexiones *installer*.
     *
     * **ATENCIÓN** pérdida total de datos, no realiza respaldos.
     *
     * @return boolean
     */
    private function destroy_db()
    {
        $forge = \Config\Database::forge('installer');

        return $forge->dropDatabase('scbmk');
    }
}
