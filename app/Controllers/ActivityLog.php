<?php namespace App\Controllers;

use App\Models\UserLogModel;
use CodeIgniter\I18n\Time;

class ActivityLog extends BaseController
{
    public function index()
    {
        $draw = $this->request->getGet('draw', FILTER_SANITIZE_NUMBER_INT);
        $start = $this->request->getGet('start', FILTER_SANITIZE_NUMBER_INT);
        $length = $this->request->getGet('length', FILTER_SANITIZE_NUMBER_INT);
        $search = $this->request->getGet('search');
        $order = $this->request->getGet('order');
        $columns = $this->request->getGet('columns');

        $params = [
            'draw'    => intval($draw),
            'start'   => $start,
            'length'  => $length,
            'search'  => $search,
            'order'   => $order,
            'columns' => $columns
        ];

        if (! is_numeric($draw))
        {
            $params['draw'] = 0;
        }

        if (! is_numeric($start))
        {
            $params['start'] = 0;
        }

        if (! is_numeric($length))
        {
            $params['length'] = 0;
        }

        $isSearch = is_array($search) && (isset($search['value'], $search['regex']) && ! empty($search['value']));

        if (! $isSearch)
        {
            $params['search'] = [];
        }

        $isOrderable = is_array($order) && (count($order));

        if (! $isOrderable)
        {
            $params['order'] = [];
        }

        $areColumns = is_array($columns) && (count($columns));

        if (! $areColumns)
        {
            $params['columns'] = [];
        }

        return $this->searchData($params);
    }

    private function searchData(array $params)
    {
        $orderBy = $params['order'];
        $columns = $params['columns'];
        $filtersApplied = false;

        $userLogModel = new UserLogModel();

        $recordsTotal = $userLogModel->where('user_id', session('id'))
                                     ->countAllResults();
        $recordsFiltered = $recordsTotal;

        $userLogModel->select(
            'user_ip AS ip_dir,
            object_repr AS obj_name,
            object_type AS obj_type,
            action_flag AS action,
            message AS detail,
            created_at AS timestamp'
        )->where('user_id', session('id'));

         $transliteratedAction = [
            'visualización' => ACTION_FLAG_VIEW,
            'modificación'  => ACTION_FLAG_CHANGE,
            'desactivación' => ACTION_FLAG_DISABLE,
            'activación'    => ACTION_FLAG_ENABLE,
            'eliminación'   => ACTION_FLAG_DELETE,
            'agregación'    => ACTION_FLAG_ADD,
            'inicia sesión' => ACTION_FLAG_LOGIN,
            'cierra sesión' => ACTION_FLAG_LOGOUT,
        ];

        foreach ($columns as $column)
        {
            if ($column['searchable'] && (! empty($column['search']['value'])))
            {
                if ($column['name'] === 'action')
                {
                    if ($this->isActionName($column['search']['value']))
                    {
                        $userLogModel->where('action_flag', mb_strtolower($transliteratedAction[$column['search']['value']]));
                        $filtersApplied = true;
                    }
                }

                if ($column['name'] === 'timestamp')
                {
                    $userLogModel->where("to_char(created_at, 'YYYY-MM-DD')", $column['search']['value']);
                    $filtersApplied = true;
                }

                if ($column['name'] === 'ip_dir')
                {
                    $userLogModel->like('text(user_ip)', $column['search']['value'], 'both');
                    $filtersApplied = true;
                }

                if ($column['name'] === 'obj_name')
                {
                    $userLogModel->like('object_repr', $column['search']['value']);
                    $filtersApplied = true;
                }

                if ($column['name'] === 'obj_type')
                {
                    $userLogModel->like('object_type', $column['search']['value']);
                    $filtersApplied = true;
                }
            }
        }

        foreach ($orderBy as $column)
        {
            if ($columns[$column['column']]['name'] === 'ip_dir')
            {
                $userLogModel->orderBy('user_ip', $column['dir']);
            }

            if ($columns[$column['column']]['name'] === 'obj_name')
            {
                $userLogModel->orderBy('object_repr', $column['dir']);
            }

            if ($columns[$column['column']]['name'] === 'obj_type')
            {
                $userLogModel->orderBy('object_type', $column['dir']);
            }

            if ($columns[$column['column']]['name'] === 'action')
            {
                $userLogModel->orderBy('action_flag', $column['dir']);
            }

            if ($columns[$column['column']]['name'] === 'detail')
            {
                $userLogModel->orderBy('message', $column['dir']);
            }

            if ($columns[$column['column']]['name'] === 'timestamp')
            {
                $userLogModel->orderBy('created_at', $column['dir']);
            }
        }

        $query = $userLogModel->getCompiledSelect(false);
        $data = $userLogModel->asArray()
                             ->findAll($params['length'], $params['start']);

        if ($filtersApplied)
        {
            $db = db_connect('default');
            $recordsFiltered = count($db->query($query)->getResultArray());
        }

        return json_encode([
            "draw"            => $params['draw'],
            "recordsTotal"    => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $this->formatData($data)
        ]);
    }

    /**
     * Determina si una cadena es un nombre de acción válido.
     *
     * @param string $str Cadena a verificar.
     * @return boolean true si es válido, false en caso contrario.
     */
    private function isActionName(string $str)
    {
        $actionNames = [
            'visualización',
            'modificación',
            'desactivación',
            'activación',
            'eliminación',
            'agregación',
            'inicia sesión',
            'cierra sesión',
        ];

        return in_array(mb_strtolower($str), $actionNames, true);
    }
    /**
     * Formatea los datos a ser presentados.
     *
     * @param array $data
     * @return array
     */
    private function formatData(array $data)
    {
        $transliteratedFlag = [
            ACTION_FLAG_VIEW    => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_VIEW] .    ' text-dark">Visualización</span>',
            ACTION_FLAG_CHANGE  => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_CHANGE] .  '">Modificación</span>',
            ACTION_FLAG_DISABLE => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_DISABLE] . '">Desactivación</span>',
            ACTION_FLAG_ENABLE  => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_ENABLE] .  '">Activación</span>',
            ACTION_FLAG_DELETE  => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_DELETE] .  '">Eliminación</span>',
            ACTION_FLAG_ADD     => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_ADD] .     '">Agregación</span>',
            ACTION_FLAG_LOGIN   => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_LOGIN] .   '">Inicia sesión</span>',
            ACTION_FLAG_LOGOUT  => '<span class="badge badge-pill badge-' . BG_COLOR_CSS[ACTION_FLAG_LOGOUT] .  '">Cierra sesión</span>',
        ];
        $formattedData = [];

        foreach ($data as $row)
        {
            foreach ($row as $key => &$value)
            {
                if ($key === 'action')
                {
                    $value = $transliteratedFlag[$value];
                }

                if ($key === 'obj_type')
                {
                    $value = ucfirst($value);
                }

                if ($key === 'detail')
                {
                    $value = ucfirst($value);
                }

                if ($key === 'timestamp')
                {
                    $time = Time::parse($value);
                    $value = $time->toLocalizedString("EEEE, d 'de' MMMM 'de' yyyy 'a las' h:mm:ss a VVVV");
                }
            }

            unset($value);
            $formattedData[] = $row;
        }

        return $formattedData;
    }
}
