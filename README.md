# SCBmk

Sistema de Control Base mk

[![Product Name Screen Shot][product-screenshot]](https://gitlab.com/profemaik/php-codeigniter-scbmk)

Plantilla base para sistemas de tipo dashboard o panel de control. Esto permite, añadir funcionalidades de manera modular al sistema sin afectar a otras funcionalidades o módulos.

El diseño del front-end está basado en la plantilla de código abierto SB Admin 2, la cual, ofrece un diseño visual agradable, simple y fácil de usar.

## Tabla de Contenidos

1. [Construido con](#construido-con)
2. [Primeros Pasos](#primeros-pasos)
3. [Uso](#uso)
4. [Hoja de Ruta](#hoja-de-ruta)
5. [Contribuir](#contribuir)
6. [Licencia](#licencia)
7. [Contacto](#contacto)
8. [Agradecimientos](#agradecimientos)

## Construido con

* Back-end
  * [CodeIgniter 4](https://codeigniter.com/)
  * [PostgreSQL 13](https://www.postgresql.org/)
* Front-end
  * [jQuery](https://jquery.com/)
  * [Node.js](https://nodejs.org/)
  * [Webpack](https://webpack.js.org/)

## Primeros Pasos

El SCBmk, al estar basado en CodeIgniter 4, le permite establecer las configuraciones esenciales del sistema (conexiones a bases de datos, URL base del sistema, localizaciones, internacionalización, etc.) bien en archivos de clases PHP o en variables de entorno.

**Siempre prefiera establecer estas configuraciones en variables de entorno, especificándolas en un archivo `.env`, el cual debe existir en el directorio raíz del sistema.** Más información en: [Configuración inicial de CodeIgniter].

### Prerequisitos

* PHP 7.2 o superior (*por ahora incompatible con PHP 8*).
* Servidor de Base de Datos PostgreSQL 10 o superior.

Si desea desarrollar nuevas funcionalidades al sistema, también es necesario:

* Composer.
* Node 14.15 o superior.

### Instalación de prueba

1. Clone el repositorio

   ```sh
   git clone https://gitlab.com/profemaik/php-codeigniter-scbmk.git
   ```

2. Instale las dependencias del servidor back-end con Composer

   ```sh
   composer install
   ```

3. Defina la configuración de la base de datos en el archivo ubicado en `app/Config/Database.php`, o mejor aún, en un archivo `.env`. Se deben definir dos grupos distintos de conexión: el grupo `default`, que será la conexión a la base de datos del sistema; y el grupo `installer` que será la conexión al SGBD existente para poder crear la base de datos del sistema.
4. Estando en el directorio raíz ejecute, en consola (o terminal)

   ```sh
   php spark serve
   ```

5. Abra un navegador web y diríjase a la dirección `http://localhost:8080/preparar-sistema` y haga clic en el botón Instalar para crear la base de datos del sistema.
6. Espere a que finalice la instalación. Si el mensaje fue de éxito, ya está listo para usar el sistema.

### Instalación avanzada

Si va a instalar el sistema en un servidor Apache (virtualizado o no) recuerde que debe cambiar los permisos, __de manera recursiva__, del directorio `writable` para que Apache pueda escribir y leer en él. Además, se debe habilitar el módulo `rewrite`. Más información en: [Hospedaje con Apache en CodeIgniter].

1. Clone el repositorio

   ```sh
   git clone https://gitlab.com/profemaik/php-codeigniter-scbmk.git
   ```

2. Instale las dependencias del servidor back-end con Composer

   ```sh
   composer install
   ```

3. Si su intención es desarrollar nuevas funcionalidades al sistema, además de instalar las dependencias del back-end con Composer debe instalar las dependencias del front-end con NPM

   ```sh
   npm install
   ```

4. Defina la conexión a la base de datos en el archivo ubicado en `app/Config/Database.php`, o mejor aún, en un archivo `.env` que debe guardar en el directorio raíz. Se deben definir dos grupos distintos de conexión: el grupo default, que será la conexión a la base de datos del sistema; y el grupo installer que será la conexión al SGBD existente para poder crear la base de datos del sistema. Más información en: [Configurando Bases de Datos en CodeIgniter].
5. Defina la configuración general del sistema en el archivo ubicado en app/Config/App.php, o en un archivo `.env`. Más información en [Configuración inicial de CodeIgniter].
6. Si necesita realizar cambios en las migraciones o seeders de la base de datos este es el momento para hacerlo. Más información en: [Gestionando Bases de Datos en CodeIgniter]
7. Abra un navegador web y diríjase a la dirección `url_base/preparar-sistema` y haga clic en el botón Instalar para crear la base de datos del sistema.
8. Espere a que finalice la instalación. Si el mensaje fue de éxito, ya está listo para usar el sistema.

## Uso

Al ser una plantilla base, las funcionalidades que ofrece SCBmk son:

* Gestión de Usuarios
* Gestión de Grupos de Usuarios (Roles)

Los usuarios del sistema pueden ser de tres tipos:

1. Superusuarios: no tienen ningún tipo de restricciones.
2. Usuarios admin: para realizar tareas específicas de administradores de sistemas.
3. Usuarios normales: aquellos usuarios a los que se les han asignado permisos y que no deben ejecutar procesos relacionados con la estabilidad del sistema.

A pesar de esta distinción, a cualquier usuario se le pueden asignar permisos específicos o puede ser asignado a uno o más roles (grupos de usuarios) y, de esta manera, heredar los permisos del rol (o roles) asignado(s).

__No se deberían asignar permisos específicos ni roles a ningún superusuario, ya que por definición pueden ejecutar cualquier acción o proceso dentro del sistema.__

_Recuerde, este sistema es una plantilla. Puede ser usado para implementar nuevas funcionalidades que encajen dentro de aplicaciones de tipo panel de control._

## Hoja de Ruta

Eche un vistazo a las [incidencias](https://gitlab.com/profemaik/php-codeigniter-scbmk/-/issues) para que conozca nuevas funcionalidades propuestas y/o problemas conocidos. Siéntase libre de
solicitar nuevas funcionalidades o aportar nuevas ideas.

## Contribuir

Las contribuciones son las que hacen que la comunidad de código abierto sea un lugar tan increíble para aprender, inspirar y crear. Cualquier contribución que haga es **muy apreciada**.

1. Realice un fork del proyecto
2. Cree su rama feature (`git checkout -b feature/NuevaFuncionalidad`)
3. Confirme sus cambios (`git commit -m 'Agrega nueva funcionalidad'`)
4. Ejecute un Push a la rama (`git push origin feature/NuevaFuncionalidad`)
5. Abra una petición de fusión (Merge)

## Licencia

Se distribuye bajo la Licencia MIT. Lea `LICENSE` para más información.

## Contacto

Maikel Carballo - mcarballo@tutanota.com

Enlace del Proyecto: [SCBmk](https://gitlab.com/profemaik/php-codeigniter-scbmk)

## Agradecimientos

* [Start Bootstrap - SB Admin 2](https://github.com/StartBootstrap/startbootstrap-sb-admin-2)
* [DataTables](https://datatables.net/)
* [Lightbox](https://lokeshdhakar.com/projects/lightbox2/)
* [bsCustomFileInput](https://github.com/Johann-S/bs-custom-file-input)

_Las siguientes personas merecen mención especial, de ellas(os) he adquirido
conocimientos invaluables en mayor o menor medida:_

* Sr. Alexis Ontiveros
* Tomás Romero
* Ing. Oswaldo Graterol
* Ing. Maikel Hernández
* Idelina Ponte
* Keisy López

[Hospedaje con Apache en CodeIgniter]: https://codeigniter4.github.io/userguide/installation/running.html#hosting-with-apache
[Gestionando Bases de Datos en CodeIgniter]: https://codeigniter4.github.io/userguide/dbmgmt/index.html
[Configurando Bases de Datos en CodeIgniter]: https://codeigniter4.github.io/userguide/database/configuration.html
[Configuración inicial de CodeIgniter]: https://codeigniter4.github.io/userguide/installation/running.html#initial-configuration-set-up
[product-screenshot]: project_assets/screenshot_users.jpg
[Documentación]: https://gitlab.com/profemaik/php-codeigniter-scbmk/-/wikis/home
[Informar Error]: https://gitlab.com/profemaik/php-codeigniter-scbmk/-/issues
[Nuevas Ideas]: https://gitlab.com/profemaik/php-codeigniter-scbmk/-/issues
