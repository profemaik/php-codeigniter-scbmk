import { showAlert } from "./modules/alert/alert";
import {
    makeAjaxRequest, processDoneRequest, processFailedRequest
} from "./modules/process/ajaxRequests";

$(window).on('load', function() {
    $('#preloader')
    .fadeOut('slow', function() { $(this).css('visibility', 'hidden'); });
});

bsCustomFileInput.init();

$('.btn_action').on('click', function(e) {
    e.preventDefault();

    const button = $(this).data('button');
    const data = $(this).data();
    const waitingAlert = {type: 'info', msg: 'Procesando... por favor, espere'};

    switch (button) {
        case 'btn_install':
        case 'btn_reinstall':
            showAlert(waitingAlert);
            makeAjaxRequest(data, 'alert');
            break;
        case 'btn_login':
            $('#preloader').css('visibility', 'visible').show();
            makeAjaxRequest(data, 'check_error_alert_validate');
            break;
        case 'btn_change_psw':
            showAlert(waitingAlert);
            makeAjaxRequest(data, 'check_error_alert_validate');
            break;
        case 'btn_rcvpsw':
            showAlert(waitingAlert);
            makeAjaxRequest(data, 'alert_validate');
            break;
        case 'btn_save':
        case 'btn_saveanother':
        case 'btn_savedit':
        case 'btn_delete':
        case 'btn_disable':
        case 'btn_enable':
            makeAjaxRequest(data, 'check_error_alert_validate');
            break;

        default:
            break;
    }
});

$( '#swDisReg' ).change(function (e) {
    e.preventDefault();

    if ($(this).prop('checked') === true) {
        window.location.replace($(this).data('deleted'));
    } else {
        window.location.replace($(this).data('nodeleted'));
    }
});

$('#Permission_id').bootstrapDualListbox({
    nonSelectedListLabel: '<h4><span class="badge badge-primary">Permisos Disponibles</span></h3>',
    selectedListLabel: '<h4><span class="badge badge-success">Permisos Asignados</span></h3>',
    preserveSelectionOnMove: 'moved',
    filterPlaceHolder: 'Filtro',
    filterTextClear: '<span class="badge badge-primary">MOSTRAR TODOS</span>',
    moveAllLabel: 'Mover todos',
    removeAllLabel: 'Quitar todos',
    moveOnSelect: true,
    infoText: 'Mostrando todos {0}',
    infoTextEmpty: 'Ninguno',
    infoTextFiltered: '<span class="badge badge-warning">FILTRADOS</span> {0} de {1}',
});

$('#Role_id').bootstrapDualListbox({
    nonSelectedListLabel: '<h4><span class="badge badge-primary">Roles Disponibles</span></h3>',
    selectedListLabel: '<h4><span class="badge badge-success">Roles Asignados</span></h3>',
    preserveSelectionOnMove: 'moved',
    filterPlaceHolder: 'Filtro',
    filterTextClear: '<span class="badge badge-primary">MOSTRAR TODOS</span>',
    moveAllLabel: 'Mover todos',
    removeAllLabel: 'Quitar todos',
    moveOnSelect: true,
    infoText: 'Mostrando todos {0}',
    infoTextEmpty: 'Ninguno',
    infoTextFiltered: '<span class="badge badge-warning">FILTRADOS</span> {0} de {1}',
});

$('#inDetailModal').on('show.bs.modal', function (e) {
    const button = $(e.relatedTarget);
    const args = button.data();

    makeAjaxRequest(args, 'modal');
});

$('.dtb-btn-change').click(function (e) {
    e.preventDefault();

    const args = $(this).data();

    makeAjaxRequest(args, 'assign');
});

$('#confirmActionModal').on('shown.bs.modal', function (e) {
    const button = $(e.relatedTarget);
    const args = button.data();

    $('#mdl_btn_accept').on('click', function (e) {
        makeAjaxRequest(args, 'reload');
    });
});

$('#userPicFormModal').on('show.bs.modal', function (e) {
    $('#modal_alert_parent').empty();
    const file = $('#Photo');
    file.value = '';
    file.removeClass('is-invalid');
    file.next('.custom-file-label').text('Buscar archivo');
});

$( '#mdl_user_pic_btn_accept' ).click(function (e) {
    e.preventDefault();
    showAlert('info', 'Procesando... por favor, espere');

    const formId = $(this).data('form');
    const form = document.getElementById(formId.replace(/#/g, ''));

    $.ajax({
        method: $(this).data('method').toUpperCase(),
        url: $(this).data('url'),
        processData: false,
        data: new FormData(form),
        dataType: $(this).data('type'),
        contentType: false
    })
    .done(function(response) {
        const args = {
            whenDone: 'check_error_alert_validate',
            ajaxParams: $(this).data(),
            response: response
        };
        processDoneRequest(args);
    })
    .fail(function(response) {
        processFailedRequest(response);
    });
});

$('#userToolModal').on('show.bs.modal', function (e) {
    const item = $(e.relatedTarget);
    $('#userToolModalLabel').html(item.html())

    makeAjaxRequest(item.data(), 'modal');
});
