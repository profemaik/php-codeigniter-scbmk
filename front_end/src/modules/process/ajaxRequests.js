import { showAlert, closeAlert } from "./../alert/alert";
import { refreshCsrfToken } from "../security/csrf";
import { showVal } from "../form/inputValidation";
import { TRANSLATE_STATUS } from "../conf/failedRequestCodes";

/**
 * Ejecuta una petición HTTP AJAX usando jQuery.
 * @param {Object} params Parámetros de la petición.
 * @param {string} whenDone Qué hacer cuando se complete la petición.
 */
function makeAjaxRequest(params, whenDone) {

    $.ajax({
        method: params.method.toUpperCase(),
        url: params.url.toString(),
        data: getData(params),
        dataType: params.type
    })
    .done(function(response) {
        const args = {
            whenDone: whenDone,
            ajaxParams: params,
            response: response
        };
        processDoneRequest(args);
    })
    .fail(function(response) {
        processFailedRequest(response);
    });
}

/**
 * Devuelve los datos a ser enviados en una petición HTTP AJAX.
 * @param {Object} params Parámetros de la petición.
 */
function getData(params) {
    if (! params.form) {
        return params;
    }

    if (params.button === 'btn_saveanother') {
        return $(params.form).serialize() + '&another=true';
    }

    if (params.button === 'btn_savedit') {
        return $(params.form).serialize() + '&edit=true';
    }

    return $(params.form).serialize();
}

/**
 * Ejecuta las acciones necesarias tras completarse una petición AJAX.
 * @param {Object} params Contiene: whenDone, ajaxParams y response.
 */
function processDoneRequest(params) {
    switch (params.whenDone) {
        case 'check_error_alert_validate':
            if (params.response.alert.type === 'error') {
                if (params.ajaxParams.button === 'btn_login') {
                    $('#preloader')
                    .fadeOut('slow', function() {
                        $(this).css('visibility', 'hidden');
                    });
                }
                refreshCsrfToken(params.response.bsod);
                showAlert(params.response.alert);
                showVal(params.ajaxParams.form, params.response.wrong_fields);
                return;
            }
            window.location.replace(params.response.url);
            break;

        case 'alert_validate':
            refreshCsrfToken(params.response.bsod);
            showAlert(params.response.alert);
            showVal(params.ajaxParams.form, params.response.wrong_fields);
            break;

        case 'alert':
            refreshCsrfToken(params.response.bsod);
            showAlert(params.response.alert);
            break;

        case 'reload':
            window.location.reload();
            break;

        case 'assign':
            window.location.assign(params.response.url);
            break;

        case 'replace':
            window.location.replace(params.response.url);
            break;

        case 'href':
            window.location.href = params.response.url;
            break;

        case 'modal':
            const selector = params.ajaxParams.target + ' .modal-body';
            $(selector).html(params.response);
            break;

        default:
            break;
    }
}

/**
 * Despliega un modal que muestra una imagen y mensajes alusivos al estado de
 * la petición fallida.
 * @param {Object} response Respuesta de la petición fallida.
 */
function processFailedRequest(response) {
    closeAlert();
    $('#errqtModalLabel').text(TRANSLATE_STATUS[response.status].txt);
    $('#errqim').attr('src', TRANSLATE_STATUS[response.status].img)
    $('#errqms').text('ERROR: ' + response.status)
    $('#errqtx').text(response.responseText);
    $('#errqtt').text(response.statusText);

    $('.modal-action').on('shown.bs.modal', function () {
        $(this).modal('hide');
    });
    $('.modal-action').on('hidden.bs.modal', function () {
        $('#errqtModal').modal();
    });
    $('#errqtModal').modal();
}

export { makeAjaxRequest, processDoneRequest, processFailedRequest };
