import  * as Field from "./inputTypeChecker";

/**
 * Restablece todos los campos de un formulario a sus valores iniciales.
 * @param {string} formId Valor del atributo id del formulario.
 * @param {Array} exceptions Campos cuyos valores se desean mantener.
 */
function resetFields(formId, exceptions = []) {
    $(formId).hide();
    $(formId + ' *').filter(':input').each(function(index, field) {
        resetText(field, exceptions);
        resetMark(field, exceptions);
        resetSelect(field, exceptions);
    });
    return $(formId).fadeIn();
}

/**
 * Establece el valor del campo en una cadena vacía.
 * @param {Element} input Elemento HTML input.
 * @param {Array} exceptions Campos cuyos valores se desean mantener.
 */
function resetText(input, exceptions) {
    if (Field.isText(input)) {
        if (exceptions.length === 0) {
            input.value = '';
            resetCssStyle(input.id)
            return;
        }
        $.each(exceptions, function (index, value) {
            if (value !== input.id) {
                input.value = '';
                resetCssStyle(input.id);
            }
        });
    }
}

/**
 * Desmarca un campo checkbox o radio.
 * @param {Element} input Elemento HTML input.
 * @param {Array} exceptions Campos cuyos valores se desean mantener.
 */
function resetMark(input, exceptions) {
    if (Field.isMarkable(input)) {
        if (exceptions.length === 0) {
            document.getElementById(input.id).checked = false;
            resetCssStyle(input.id);
            return;
        }
        $.each(exceptions, function (index, value) {
            if (value !== input.id) {
                document.getElementById(input.id).checked = false;
                resetCssStyle(input.id);
            }
        });
    }
}

/**
 * Deselecciona las opciones en un campo select.
 * @param {Element} input Elemento HTML input.
 * @param {Array} exceptions Campos cuyos valores se desean mantener.
 */
function resetSelect(input, exceptions) {
    if (Field.isSelect()) {
        if (exceptions.length === 0) {
            input.selectedIndex = 0;
            $('.miselect2').val(null).trigger('change');
            resetCssStyle(input.id);
            return;
        }
        $.each(exceptions, function (index, value) {
            if (value !== input.id) {
                input.selectedIndex = 0;
                $('.miselect2').val(null).trigger('change');
                resetCssStyle(input.id);
            }
        });
    }
}

/**
 * Restablece el estilo CSS de un input.
 * @param {string} inputId Valor del atributo id del input.
 */
function resetCssStyle(inputId) {
    $("[id='" + inputId + "']").removeClass('is-invalid is-valid');
    $("[id='" + inputId + "']").parent('.desenfocar').removeClass('focused');
    $("[id='" + inputId + "'] + .select2 .select2-selection--single").css('border-color', '#e6e6e6');
}

export { resetFields };
