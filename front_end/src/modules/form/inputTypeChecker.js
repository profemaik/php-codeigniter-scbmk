/**
 * Verifica si en un input HTML se puede escribir texto.
 * @param {Element} inputElement Elemento HTML de tipo input.
 */
function isText(inputElement) {
    if (! inputElement) {

        return false;
    }
    let inputType = inputElement.type;
    let inputTag = inputElement.tagName;

    return (
        inputType === 'text'
        || inputType === 'password'
        || inputType === 'number'
        || inputType === 'email'
        || inputTag === 'TEXTAREA'
    );
}

/**
 * Verifica si un input HTML se puede marcar (tildar o activar).
 * @param {Element} inputElement Elemento HTML de tipo input.
 */
function isMarkable(inputElement) {
    if (! inputElement) {

        return false;
    }
    let inputType = inputElement.type;

    return (
        inputType === 'checkbox'
        || inputType === 'radio'
    );
}

/**
 * Verifica si un input HTML es una lista de opciones.
 * @param {Element} inputElement Elemento HTML de tipo input.
 */
function isSelect(inputElement) {
    if (! inputElement) {

        return false;
    }
    let inputType = inputElement.type;

    return (inputType === 'select');
}

export { isText, isMarkable, isSelect };
