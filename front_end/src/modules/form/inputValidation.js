import { firstCharToUppercase } from "../helper/string";

/**
 * Marca los campos de un formulario como válidos.
 * @param {string} formId Id del formulario en formato selector de CSS.
 * @param {Object|Array} errors JSON {'nombreCampo': 'Mensaje del error'} ó [].
 */
function markCorrect(formId, errors) {
    const inputErrors = errors;

    if (inputErrors && inputErrors.length === 0) {
        $(formId + ' *').filter(':input').each(function(index, value) {
            $("[id='" + value.id + "']").removeClass('is-invalid');
            $("[id='" + value.id + "']").addClass('is-valid');
            $("[id='" + value.id + "'] + .select2 .select2-selection--single")
            .css('border-color', '#28a745');
        });

        return;
    }

    $(formId + ' *').filter(':input').each(function(index, value) {
        $.each(inputErrors, function(fieldName) {
            let fieldId = firstCharToUppercase(fieldName);
            if (value.id !== fieldId) {
                $("[id='" + value.id + "']").removeClass('is-invalid');
                $("[id='" + value.id + "']").addClass('is-valid');
                $("[id='" + value.id + "'] + .select2 .select2-selection--single")
                .css('border-color', '#28a745');
            }
        });
    });
}


/**
 * Marca los campos de un formulario como inválidos.
 * @param {Object|Array} errors JSON {'nombreCampo': 'Mensaje del error'} ó [].
 */
function markIncorrect(errors) {
    const inputErrors = errors;

    if (inputErrors && inputErrors.length === 0) {

        return;
    }

    $.each(inputErrors, function(fieldName, errorMsg) {
        let fieldId = firstCharToUppercase(fieldName);
        $("[id='" + fieldId + "'] ~ .invalid-feedback").text(errorMsg);
        $("[id='" + fieldId + "']").removeClass('is-valid');
        $("[id='" + fieldId + "']").addClass('is-invalid');
        $("[id='" + fieldId + "'] + .select2 .select2-selection--single")
        .css('border-color', 'red');
    });
}

/**
 * Invocador en una sola llamada a markCorrect() y markIncorrect() para mostrar
 * la validación de los campos en un formulario.
 * @param {string} formId Id del formulario en formato selector CSS.
 * @param {Object|Array} errors JSON {'nombreCampo': 'Mensaje del error'} ó [].
 */
function showVal(formId, errors) {
    markCorrect(formId, errors);
    markIncorrect(errors);
}

export { showVal };
