/**
 * Mapeo de códigos de estatus de peticiones fallidas a mensajes en formato
 * humano-comprensible.
 */
export const TRANSLATE_STATUS = {
    200: {
        txt: 'El servidor devolvió un tipo de contenido errado.',
        img: '/assets/imgs/bg/undraw_server.svg',
        code: 400
    },
    404: {
        txt: 'La página no existe.',
        img: '/assets/imgs/bg/undraw_page_not_found.svg',
        code: 400
    },
    500: {
        txt: 'No se pudo procesar su solicitud.',
        img: '/assets/imgs/bg/undraw_server_down.svg',
        code: 500
    }
}
