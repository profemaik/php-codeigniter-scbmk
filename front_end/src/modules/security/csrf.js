/**
 * Refresca los tokens CSRF en el meta y en los formularios post.
 * @param {Object} csrf Contiene csrf_header, csrf_token y csrf_hash.
 */
function refreshCsrfToken(csrf) {
    const csrfHeader = 'meta[name="' + csrf.csrf_header + '"]';
    const csrfToken = 'input[name=' + csrf.csrf_token + ']';

    document.head.querySelector(csrfHeader).content = csrf.csrf_hash;
    document.body.querySelector(csrfToken).value = csrf.csrf_hash;
}

export { refreshCsrfToken };
