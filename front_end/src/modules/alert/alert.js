/**
 * Clases contextuales de Bootstrap para definir el tipo de la alerta.
 */
const ALERT_STYLE = {
    success: 'alert alert-success',
    info: 'alert alert-info',
    warning: 'alert alert-warning',
    error: 'alert alert-danger'
};

/**
 * Crea la alerta y la muestra usando fadeIn() de jQuery.
 * @param {Object} alert Contiene el tipo y mensaje de la alerta.
 */
function showAlert(alert) {
    let parent = document.getElementById('alert_parent');
    let modalParent = document.getElementById('modal_alert_parent');

    if (modalParent) {
        let divAlert = document.createElement('div');
        divAlert.setAttribute('class', ALERT_STYLE[alert.type]);
        divAlert.setAttribute('role', 'alert');
        divAlert.textContent = alert.msg;
        modalParent.innerHTML = '';
        modalParent.appendChild(divAlert);
        $(modalParent).fadeIn();
        return;
    }

    if (parent === null) {
        return;
    }

    let divAlert = document.createElement('div');
    divAlert.setAttribute('class', ALERT_STYLE[alert.type]);
    divAlert.setAttribute('role', 'alert');
    divAlert.textContent = alert.msg;
    parent.innerHTML = '';
    parent.appendChild(divAlert);
    $(parent).fadeIn();
}

/**
 * Destruye la alerta y la oculta usando hide() de jQuery.
 */
function closeAlert() {
    let parent = document.getElementById('alert_parent');

    if (parent === null) {
        console.log('No hay contenedor para la alerta');

        return;
    }

    parent.innerHTML = '';
    $(parent).hide();
}

export { showAlert,closeAlert };
