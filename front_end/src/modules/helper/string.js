/**
 * Devuelve una cadena con el primer carácter en mayúscula.
 * @param {string} str Cadena a ser procesada.
 */
function firstCharToUppercase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export { firstCharToUppercase };
